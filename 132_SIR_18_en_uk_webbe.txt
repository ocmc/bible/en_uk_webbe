en_uk_webbe~SIR~C18~01~He that lives forever created all things in common.
en_uk_webbe~SIR~C18~02~The Lord alone will be justified.
en_uk_webbe~SIR~C18~03~
en_uk_webbe~SIR~C18~04~To none has he given power to declare his works: And who will trace out his mighty deeds?
en_uk_webbe~SIR~C18~05~Who shall number the strength of his majesty? And who shall also tell out his mercies?
en_uk_webbe~SIR~C18~06~As for the wondrous works of the Lord, it is not possible to take from them nor add to them, Neither is it possible to track them out.
en_uk_webbe~SIR~C18~07~When a man has finished, then he is but at the beginning; And when he ceases, then he will be in perplexity.
en_uk_webbe~SIR~C18~08~What is man, and what purpose do they serve? What is his good, and what is his evil?
en_uk_webbe~SIR~C18~09~The number of man’s days at the most are a hundred years.
en_uk_webbe~SIR~C18~10~As a drop of water from the sea, and a pebble from the sand; So are a few years in the day of eternity.
en_uk_webbe~SIR~C18~11~For this cause the Lord was patient over them, And poured out his mercy upon them.
en_uk_webbe~SIR~C18~12~He saw and perceived their end, that it is evil; Therefore he multiplied his forgiveness.
en_uk_webbe~SIR~C18~13~The mercy of a man is upon his neighbour; But the mercy of the Lord is upon all flesh; Reproving, and chastening, and teaching, And bringing again, as a shepherd does his flock.
en_uk_webbe~SIR~C18~14~He has mercy on those who accept chastening, And that diligently seek after his judgements.
en_uk_webbe~SIR~C18~15~My son, to your good deeds add no blemish; And no grief of words in any of your giving.
en_uk_webbe~SIR~C18~16~Shall not the dew assuage the scorching heat? So is a word better than a gift.
en_uk_webbe~SIR~C18~17~Behold, is not a word better than a gift? And both are with a gracious man.
en_uk_webbe~SIR~C18~18~A fool will upbrade ungraciously; And the gift of an envious man consumes the eyes.
en_uk_webbe~SIR~C18~19~Learn before you speak; And have a care of your health or ever you be sick.
en_uk_webbe~SIR~C18~20~Before judgement examine yourself; And in the hour of visitation you will find forgiveness.
en_uk_webbe~SIR~C18~21~Humble yourself before you be sick; And in the time of sins show repentance.
en_uk_webbe~SIR~C18~22~Let nothing hinder you to pay your vow in due time; And wait not until death to be justified.
en_uk_webbe~SIR~C18~23~Before you make a vow, prepare yourself; And be not as a man that tempts the Lord.
en_uk_webbe~SIR~C18~24~Think upon the wrath [that will be ] in the days of the end, And the time of vengeance, when he turns away his face.
en_uk_webbe~SIR~C18~25~In the days of fullness remember the time of hunger, [And ] poverty and lack in the days of wealth.
en_uk_webbe~SIR~C18~26~From morning until evening the time changes; And all things are speedy before the Lord.
en_uk_webbe~SIR~C18~27~A wise man will fear in everything; And in days of sinning he will beware of offence.
en_uk_webbe~SIR~C18~28~Every man of understanding knows wisdom; And he will give thanks to him that found her.
en_uk_webbe~SIR~C18~29~They that were of understanding in sayings became also wise themselves, And poured forth apt proverbs.
en_uk_webbe~SIR~C18~30~Go not after your lusts; And refrain yourself from your appetites.
en_uk_webbe~SIR~C18~31~If you give fully to your soul the delight of her desire, She will make you the laughing stock of your enemies.
en_uk_webbe~SIR~C18~32~Make not merry in much luxury; Neither be tied to the expense thereof.
en_uk_webbe~SIR~C18~33~Be not made a beggar by banqueting upon borrowing, When you have nothing in your purse.
