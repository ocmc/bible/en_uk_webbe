en_uk_webbe~PSA~C077~001~(C078:001) (0) A contemplation by Asaph. (1) Hear my teaching, my people. Turn your ears to the words of my mouth.
en_uk_webbe~PSA~C077~002~(C078:002) I will open my mouth in a parable. I will utter dark sayings of old,
en_uk_webbe~PSA~C077~003~(C078:003) Which we have heard and known, and our fathers have told us.
en_uk_webbe~PSA~C077~004~(C078:004) We will not hide them from their children, telling to the generation to come the praises of the LORD, his strength, and his wondrous deeds that he has done.
en_uk_webbe~PSA~C077~005~(C078:005) For he established a covenant in Jacob, and appointed a teaching in Israel, which he commanded our fathers, that they should make them known to their children;
en_uk_webbe~PSA~C077~006~(C078:006) that the generation to come might know, even the children who should be born; who should arise and tell their children,
en_uk_webbe~PSA~C077~007~(C078:007) that they might set their hope in God, and not forget God’s deeds, but keep his commandments,
en_uk_webbe~PSA~C077~008~(C078:008) and might not be as their fathers, a stubborn and rebellious generation, a generation that didn’t make their hearts loyal, whose spirit was not steadfast with God.
en_uk_webbe~PSA~C077~009~(C078:009) The children of Ephraim, being armed and carrying bows, turned back in the day of battle.
en_uk_webbe~PSA~C077~010~(C078:010) They didn’t keep God’s covenant, and refused to walk in his law.
en_uk_webbe~PSA~C077~011~(C078:011) They forgot his doings, his wondrous deeds that he had shown them.
en_uk_webbe~PSA~C077~012~(C078:012) He did marvellous things in the sight of their fathers, in the land of Egypt, in the field of Zoan.
en_uk_webbe~PSA~C077~013~(C078:013) He split the sea, and caused them to pass through. He made the waters stand as a heap.
en_uk_webbe~PSA~C077~014~(C078:014) In the daytime he also led them with a cloud, and all night with a light of fire.
en_uk_webbe~PSA~C077~015~(C078:015) He split rocks in the wilderness, and gave them drink abundantly as out of the depths.
en_uk_webbe~PSA~C077~016~(C078:016) He brought streams also out of the rock, and caused waters to run down like rivers.
en_uk_webbe~PSA~C077~017~(C078:017) Yet they still went on to sin against him, to rebel against the Most High in the desert.
en_uk_webbe~PSA~C077~018~(C078:018) They tempted God in their heart by asking food according to their desire.
en_uk_webbe~PSA~C077~019~(C078:019) Yes, they spoke against God. They said, “Can God prepare a table in the wilderness?
en_uk_webbe~PSA~C077~020~(C078:020) Behold, he struck the rock, so that waters gushed out, and streams overflowed. Can he give bread also? Will he provide meat for his people?”
en_uk_webbe~PSA~C077~021~(C078:021) Therefore the LORD heard, and was angry. A fire was kindled against Jacob, anger also went up against Israel,
en_uk_webbe~PSA~C077~022~(C078:022) because they didn’t believe in God, and didn’t trust in his salvation.
en_uk_webbe~PSA~C077~023~(C078:023) Yet he commanded the skies above, and opened the doors of heaven.
en_uk_webbe~PSA~C077~024~(C078:024) He rained down manna on them to eat, and gave them food from the sky.
en_uk_webbe~PSA~C077~025~(C078:025) Man ate the bread of angels. He sent them food to the full.
en_uk_webbe~PSA~C077~026~(C078:026) He caused the east wind to blow in the sky. By his power he guided the south wind.
en_uk_webbe~PSA~C077~027~(C078:027) He also rained meat on them as the dust, winged birds as the sand of the seas.
en_uk_webbe~PSA~C077~028~(C078:028) He let them fall in the middle of their camp, around their habitations.
en_uk_webbe~PSA~C077~029~(C078:029) So they ate, and were well filled. He gave them their own desire.
en_uk_webbe~PSA~C077~030~(C078:030) They didn’t turn from their cravings. Their food was yet in their mouths,
en_uk_webbe~PSA~C077~031~(C078:031) when the anger of God went up against them, killed some of their fattest, and struck down the young men of Israel.
en_uk_webbe~PSA~C077~032~(C078:032) For all this they still sinned, and didn’t believe in his wondrous works.
en_uk_webbe~PSA~C077~033~(C078:033) Therefore he consumed their days in vanity, and their years in terror.
en_uk_webbe~PSA~C077~034~(C078:034) When he killed them, then they enquired after him. They returned and sought God earnestly.
en_uk_webbe~PSA~C077~035~(C078:035) They remembered that God was their rock, the Most High God, their redeemer.
en_uk_webbe~PSA~C077~036~(C078:036) But they flattered him with their mouth, and lied to him with their tongue.
en_uk_webbe~PSA~C077~037~(C078:037) For their heart was not right with him, neither were they faithful in his covenant.
en_uk_webbe~PSA~C077~038~(C078:038) But he, being merciful, forgave iniquity, and didn’t destroy them. Yes, many times he turned his anger away, and didn’t stir up all his wrath.
en_uk_webbe~PSA~C077~039~(C078:039) He remembered that they were but flesh, a wind that passes away, and doesn’t come again.
en_uk_webbe~PSA~C077~040~(C078:040) How often they rebelled against him in the wilderness, and grieved him in the desert!
en_uk_webbe~PSA~C077~041~(C078:041) They turned again and tempted God, and provoked the Holy One of Israel.
en_uk_webbe~PSA~C077~042~(C078:042) They didn’t remember his hand, nor the day when he redeemed them from the adversary;
en_uk_webbe~PSA~C077~043~(C078:043) how he set his signs in Egypt, his wonders in the field of Zoan,
en_uk_webbe~PSA~C077~044~(C078:044) he turned their rivers into blood, and their streams, so that they could not drink.
en_uk_webbe~PSA~C077~045~(C078:045) He sent amongst them swarms of flies, which devoured them; and frogs, which destroyed them.
en_uk_webbe~PSA~C077~046~(C078:046) He gave also their increase to the caterpillar, and their labour to the locust.
en_uk_webbe~PSA~C077~047~(C078:047) He destroyed their vines with hail, their sycamore fig trees with frost.
en_uk_webbe~PSA~C077~048~(C078:048) He gave over their livestock also to the hail, and their flocks to hot thunderbolts.
en_uk_webbe~PSA~C077~049~(C078:049) He threw on them the fierceness of his anger, wrath, indignation, and trouble, and a band of angels of evil.
en_uk_webbe~PSA~C077~050~(C078:050) He made a path for his anger. He didn’t spare their soul from death, but gave their life over to the pestilence,
en_uk_webbe~PSA~C077~051~(C078:051) and struck all the firstborn in Egypt, the chief of their strength in the tents of Ham.
en_uk_webbe~PSA~C077~052~(C078:052) But he led out his own people like sheep, and guided them in the wilderness like a flock.
en_uk_webbe~PSA~C077~053~(C078:053) He led them safely, so that they weren’t afraid, but the sea overwhelmed their enemies.
en_uk_webbe~PSA~C077~054~(C078:054) He brought them to the border of his sanctuary, to this mountain, which his right hand had taken.
en_uk_webbe~PSA~C077~055~(C078:055) He also drove out the nations before them, allotted them for an inheritance by line, and made the tribes of Israel to dwell in their tents.
en_uk_webbe~PSA~C077~056~(C078:056) Yet they tempted and rebelled against the Most High God, and didn’t keep his testimonies,
en_uk_webbe~PSA~C077~057~(C078:057) but turned back, and dealt treacherously like their fathers. They were twisted like a deceitful bow.
en_uk_webbe~PSA~C077~058~(C078:058) For they provoked him to anger with their high places, and moved him to jealousy with their engraved images.
en_uk_webbe~PSA~C077~059~(C078:059) When God heard this, he was angry, and greatly abhorred Israel,
en_uk_webbe~PSA~C077~060~(C078:060) so that he abandoned the tent of Shiloh, the tent which he placed amongst men,
en_uk_webbe~PSA~C077~061~(C078:061) and delivered his strength into captivity, his glory into the adversary’s hand.
en_uk_webbe~PSA~C077~062~(C078:062) He also gave his people over to the sword, and was angry with his inheritance.
en_uk_webbe~PSA~C077~063~(C078:063) Fire devoured their young men. Their virgins had no wedding song.
en_uk_webbe~PSA~C077~064~(C078:064) Their priests fell by the sword, and their widows couldn’t weep.
en_uk_webbe~PSA~C077~065~(C078:065) Then the Lord awakened as one out of sleep, like a mighty man who shouts by reason of wine.
en_uk_webbe~PSA~C077~066~(C078:066) He struck his adversaries backward. He put them to a perpetual reproach.
en_uk_webbe~PSA~C077~067~(C078:067) Moreover he rejected the tent of Joseph, and didn’t choose the tribe of Ephraim,
en_uk_webbe~PSA~C077~068~(C078:068) But chose the tribe of Judah, Mount Zion which he loved.
en_uk_webbe~PSA~C077~069~(C078:069) He built his sanctuary like the heights, like the earth which he has established forever.
en_uk_webbe~PSA~C077~070~(C078:070) He also chose David his servant, and took him from the sheepfolds;
en_uk_webbe~PSA~C077~071~(C078:071) from following the ewes that have their young, he brought him to be the shepherd of Jacob, his people, and Israel, his inheritance.
en_uk_webbe~PSA~C077~072~(C078:072) So he was their shepherd according to the integrity of his heart, and guided them by the skilfulness of his hands.
