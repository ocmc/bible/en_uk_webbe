en_uk_webbe~PRO~C13~01~A wise son listens to his father’s instruction, but a scoffer doesn’t listen to rebuke.
en_uk_webbe~PRO~C13~02~By the fruit of his lips, a man enjoys good things, but the unfaithful crave violence.
en_uk_webbe~PRO~C13~03~He who guards his mouth guards his soul. One who opens wide his lips comes to ruin.
en_uk_webbe~PRO~C13~04~The soul of the sluggard desires, and has nothing, but the desire of the diligent shall be fully satisfied.
en_uk_webbe~PRO~C13~05~A righteous man hates lies, but a wicked man brings shame and disgrace.
en_uk_webbe~PRO~C13~06~Righteousness guards the way of integrity, but wickedness overthrows the sinner.
en_uk_webbe~PRO~C13~07~There are some who pretend to be rich, yet have nothing. There are some who pretend to be poor, yet have great wealth.
en_uk_webbe~PRO~C13~08~The ransom of a man’s life is his riches, but the poor hear no threats.
en_uk_webbe~PRO~C13~09~The light of the righteous shines brightly, but the lamp of the wicked is snuffed out.
en_uk_webbe~PRO~C13~10~Pride only breeds quarrels, but wisdom is with people who take advice.
en_uk_webbe~PRO~C13~11~Wealth gained dishonestly dwindles away, but he who gathers by hand makes it grow.
en_uk_webbe~PRO~C13~12~Hope deferred makes the heart sick, but when longing is fulfilled, it is a tree of life.
en_uk_webbe~PRO~C13~13~Whoever despises instruction will pay for it, but he who respects a command will be rewarded.
en_uk_webbe~PRO~C13~14~The teaching of the wise is a spring of life, to turn from the snares of death.
en_uk_webbe~PRO~C13~15~Good understanding wins favour, but the way of the unfaithful is hard.
en_uk_webbe~PRO~C13~16~Every prudent man acts from knowledge, but a fool exposes folly.
en_uk_webbe~PRO~C13~17~A wicked messenger falls into trouble, but a trustworthy envoy gains healing.
en_uk_webbe~PRO~C13~18~Poverty and shame come to him who refuses discipline, but he who heeds correction shall be honoured.
en_uk_webbe~PRO~C13~19~Longing fulfilled is sweet to the soul, but fools detest turning from evil.
en_uk_webbe~PRO~C13~20~One who walks with wise men grows wise, but a companion of fools suffers harm.
en_uk_webbe~PRO~C13~21~Misfortune pursues sinners, but prosperity rewards the righteous.
en_uk_webbe~PRO~C13~22~A good man leaves an inheritance to his children’s children, but the wealth of the sinner is stored for the righteous.
en_uk_webbe~PRO~C13~23~An abundance of food is in poor people’s fields, but injustice sweeps it away.
en_uk_webbe~PRO~C13~24~One who spares the rod hates his son, but one who loves him is careful to discipline him.
en_uk_webbe~PRO~C13~25~The righteous one eats to the satisfying of his soul, but the belly of the wicked goes hungry.
