en_uk_webbe~CH1~C12~01~Now these are those who came to David to Ziklag, while he was a fugitive from Saul the son of Kish. They were amongst the mighty men, his helpers in war.
en_uk_webbe~CH1~C12~02~They were armed with bows, and could use both the right hand and the left in slinging stones and in shooting arrows from the bow. They were of Saul’s relatives of the tribe of Benjamin.
en_uk_webbe~CH1~C12~03~The chief was Ahiezer, then Joash, the sons of Shemaah the Gibeathite; Jeziel and Pelet, the sons of Azmaveth; Beracah; Jehu the Anathothite;
en_uk_webbe~CH1~C12~04~Ishmaiah the Gibeonite, a mighty man amongst the thirty and a leader of the thirty; Jeremiah; Jahaziel; Johanan; Jozabad the Gederathite;
en_uk_webbe~CH1~C12~05~Eluzai; Jerimoth; Bealiah; Shemariah; Shephatiah the Haruphite;
en_uk_webbe~CH1~C12~06~Elkanah, Isshiah Azarel, Joezer, and Jashobeam, the Korahites;
en_uk_webbe~CH1~C12~07~and Joelah and Zebadiah, the sons of Jeroham of Gedor.
en_uk_webbe~CH1~C12~08~Some Gadites joined David in the stronghold in the wilderness, mighty men of valour, men trained for war, who could handle shield and spear; whose faces were like the faces of lions, and they were as swift as the gazelles on the mountains:
en_uk_webbe~CH1~C12~09~Ezer the chief, Obadiah the second, Eliab the third,
en_uk_webbe~CH1~C12~10~Mishmannah the fourth, Jeremiah the fifth,
en_uk_webbe~CH1~C12~11~Attai the sixth, Eliel the seventh,
en_uk_webbe~CH1~C12~12~Johanan the eighth, Elzabad the ninth,
en_uk_webbe~CH1~C12~13~Jeremiah the tenth, and Machbannai the eleventh.
en_uk_webbe~CH1~C12~14~These of the sons of Gad were captains of the army: he who was least was equal to one hundred, and the greatest to one thousand.
en_uk_webbe~CH1~C12~15~These are those who went over the Jordan in the first month, when it had overflowed all its banks; and they put to flight all who lived in the valleys, both towards the east and towards the west.
en_uk_webbe~CH1~C12~16~Some of the children of Benjamin and Judah came to the stronghold to David.
en_uk_webbe~CH1~C12~17~David went out to meet them, and answered them, “If you have come peaceably to me to help me, my heart will be united with you; but if you have come to betray me to my adversaries, since there is no wrong in my hands, may the God of our fathers see this and rebuke it.”
en_uk_webbe~CH1~C12~18~Then the Spirit came on Amasai, who was chief of the thirty, and he said, “We are yours, David, and on your side, you son of Jesse. Peace, peace be to you, and peace be to your helpers; for your God helps you.” Then David received them, and made them captains of the band.
en_uk_webbe~CH1~C12~19~Some of Manasseh also joined David, when he came with the Philistines against Saul to battle; but they didn’t help them; for the lords of the Philistines sent him away after consultation, saying, “He will desert to his master Saul to the jeopardy of our heads.”
en_uk_webbe~CH1~C12~20~As he went to Ziklag, some from Manasseh joined him: Adnah, Jozabad, Jediael, Michael, Jozabad, Elihu, and Zillethai, captains of thousands who were of Manasseh.
en_uk_webbe~CH1~C12~21~They helped David against the band of rovers; for they were all mighty men of valour, and were captains in the army.
en_uk_webbe~CH1~C12~22~For from day to day men came to David to help him, until there was a great army, like God’s army.
en_uk_webbe~CH1~C12~23~These are the numbers of the heads of those who were armed for war, who came to David to Hebron, to turn the kingdom of Saul to him, according to the LORD’s word.
en_uk_webbe~CH1~C12~24~The children of Judah who bore shield and spear were six thousand and eight hundred, armed for war.
en_uk_webbe~CH1~C12~25~Of the children of Simeon, mighty men of valour for the war: seven thousand and one hundred.
en_uk_webbe~CH1~C12~26~Of the children of Levi: four thousand and six hundred.
en_uk_webbe~CH1~C12~27~Jehoiada was the leader of the household of Aaron; and with him were three thousand and seven hundred,
en_uk_webbe~CH1~C12~28~and Zadok, a young man mighty of valour, and of his father’s house twenty-two captains.
en_uk_webbe~CH1~C12~29~Of the children of Benjamin, Saul’s relatives: three thousand, for until then, the greatest part of them had kept their allegiance to Saul’s house.
en_uk_webbe~CH1~C12~30~Of the children of Ephraim: twenty thousand and eight hundred, mighty men of valour, famous men in their fathers’ houses.
en_uk_webbe~CH1~C12~31~Of the half-tribe of Manasseh: eighteen thousand, who were mentioned by name, to come and make David king.
en_uk_webbe~CH1~C12~32~Of the children of Issachar, men who had understanding of the times, to know what Israel ought to do, their heads were two hundred; and all their brothers were at their command.
en_uk_webbe~CH1~C12~33~Of Zebulun, such as were able to go out in the army, who could set the battle in array, with all kinds of instruments of war: fifty thousand who could command and were not of double heart.
en_uk_webbe~CH1~C12~34~Of Naphtali: one thousand captains, and with them with shield and spear thirty-seven thousand.
en_uk_webbe~CH1~C12~35~Of the Danites who could set the battle in array: twenty-eight thousand and six hundred.
en_uk_webbe~CH1~C12~36~Of Asher, such as were able to go out in the army, who could set the battle in array: forty thousand.
en_uk_webbe~CH1~C12~37~On the other side of the Jordan, of the Reubenites, the Gadites, and of the half-tribe of Manasseh, with all kinds of instruments of war for the battle: one hundred and twenty thousand.
en_uk_webbe~CH1~C12~38~All these were men of war, who could order the battle array, and came with a perfect heart to Hebron, to make David king over all Israel; and all the rest also of Israel were of one heart to make David king.
en_uk_webbe~CH1~C12~39~They were there with David three days, eating and drinking; for their brothers had supplied provisions for them.
en_uk_webbe~CH1~C12~40~Moreover those who were near to them, as far as Issachar, Zebulun, and Naphtali, brought bread on donkeys, on camels, on mules, and on oxen: supplies of flour, cakes of figs, clusters of raisins, wine, oil, cattle, and sheep in abundance; for there was joy in Israel.
