en_uk_webbe~JER~C43~01~When Jeremiah had finished speaking to all the people all the words of the LORD their God, with which the LORD their God had sent him to them, even all these words,
en_uk_webbe~JER~C43~02~then Azariah the son of Hoshaiah, Johanan the son of Kareah, and all the proud men spoke, saying to Jeremiah, “You speak falsely. The LORD our God has not sent you to say, ‘You shall not go into Egypt to live there;’
en_uk_webbe~JER~C43~03~but Baruch the son of Neriah has turned you against us, to deliver us into the hand of the Chaldeans, that they may put us to death, and carry us away captive to Babylon.”
en_uk_webbe~JER~C43~04~So Johanan the son of Kareah, and all the captains of the forces, and all the people, didn’t obey the LORD’s voice, to dwell in the land of Judah.
en_uk_webbe~JER~C43~05~But Johanan the son of Kareah, and all the captains of the forces, took all the remnant of Judah, who had returned from all the nations where they had been driven, to live in the land of Judah;
en_uk_webbe~JER~C43~06~the men, and the women, and the children, and the king’s daughters, and every person who Nebuzaradan the captain of the guard had left with Gedaliah the son of Ahikam, the son of Shaphan; and Jeremiah the prophet, and Baruch the son of Neriah;
en_uk_webbe~JER~C43~07~and they came into the land of Egypt; for they didn’t obey the LORD’s voice: and they came to Tahpanhes.
en_uk_webbe~JER~C43~08~Then the LORD’s word came to Jeremiah in Tahpanhes, saying,
en_uk_webbe~JER~C43~09~“Take great stones in your hand, and hide them in mortar in the brick work, which is at the entry of Pharaoh’s house in Tahpanhes, in the sight of the men of Judah;
en_uk_webbe~JER~C43~10~and tell them, the LORD of Armies, the God of Israel, says: ‘Behold, I will send and take Nebuchadnezzar the king of Babylon, my servant, and will set his throne on these stones that I have hidden; and he will spread his royal pavilion over them.
en_uk_webbe~JER~C43~11~He will come, and will strike the land of Egypt; such as are for death will be put to death, and such as are for captivity to captivity, and such as are for the sword to the sword.
en_uk_webbe~JER~C43~12~I will kindle a fire in the houses of the gods of Egypt. He will burn them, and carry them away captive. He will array himself with the land of Egypt, as a shepherd puts on his garment; and he will go out from there in peace.
en_uk_webbe~JER~C43~13~He will also break the pillars of Beth Shemesh, that is in the land of Egypt; and he will burn the houses of the gods of Egypt with fire.’”
