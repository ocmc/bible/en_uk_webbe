en_uk_webbe~PRO~C04~01~Listen, sons, to a father’s instruction. Pay attention and know understanding;
en_uk_webbe~PRO~C04~02~for I give you sound learning. Don’t forsake my law.
en_uk_webbe~PRO~C04~03~For I was a son to my father, tender and an only child in the sight of my mother.
en_uk_webbe~PRO~C04~04~He taught me, and said to me: “Let your heart retain my words. Keep my commandments, and live.
en_uk_webbe~PRO~C04~05~Get wisdom. Get understanding. Don’t forget, and don’t deviate from the words of my mouth.
en_uk_webbe~PRO~C04~06~Don’t forsake her, and she will preserve you. Love her, and she will keep you.
en_uk_webbe~PRO~C04~07~Wisdom is supreme. Get wisdom. Yes, though it costs all your possessions, get understanding.
en_uk_webbe~PRO~C04~08~Esteem her, and she will exalt you. She will bring you to honour when you embrace her.
en_uk_webbe~PRO~C04~09~She will give to your head a garland of grace. She will deliver a crown of splendour to you.”
en_uk_webbe~PRO~C04~10~Listen, my son, and receive my sayings. The years of your life will be many.
en_uk_webbe~PRO~C04~11~I have taught you in the way of wisdom. I have led you in straight paths.
en_uk_webbe~PRO~C04~12~When you go, your steps will not be hampered. When you run, you will not stumble.
en_uk_webbe~PRO~C04~13~Take firm hold of instruction. Don’t let her go. Keep her, for she is your life.
en_uk_webbe~PRO~C04~14~Don’t enter into the path of the wicked. Don’t walk in the way of evil men.
en_uk_webbe~PRO~C04~15~Avoid it, and don’t pass by it. Turn from it, and pass on.
en_uk_webbe~PRO~C04~16~For they don’t sleep unless they do evil. Their sleep is taken away, unless they make someone fall.
en_uk_webbe~PRO~C04~17~For they eat the bread of wickedness and drink the wine of violence.
en_uk_webbe~PRO~C04~18~But the path of the righteous is like the dawning light that shines more and more until the perfect day.
en_uk_webbe~PRO~C04~19~The way of the wicked is like darkness. They don’t know what they stumble over.
en_uk_webbe~PRO~C04~20~My son, attend to my words. Turn your ear to my sayings.
en_uk_webbe~PRO~C04~21~Let them not depart from your eyes. Keep them in the centre of your heart.
en_uk_webbe~PRO~C04~22~For they are life to those who find them, and health to their whole body.
en_uk_webbe~PRO~C04~23~Keep your heart with all diligence, for out of it is the wellspring of life.
en_uk_webbe~PRO~C04~24~Put away from yourself a perverse mouth. Put corrupt lips far from you.
en_uk_webbe~PRO~C04~25~Let your eyes look straight ahead. Fix your gaze directly before you.
en_uk_webbe~PRO~C04~26~Make the path of your feet level. Let all of your ways be established.
en_uk_webbe~PRO~C04~27~Don’t turn to the right hand nor to the left. Remove your foot from evil.
