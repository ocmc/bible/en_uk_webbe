en_uk_webbe~JOB~C21~01~Then Job answered,
en_uk_webbe~JOB~C21~02~“Listen diligently to my speech. Let this be your consolation.
en_uk_webbe~JOB~C21~03~Allow me, and I also will speak; After I have spoken, mock on.
en_uk_webbe~JOB~C21~04~As for me, is my complaint to man? Why shouldn’t I be impatient?
en_uk_webbe~JOB~C21~05~Look at me, and be astonished. Lay your hand on your mouth.
en_uk_webbe~JOB~C21~06~When I remember, I am troubled. Horror takes hold of my flesh.
en_uk_webbe~JOB~C21~07~“Why do the wicked live, become old, yes, and grow mighty in power?
en_uk_webbe~JOB~C21~08~Their child is established with them in their sight, their offspring before their eyes.
en_uk_webbe~JOB~C21~09~Their houses are safe from fear, neither is the rod of God upon them.
en_uk_webbe~JOB~C21~10~Their bulls breed without fail. Their cows calve, and don’t miscarry.
en_uk_webbe~JOB~C21~11~They send out their little ones like a flock. Their children dance.
en_uk_webbe~JOB~C21~12~They sing to the tambourine and harp, and rejoice at the sound of the pipe.
en_uk_webbe~JOB~C21~13~They spend their days in prosperity. In an instant they go down to Sheol.
en_uk_webbe~JOB~C21~14~They tell God, ‘Depart from us, for we don’t want to know about your ways.
en_uk_webbe~JOB~C21~15~What is the Almighty, that we should serve him? What profit should we have, if we pray to him?’
en_uk_webbe~JOB~C21~16~Behold, their prosperity is not in their hand. The counsel of the wicked is far from me.
en_uk_webbe~JOB~C21~17~“How often is it that the lamp of the wicked is put out, that their calamity comes on them, that God distributes sorrows in his anger?
en_uk_webbe~JOB~C21~18~How often is it that they are as stubble before the wind, as chaff that the storm carries away?
en_uk_webbe~JOB~C21~19~You say, ‘God lays up his iniquity for his children.’ Let him recompense it to himself, that he may know it.
en_uk_webbe~JOB~C21~20~Let his own eyes see his destruction. Let him drink of the wrath of the Almighty.
en_uk_webbe~JOB~C21~21~For what does he care for his house after him, when the number of his months is cut off?
en_uk_webbe~JOB~C21~22~“Shall any teach God knowledge, since he judges those who are high?
en_uk_webbe~JOB~C21~23~One dies in his full strength, being wholly at ease and quiet.
en_uk_webbe~JOB~C21~24~His pails are full of milk. The marrow of his bones is moistened.
en_uk_webbe~JOB~C21~25~Another dies in bitterness of soul, and never tastes of good.
en_uk_webbe~JOB~C21~26~They lie down alike in the dust. The worm covers them.
en_uk_webbe~JOB~C21~27~“Behold, I know your thoughts, the plans with which you would wrong me.
en_uk_webbe~JOB~C21~28~For you say, ‘Where is the house of the prince? Where is the tent in which the wicked lived?’
en_uk_webbe~JOB~C21~29~Haven’t you asked wayfaring men? Don’t you know their evidences,
en_uk_webbe~JOB~C21~30~that the evil man is reserved to the day of calamity, That they are led out to the day of wrath?
en_uk_webbe~JOB~C21~31~Who will declare his way to his face? Who will repay him what he has done?
en_uk_webbe~JOB~C21~32~Yet he will be borne to the grave. Men will keep watch over the tomb.
en_uk_webbe~JOB~C21~33~The clods of the valley will be sweet to him. All men will draw after him, as there were innumerable before him.
en_uk_webbe~JOB~C21~34~So how can you comfort me with nonsense, because in your answers there remains only falsehood?”
