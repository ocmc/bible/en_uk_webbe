en_uk_webbe~ISA~C06~01~In the year that king Uzziah died, I saw the Lord sitting on a throne, high and lifted up; and his train filled the temple.
en_uk_webbe~ISA~C06~02~Above him stood the seraphim. Each one had six wings. With two he covered his face. With two he covered his feet. With two he flew.
en_uk_webbe~ISA~C06~03~One called to another, and said, “Holy, holy, holy, is the LORD of Armies! The whole earth is full of his glory!”
en_uk_webbe~ISA~C06~04~The foundations of the thresholds shook at the voice of him who called, and the house was filled with smoke.
en_uk_webbe~ISA~C06~05~Then I said, “Woe is me! For I am undone, because I am a man of unclean lips, and I dwell amongst a people of unclean lips: for my eyes have seen the King, the LORD of Armies!”
en_uk_webbe~ISA~C06~06~Then one of the seraphim flew to me, having a live coal in his hand, which he had taken with the tongs from off the altar.
en_uk_webbe~ISA~C06~07~He touched my mouth with it, and said, “Behold, this has touched your lips; and your iniquity is taken away, and your sin forgiven.”
en_uk_webbe~ISA~C06~08~I heard the Lord’s voice, saying, “Whom shall I send, and who will go for us?” Then I said, “Here I am. Send me!”
en_uk_webbe~ISA~C06~09~He said, “Go, and tell this people, ‘You hear indeed, but don’t understand. You see indeed, but don’t perceive.’
en_uk_webbe~ISA~C06~10~Make the heart of this people fat. Make their ears heavy, and shut their eyes; lest they see with their eyes, hear with their ears, understand with their heart, and turn again, and be healed.”
en_uk_webbe~ISA~C06~11~Then I said, “Lord, how long?” He answered, “Until cities are waste without inhabitant, houses without man, the land becomes utterly waste,
en_uk_webbe~ISA~C06~12~and the LORD has removed men far away, and the forsaken places are many within the land.
en_uk_webbe~ISA~C06~13~If there is a tenth left in it, that also will in turn be consumed, as a terebinth, and as an oak, whose stump remains when they are cut down; so the holy seed is its stock.”
