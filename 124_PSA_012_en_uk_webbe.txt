en_uk_webbe~PSA~C012~001~(C013:000) For the Chief Musician. A Psalm by David.
en_uk_webbe~PSA~C012~002~(C013:001) How long, LORD? Will you forget me forever? How long will you hide your face from me?
en_uk_webbe~PSA~C012~003~(C013:002) How long shall I take counsel in my soul, having sorrow in my heart every day? How long shall my enemy triumph over me?
en_uk_webbe~PSA~C012~004~(C013:003) Behold, and answer me, LORD, my God. Give light to my eyes, lest I sleep in death;
en_uk_webbe~PSA~C012~005~(C013:004) lest my enemy say, “I have prevailed against him;” lest my adversaries rejoice when I fall.
en_uk_webbe~PSA~C012~006~(C013:005) But I trust in your loving kindness. My heart rejoices in your salvation. (6)I will sing to the LORD, because he has been good to me.
