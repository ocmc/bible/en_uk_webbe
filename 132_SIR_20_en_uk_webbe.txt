en_uk_webbe~SIR~C20~01~There is a reproof that is not comely; And there is a man that keeps silence, and he is wise.
en_uk_webbe~SIR~C20~02~How good is it to reprove, rather than to be angry; And he that makes confession will be kept back from hurt.
en_uk_webbe~SIR~C20~03~
en_uk_webbe~SIR~C20~04~As is the lust of an eunuch to deflower a virgin; So is he that executes judgements with violence.
en_uk_webbe~SIR~C20~05~There is one that keeps silence, and is found wise; And there is one that is hated for his much talk.
en_uk_webbe~SIR~C20~06~There is one that keeps silence, for he has no answer to make; And there is that keeps silence, as knowing his time.
en_uk_webbe~SIR~C20~07~A wise man will be silent till his time come; But the braggart and fool will overpass his time.
en_uk_webbe~SIR~C20~08~He that uses many words will be abhorred; And he that takes to himself authority therein will be hated.
en_uk_webbe~SIR~C20~09~There is a prosperity that a man finds in misfortunes; And there is a gain that turns to loss.
en_uk_webbe~SIR~C20~10~There is a gift that will not profit you; And there is a gift whose recompense is double.
en_uk_webbe~SIR~C20~11~There is an abasement because of glory; And there is that has lifted up his head from a low estate.
en_uk_webbe~SIR~C20~12~There is that buys much for a little, And pays for it again sevenfold.
en_uk_webbe~SIR~C20~13~He that is wise in words will make himself beloved; But the pleasantries of fools will be wasted.
en_uk_webbe~SIR~C20~14~The gift of a fool will not profit you; For his eyes are many instead of one.
en_uk_webbe~SIR~C20~15~He will give little, and upbraid much; And he will open his mouth like a crier: Today he will lend, and tomorrow he will ask it again: Such an one is a hateful man.
en_uk_webbe~SIR~C20~16~The fool will say, I have no friend, And I have no thanks for my good deeds; They that eat my bread are of evil tongue.
en_uk_webbe~SIR~C20~17~How often, and of how many, will he be laughed to scorn!
en_uk_webbe~SIR~C20~18~A slip on a pavement is better than [a slip ] with the tongue; So the fall of the wicked will come speedily.
en_uk_webbe~SIR~C20~19~A man without grace is [as ] a tale out of season: It will be continually in the mouth of the ignorant.
en_uk_webbe~SIR~C20~20~A wise sentence from a fool’s mouth will be rejected; For he will not speak it in its season.
en_uk_webbe~SIR~C20~21~There is that is hindered from sinning through lack; And when he takes rest, he will not be troubled.
en_uk_webbe~SIR~C20~22~There is that destroys his soul through bashfulness; And by a foolish countenance he will destroy it.
en_uk_webbe~SIR~C20~23~There is that for bashfulness promises to his friend; And he makes him his enemy for nothing.
en_uk_webbe~SIR~C20~24~A lie is a foul blot in a man: It will be continually in the mouth of the ignorant.
en_uk_webbe~SIR~C20~25~A thief is better than a man that is continually lying: But they both will inherit destruction.
en_uk_webbe~SIR~C20~26~The disposition of a liar is dishonour; And his shame is with him continually.
en_uk_webbe~SIR~C20~27~He that is wise in words will advance himself; And one that is prudent will please great men.
en_uk_webbe~SIR~C20~28~He that tils his land will raise his heap high; And he that pleases great men will get pardon for iniquity.
en_uk_webbe~SIR~C20~29~Presents and gifts blind the eyes of the wise, And as a muzzle on the mouth, turn away reproofs.
en_uk_webbe~SIR~C20~30~Wisdom that is hid, and treasure that is out of sight, What profit is in them both?
en_uk_webbe~SIR~C20~31~Better is a man that hides his folly Than a man that hides his wisdom.
en_uk_webbe~SIR~C20~32~
