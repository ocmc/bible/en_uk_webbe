en_uk_webbe~PSA~C083~001~(C084:000) For the Chief Musician. On an instrument of Gath. A Psalm by the sons of Korah.
en_uk_webbe~PSA~C083~002~(C084:001) How lovely are your dwellings, LORD of Armies!
en_uk_webbe~PSA~C083~003~(C084:002) My soul longs, and even faints for the courts of the LORD. My heart and my flesh cry out for the living God.
en_uk_webbe~PSA~C083~004~(C084:003) Yes, the sparrow has found a home, and the swallow a nest for herself, where she may have her young, near your altars, LORD of Armies, my King, and my God.
en_uk_webbe~PSA~C083~005~(C084:004) Blessed are those who dwell in your house. They are always praising you. Selah.
en_uk_webbe~PSA~C083~006~(C084:005) Blessed are those whose strength is in you, who have set their hearts on a pilgrimage.
en_uk_webbe~PSA~C083~007~(C084:006) Passing through the valley of Weeping, they make it a place of springs. Yes, the autumn rain covers it with blessings.
en_uk_webbe~PSA~C083~008~(C084:007) They go from strength to strength. Every one of them appears before God in Zion.
en_uk_webbe~PSA~C083~009~(C084:008) LORD, God of Armies, hear my prayer. Listen, God of Jacob. Selah.
en_uk_webbe~PSA~C083~010~(C084:009) Behold, God our shield, look at the face of your anointed.
en_uk_webbe~PSA~C083~011~(C084:010) For a day in your courts is better than a thousand. I would rather be a doorkeeper in the house of my God, than to dwell in the tents of wickedness.
en_uk_webbe~PSA~C083~012~(C084:011) For the LORD God is a sun and a shield. The LORD will give grace and glory. He withholds no good thing from those who walk blamelessly.
en_uk_webbe~PSA~C083~013~(C084:012) LORD of Armies, blessed is the man who trusts in you.
