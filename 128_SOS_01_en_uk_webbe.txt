en_uk_webbe~SOS~C01~01~The Song of songs, which is Solomon’s.
en_uk_webbe~SOS~C01~02~Let him kiss me with the kisses of his mouth; for your love is better than wine.
en_uk_webbe~SOS~C01~03~Your oils have a pleasing fragrance. Your name is oil poured out, therefore the virgins love you.
en_uk_webbe~SOS~C01~04~Take me away with you. Let’s hurry. The king has brought me into his rooms. Friends We will be glad and rejoice in you. We will praise your love more than wine! Beloved They are right to love you.
en_uk_webbe~SOS~C01~05~I am dark, but lovely, you daughters of Jerusalem, like Kedar’s tents, like Solomon’s curtains.
en_uk_webbe~SOS~C01~06~Don’t stare at me because I am dark, because the sun has scorched me. My mother’s sons were angry with me. They made me keeper of the vineyards. I haven’t kept my own vineyard.
en_uk_webbe~SOS~C01~07~Tell me, you whom my soul loves, where you graze your flock, where you rest them at noon; for why should I be as one who is veiled beside the flocks of your companions?
en_uk_webbe~SOS~C01~08~If you don’t know, most beautiful amongst women, follow the tracks of the sheep. Graze your young goats beside the shepherds’ tents.
en_uk_webbe~SOS~C01~09~I have compared you, my love, to a steed in Pharaoh’s chariots.
en_uk_webbe~SOS~C01~10~Your cheeks are beautiful with earrings, your neck with strings of jewels.
en_uk_webbe~SOS~C01~11~We will make you earrings of gold, with studs of silver.
en_uk_webbe~SOS~C01~12~While the king sat at his table, my perfume spread its fragrance.
en_uk_webbe~SOS~C01~13~My beloved is to me a sachet of myrrh, that lies between my breasts.
en_uk_webbe~SOS~C01~14~My beloved is to me a cluster of henna blossoms from the vineyards of En Gedi.
en_uk_webbe~SOS~C01~15~Behold, you are beautiful, my love. Behold, you are beautiful. Your eyes are like doves.
en_uk_webbe~SOS~C01~16~Behold, you are beautiful, my beloved, yes, pleasant; and our couch is verdant.
en_uk_webbe~SOS~C01~17~The beams of our house are cedars. Our rafters are firs.
