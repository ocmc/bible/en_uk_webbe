en_uk_webbe~ISA~C32~01~Behold, a king shall reign in righteousness, and princes shall rule in justice.
en_uk_webbe~ISA~C32~02~A man shall be as a hiding place from the wind, and a covert from the storm, as streams of water in a dry place, as the shade of a large rock in a weary land.
en_uk_webbe~ISA~C32~03~The eyes of those who see will not be dim, and the ears of those who hear will listen.
en_uk_webbe~ISA~C32~04~The heart of the rash will understand knowledge, and the tongue of the stammerers will be ready to speak plainly.
en_uk_webbe~ISA~C32~05~The fool will no longer be called noble, nor the scoundrel be highly respected.
en_uk_webbe~ISA~C32~06~For the fool will speak folly, and his heart will work iniquity, to practise profanity, and to utter error against the LORD, to make empty the soul of the hungry, and to cause the drink of the thirsty to fail.
en_uk_webbe~ISA~C32~07~The ways of the scoundrel are evil. He devises wicked plans to destroy the humble with lying words, even when the needy speaks right.
en_uk_webbe~ISA~C32~08~But the noble devises noble things; and he will continue in noble things.
en_uk_webbe~ISA~C32~09~Rise up, you women who are at ease! Hear my voice! You careless daughters, give ear to my speech!
en_uk_webbe~ISA~C32~10~For days beyond a year you will be troubled, you careless women; for the vintage will fail. The harvest won’t come.
en_uk_webbe~ISA~C32~11~Tremble, you women who are at ease! Be troubled, you careless ones! Strip yourselves, make yourselves naked, and put sackcloth on your waist.
en_uk_webbe~ISA~C32~12~Beat your breasts for the pleasant fields, for the fruitful vine.
en_uk_webbe~ISA~C32~13~Thorns and briers will come up on my people’s land; yes, on all the houses of joy in the joyous city.
en_uk_webbe~ISA~C32~14~For the palace will be forsaken. The populous city will be deserted. The hill and the watchtower will be for dens forever, a delight for wild donkeys, a pasture of flocks,
en_uk_webbe~ISA~C32~15~until the Spirit is poured on us from on high, and the wilderness becomes a fruitful field, and the fruitful field is considered a forest.
en_uk_webbe~ISA~C32~16~Then justice will dwell in the wilderness; and righteousness will remain in the fruitful field.
en_uk_webbe~ISA~C32~17~The work of righteousness will be peace, and the effect of righteousness, quietness and confidence forever.
en_uk_webbe~ISA~C32~18~My people will live in a peaceful habitation, in safe dwellings, and in quiet resting places,
en_uk_webbe~ISA~C32~19~though hail flattens the forest, and the city is levelled completely.
en_uk_webbe~ISA~C32~20~Blessed are you who sow beside all waters, who send out the feet of the ox and the donkey.
