en_uk_webbe~EXO~C25~01~The LORD spoke to Moses, saying,
en_uk_webbe~EXO~C25~02~“Speak to the children of Israel, that they take an offering for me. From everyone whose heart makes him willing you shall take my offering.
en_uk_webbe~EXO~C25~03~This is the offering which you shall take from them: gold, silver, bronze,
en_uk_webbe~EXO~C25~04~blue, purple, scarlet, fine linen, goats’ hair,
en_uk_webbe~EXO~C25~05~rams’ skins dyed red, sea cow hides, acacia wood,
en_uk_webbe~EXO~C25~06~oil for the light, spices for the anointing oil and for the sweet incense,
en_uk_webbe~EXO~C25~07~onyx stones, and stones to be set for the ephod and for the breastplate.
en_uk_webbe~EXO~C25~08~Let them make me a sanctuary, that I may dwell amongst them.
en_uk_webbe~EXO~C25~09~According to all that I show you, the pattern of the tabernacle, and the pattern of all of its furniture, even so you shall make it.
en_uk_webbe~EXO~C25~10~“They shall make an ark of acacia wood. Its length shall be two and a half cubits, its width a cubit and a half, and a cubit and a half its height.
en_uk_webbe~EXO~C25~11~You shall overlay it with pure gold. You shall overlay it inside and outside, and you shall make a gold moulding around it.
en_uk_webbe~EXO~C25~12~You shall cast four rings of gold for it, and put them in its four feet. Two rings shall be on the one side of it, and two rings on the other side of it.
en_uk_webbe~EXO~C25~13~You shall make poles of acacia wood, and overlay them with gold.
en_uk_webbe~EXO~C25~14~You shall put the poles into the rings on the sides of the ark to carry the ark.
en_uk_webbe~EXO~C25~15~The poles shall be in the rings of the ark. They shall not be taken from it.
en_uk_webbe~EXO~C25~16~You shall put the covenant which I shall give you into the ark.
en_uk_webbe~EXO~C25~17~You shall make a mercy seat of pure gold. Two and a half cubits shall be its length, and a cubit and a half its width.
en_uk_webbe~EXO~C25~18~You shall make two cherubim of hammered gold. You shall make them at the two ends of the mercy seat.
en_uk_webbe~EXO~C25~19~Make one cherub at the one end, and one cherub at the other end. You shall make the cherubim on its two ends of one piece with the mercy seat.
en_uk_webbe~EXO~C25~20~The cherubim shall spread out their wings upward, covering the mercy seat with their wings, with their faces towards one another. The faces of the cherubim shall be towards the mercy seat.
en_uk_webbe~EXO~C25~21~You shall put the mercy seat on top of the ark, and in the ark you shall put the covenant that I will give you.
en_uk_webbe~EXO~C25~22~There I will meet with you, and I will tell you from above the mercy seat, from between the two cherubim which are on the ark of the covenant, all that I command you for the children of Israel.
en_uk_webbe~EXO~C25~23~“You shall make a table of acacia wood. Its length shall be two cubits, and its width a cubit, and its height one and a half cubits.
en_uk_webbe~EXO~C25~24~You shall overlay it with pure gold, and make a gold moulding around it.
en_uk_webbe~EXO~C25~25~You shall make a rim of a hand width around it. You shall make a golden moulding on its rim around it.
en_uk_webbe~EXO~C25~26~You shall make four rings of gold for it, and put the rings in the four corners that are on its four feet.
en_uk_webbe~EXO~C25~27~the rings shall be close to the rim, for places for the poles to carry the table.
en_uk_webbe~EXO~C25~28~You shall make the poles of acacia wood, and overlay them with gold, that the table may be carried with them.
en_uk_webbe~EXO~C25~29~You shall make its dishes, its spoons, its ladles, and its bowls to pour out offerings with. You shall make them of pure gold.
en_uk_webbe~EXO~C25~30~You shall set bread of the presence on the table before me always.
en_uk_webbe~EXO~C25~31~“You shall make a lamp stand of pure gold. The lamp stand shall be made of hammered work. Its base, its shaft, its cups, its buds, and its flowers shall be of one piece with it.
en_uk_webbe~EXO~C25~32~There shall be six branches going out of its sides: three branches of the lamp stand out of its one side, and three branches of the lamp stand out of its other side;
en_uk_webbe~EXO~C25~33~three cups made like almond blossoms in one branch, a bud and a flower; and three cups made like almond blossoms in the other branch, a bud and a flower, so for the six branches going out of the lamp stand;
en_uk_webbe~EXO~C25~34~and in the lamp stand four cups made like almond blossoms, its buds and its flowers;
en_uk_webbe~EXO~C25~35~and a bud under two branches of one piece with it, and a bud under two branches of one piece with it, and a bud under two branches of one piece with it, for the six branches going out of the lamp stand.
en_uk_webbe~EXO~C25~36~Their buds and their branches shall be of one piece with it, all of it one beaten work of pure gold.
en_uk_webbe~EXO~C25~37~You shall make its lamps seven, and they shall light its lamps to give light to the space in front of it.
en_uk_webbe~EXO~C25~38~Its snuffers and its snuff dishes shall be of pure gold.
en_uk_webbe~EXO~C25~39~It shall be made of a talent of pure gold, with all these accessories.
en_uk_webbe~EXO~C25~40~See that you make them after their pattern, which has been shown to you on the mountain.
