en_uk_webbe~LEV~C06~01~The LORD spoke to Moses, saying,
en_uk_webbe~LEV~C06~02~“If anyone sins, and commits a trespass against the LORD, and deals falsely with his neighbour in a matter of deposit, or of bargain, or of robbery, or has oppressed his neighbour,
en_uk_webbe~LEV~C06~03~or has found that which was lost, and lied about it, and swearing to a lie—in any of these things that a man sins in his actions—
en_uk_webbe~LEV~C06~04~then it shall be, if he has sinned, and is guilty, he shall restore that which he took by robbery, or the thing which he has gotten by oppression, or the deposit which was committed to him, or the lost thing which he found,
en_uk_webbe~LEV~C06~05~or any thing about which he has sworn falsely: he shall restore it in full, and shall add a fifth part more to it. He shall return it to him to whom it belongs in the day of his being found guilty.
en_uk_webbe~LEV~C06~06~He shall bring his trespass offering to the LORD: a ram without defect from the flock, according to your estimation, for a trespass offering, to the priest.
en_uk_webbe~LEV~C06~07~The priest shall make atonement for him before the LORD, and he will be forgiven concerning whatever he does to become guilty.”
en_uk_webbe~LEV~C06~08~The LORD spoke to Moses, saying,
en_uk_webbe~LEV~C06~09~“Command Aaron and his sons, saying, ‘This is the law of the burnt offering: the burnt offering shall be on the hearth on the altar all night until the morning; and the fire of the altar shall be kept burning on it.
en_uk_webbe~LEV~C06~10~The priest shall put on his linen garment, and he shall put on his linen trousers upon his body; and he shall remove the ashes from where the fire has consumed the burnt offering on the altar, and he shall put them beside the altar.
en_uk_webbe~LEV~C06~11~He shall take off his garments, and put on other garments, and carry the ashes outside the camp to a clean place.
en_uk_webbe~LEV~C06~12~The fire on the altar shall be kept burning on it, it shall not go out; and the priest shall burn wood on it every morning. He shall lay the burnt offering in order upon it, and shall burn on it the fat of the peace offerings.
en_uk_webbe~LEV~C06~13~Fire shall be kept burning on the altar continually; it shall not go out.
en_uk_webbe~LEV~C06~14~“‘This is the law of the meal offering: the sons of Aaron shall offer it before the LORD, before the altar.
en_uk_webbe~LEV~C06~15~He shall take from there his handful of the fine flour of the meal offering, and of its oil, and all the frankincense which is on the meal offering, and shall burn it on the altar for a pleasant aroma, as its memorial portion, to the LORD.
en_uk_webbe~LEV~C06~16~That which is left of it Aaron and his sons shall eat. It shall be eaten without yeast in a holy place. They shall eat it in the court of the Tent of Meeting.
en_uk_webbe~LEV~C06~17~It shall not be baked with yeast. I have given it as their portion of my offerings made by fire. It is most holy, as are the sin offering and the trespass offering.
en_uk_webbe~LEV~C06~18~Every male amongst the children of Aaron shall eat of it, as their portion forever throughout your generations, from the offerings of the LORD made by fire. Whoever touches them shall be holy.’”
en_uk_webbe~LEV~C06~19~The LORD spoke to Moses, saying,
en_uk_webbe~LEV~C06~20~“This is the offering of Aaron and of his sons, which they shall offer to the LORD in the day when he is anointed: one tenth of an ephah of fine flour for a meal offering perpetually, half of it in the morning, and half of it in the evening.
en_uk_webbe~LEV~C06~21~It shall be made with oil in a griddle. When it is soaked, you shall bring it in. You shall offer the meal offering in baked pieces for a pleasant aroma to the LORD.
en_uk_webbe~LEV~C06~22~The anointed priest that will be in his place from amongst his sons shall offer it. By a statute forever, it shall be wholly burnt to the LORD.
en_uk_webbe~LEV~C06~23~Every meal offering of a priest shall be wholly burnt. It shall not be eaten.”
en_uk_webbe~LEV~C06~24~The LORD spoke to Moses, saying,
en_uk_webbe~LEV~C06~25~“Speak to Aaron and to his sons, saying, ‘This is the law of the sin offering: in the place where the burnt offering is killed, the sin offering shall be killed before the LORD. It is most holy.
en_uk_webbe~LEV~C06~26~The priest who offers it for sin shall eat it. It shall be eaten in a holy place, in the court of the Tent of Meeting.
en_uk_webbe~LEV~C06~27~Whatever shall touch its flesh shall be holy. When there is any of its blood sprinkled on a garment, you shall wash that on which it was sprinkled in a holy place.
en_uk_webbe~LEV~C06~28~But the earthen vessel in which it is boiled shall be broken; and if it is boiled in a bronze vessel, it shall be scoured, and rinsed in water.
en_uk_webbe~LEV~C06~29~Every male amongst the priests shall eat of it. It is most holy.
en_uk_webbe~LEV~C06~30~No sin offering, of which any of the blood is brought into the Tent of Meeting to make atonement in the Holy Place, shall be eaten. It shall be burnt with fire.
