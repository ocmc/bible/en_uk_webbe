en_uk_webbe~JER~C05~01~“Run back and forth through the streets of Jerusalem, and see now, and know, and seek in its wide places, if you can find a man, if there is anyone who does justly, who seeks truth, then I will pardon her.
en_uk_webbe~JER~C05~02~Though they say, ‘As the LORD lives,’ surely they swear falsely.”
en_uk_webbe~JER~C05~03~O LORD, don’t your eyes look on truth? You have stricken them, but they were not grieved. You have consumed them, but they have refused to receive correction. They have made their faces harder than a rock. They have refused to return.
en_uk_webbe~JER~C05~04~Then I said, “Surely these are poor. They are foolish; for they don’t know the the LORD’s way, nor the law of their God.
en_uk_webbe~JER~C05~05~I will go to the great men and will speak to them, for they know the way of the LORD, and the law of their God.” But these with one accord have broken the yoke, and burst the bonds.
en_uk_webbe~JER~C05~06~Therefore a lion out of the forest will kill them. A wolf of the evenings will destroy them. A leopard will watch against their cities. Everyone who goes out there will be torn in pieces, because their transgressions are many and their backsliding has increased.
en_uk_webbe~JER~C05~07~“How can I pardon you? Your children have forsaken me, and sworn by what are no gods. When I had fed them to the full, they committed adultery, and assembled themselves in troops at the prostitutes’ houses.
en_uk_webbe~JER~C05~08~They were as fed horses roaming at large. Everyone neighed after his neighbour’s wife.
en_uk_webbe~JER~C05~09~Shouldn’t I punish them for these things?” says the LORD. “Shouldn’t my soul be avenged on such a nation as this?
en_uk_webbe~JER~C05~10~“Go up on her walls, and destroy; but don’t make a full end. Take away her branches, for they are not the LORD’s.
en_uk_webbe~JER~C05~11~For the house of Israel and the house of Judah have dealt very treacherously against me,” says the LORD.
en_uk_webbe~JER~C05~12~They have denied the LORD, and said, “It is not he. Evil will won’t come on us. We won’t see sword or famine.
en_uk_webbe~JER~C05~13~The prophets will become wind, and the word is not in them. Thus it will be done to them.”
en_uk_webbe~JER~C05~14~Therefore the LORD, the God of Armies says, “Because you speak this word, behold, I will make my words in your mouth fire, and this people wood, and it will devour them.
en_uk_webbe~JER~C05~15~Behold, I will bring a nation on you from far away, house of Israel,” says the LORD. “It is a mighty nation. It is an ancient nation, a nation whose language you don’t know and don’t understand what they say.
en_uk_webbe~JER~C05~16~Their quiver is an open tomb. They are all mighty men.
en_uk_webbe~JER~C05~17~They will eat up your harvest and your bread, which your sons and your daughters should eat. They will eat up your flocks and your herds. They will eat up your vines and your fig trees. They will beat down your fortified cities in which you trust with the sword.
en_uk_webbe~JER~C05~18~“But even in those days,” says the LORD, “I will not make a full end of you.
en_uk_webbe~JER~C05~19~It will happen when you say, ‘Why has the LORD our God done all these things to us?’ Then you shall say to them, ‘Just as you have forsaken me and served foreign gods in your land, so you will serve strangers in a land that is not yours.’
en_uk_webbe~JER~C05~20~“Declare this in the house of Jacob, and publish it in Judah, saying,
en_uk_webbe~JER~C05~21~‘Hear this now, foolish people without understanding, who have eyes, and don’t see, who have ears, and don’t hear:
en_uk_webbe~JER~C05~22~Don’t you fear me?’ says the LORD ‘Won’t you tremble at my presence, who have placed the sand for the bound of the sea, by a perpetual decree, that it can’t pass it? Though its waves toss themselves, yet they can’t prevail. Though they roar, they still can’t pass over it.’
en_uk_webbe~JER~C05~23~“But this people has a revolting and a rebellious heart. They have revolted and gone.
en_uk_webbe~JER~C05~24~They don’t say in their heart, ‘Let’s now fear the LORD our God, who gives rain, both the former and the latter, in its season, who preserves to us the appointed weeks of the harvest.’
en_uk_webbe~JER~C05~25~“Your iniquities have turned away these things, and your sins have withheld good from you.
en_uk_webbe~JER~C05~26~For wicked men are found amongst my people. They watch, as fowlers lie in wait. They set a trap. They catch men.
en_uk_webbe~JER~C05~27~As a cage is full of birds, so are their houses full of deceit. Therefore they have become great, and grew rich.
en_uk_webbe~JER~C05~28~They have grown fat. They shine; yes, they excel in deeds of wickedness. They don’t plead the cause, the cause of the fatherless, that they may prosper; and they don’t defend the rights of the needy.
en_uk_webbe~JER~C05~29~“Shouldn’t I punish for these things?” says the LORD. “Shouldn’t my soul be avenged on such a nation as this?
en_uk_webbe~JER~C05~30~“An astonishing and horrible thing has happened in the land.
en_uk_webbe~JER~C05~31~The prophets prophesy falsely, and the priests rule by their own authority; and my people love to have it so. What will you do in the end of it?
