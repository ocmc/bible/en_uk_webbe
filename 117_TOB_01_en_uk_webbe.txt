en_uk_webbe~TOB~C01~01~The book of the words of Tobit, the son of Tobiel, the son of Ananiel, the son of Aduel, the son of Gabael, of the seed of Asiel, of the tribe of Naphtali;
en_uk_webbe~TOB~C01~02~who in the days of Enemessar king of the Assyrians was carried away captive out of Thisbe, which is on the right hand of Kedesh Naphtali in Galilee above Asher.
en_uk_webbe~TOB~C01~03~I, Tobit walked in the ways of truth and righteousness all the days of my life, and I did many alms deeds to my kindred and my nation, who went with me into the land of the Assyrians, to Nineveh.
en_uk_webbe~TOB~C01~04~When I was in my own country, in the land of Israel, while I was yet young, all the tribe of Naphtali my father fell away from the house of Jerusalem, which was chosen out of all the tribes of Israel, that all the tribes should sacrifice there, and the temple of the habitation of the Most High was hallowed and built therein for all ages.
en_uk_webbe~TOB~C01~05~All the tribes which fell away together sacrificed to the heifer Baal, and so did the house of Naphtali my father.
en_uk_webbe~TOB~C01~06~I alone went often to Jerusalem at the feasts, as it has been ordained to all Israel by an everlasting decree, having the first fruits and the tenths of my increase, and that which was first shorn; and I gave them at the altar to the priests the sons of Aaron.
en_uk_webbe~TOB~C01~07~I gave a tenth part of all my increase to the sons of Levi, who ministered at Jerusalem. A second tenth part I sold away, and went, and spent it each year at Jerusalem.
en_uk_webbe~TOB~C01~08~A third tenth I gave to them to whom it was appropriate, as Deborah my father’s mother had commanded me, because I was left an orphan by my father.
en_uk_webbe~TOB~C01~09~When I became a man, I took as wife Anna of the seed of our own family. With her, I became the father of Tobias.
en_uk_webbe~TOB~C01~10~When I was carried away captive to Nineveh, all my kindred and my relatives ate of the bread of the Gentiles;
en_uk_webbe~TOB~C01~11~but I kept myself from eating,
en_uk_webbe~TOB~C01~12~because I remembered God with all my soul.
en_uk_webbe~TOB~C01~13~So the Most High gave me grace and favour in the sight of Enemessar, and I was his purchasing agent.
en_uk_webbe~TOB~C01~14~And I went into Media, and left ten talents of silver in trust with Gabael, the brother of Gabrias, at Rages of Media.
en_uk_webbe~TOB~C01~15~And when Enemessar was dead, Sennacherib his son reigned in his place. In his time, the highways were troubled, and I could no longer go into Media.
en_uk_webbe~TOB~C01~16~In the days of Enemessar, I did many alms deeds to my kindred: I gave my bread to the hungry,
en_uk_webbe~TOB~C01~17~and my garments to the naked. If I saw any of my race dead, and thrown out on the wall of Ninevah, I buried him.
en_uk_webbe~TOB~C01~18~If Sennacherib the king killed any, when he came fleeing from Judea, I buried them privately; for in his wrath he killed many; and the bodies were sought for by the king, and were not found.
en_uk_webbe~TOB~C01~19~But one of the Ninevites went and showed to the king concerning me, how I buried them, and hid myself; and when I knew that I was sought for to be put to death, I withdrew myself for fear.
en_uk_webbe~TOB~C01~20~And all my goods were forcibly taken away, and there was nothing left to me, save my wife Anna and my son Tobias.
en_uk_webbe~TOB~C01~21~No more than fifty five days passed before two of his sons killed him, and they fled into the mountains of Ararat. And Sarchedonus his son reigned in his place; and he appointed Achiacharus my brother Anael’s son over all the accounts of his kingdom, and over all his affairs.
en_uk_webbe~TOB~C01~22~Achiacharus requested me, and I came to Nineveh. Now Achiacharus was cupbearer, keeper of the signet, steward, and overseer of the accounts. Sarchedonus appointed him next to himself, but he was my brother’s son.
