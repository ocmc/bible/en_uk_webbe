en_uk_webbe~MA1~C01~01~It came to pass, after that Alexander the Macedonian, the son of Philip, who came out of the land of Chittim, and struck Darius king of the Persians and Medes, [it came to pass, ] after he had struck him, that he reigned in his stead, in former time, over Greece.
en_uk_webbe~MA1~C01~02~And he fought many battles, and won many strongholds, and killed the kings of the earth,
en_uk_webbe~MA1~C01~03~and went through to the ends of the earth, and took spoils of a multitude of nations. And the earth was quiet before him, and he was exalted, and his heart was lifted up,
en_uk_webbe~MA1~C01~04~and he gathered together an exceedingly strong army, and ruled over countries and nations and principalities, and they became tributary to him.
en_uk_webbe~MA1~C01~05~And after these things he fell sick, and perceived that he should die.
en_uk_webbe~MA1~C01~06~And he called his servants, which were honourable, which had been brought up with him from his youth, and he divided to them his kingdom, while he was yet alive.
en_uk_webbe~MA1~C01~07~And Alexander reigned twelve years, and he died.
en_uk_webbe~MA1~C01~08~And his servants bare rule, each one in his place.
en_uk_webbe~MA1~C01~09~And they did all put diadems upon themselves after that he was dead, and so did their sons after them many years: and they multiplied evils in the earth.
en_uk_webbe~MA1~C01~10~And there came forth out of them a sinful root, Antiochus Epiphanes, son of Antiochus the king, who had been a hostage at Rome, and he reigned in the hundred and thirty and seventh year of the kingdom of the Greeks.
en_uk_webbe~MA1~C01~11~In those days came there forth out of Israel transgressors of the law, and persuaded many, saying, let’s go and make a covenant with the Gentiles that are round about us; for since we were parted from them many evils have befallen us.
en_uk_webbe~MA1~C01~12~And the saying was good in their eyes.
en_uk_webbe~MA1~C01~13~And certain of the people were forward [herein ] and went to the king, and he gave them licence to do after the ordinances of the Gentiles.
en_uk_webbe~MA1~C01~14~And they built a place of exercise in Jerusalem according to the laws of the Gentiles;
en_uk_webbe~MA1~C01~15~and they made themselves uncircumcised, and forsook the holy covenant, and joined themselves to the Gentiles, and sold themselves to do evil.
en_uk_webbe~MA1~C01~16~And the kingdom was well ordered in the sight of Antiochus, and he thought to reign over Egypt, that he might reign over the two kingdoms.
en_uk_webbe~MA1~C01~17~And he entered into Egypt with a great multitude, with chariots, and with elephants, and with horsemen, and with a great navy;
en_uk_webbe~MA1~C01~18~and he made war against Ptolemy king of Egypt; and Ptolemy was put to shame before him, and fled; and many fell wounded to death.
en_uk_webbe~MA1~C01~19~And they got possession of the strong cities in the land of Egypt; and he took the spoils of Egypt.
en_uk_webbe~MA1~C01~20~And Antiochus, after he had struck Egypt, returned in the hundred and forty and third year, and went up against Israel and Jerusalem with a great multitude,
en_uk_webbe~MA1~C01~21~and entered presumptuously into the sanctuary, and took the golden altar, and the candlestick of the light, and all that pertained thereto,
en_uk_webbe~MA1~C01~22~and the table of the show bread, and the cups to pour withal, and the bowls, and the golden censers, and the veil, and the crowns, and the adorning of gold which was on the face of the temple, and he peeled it all off.
en_uk_webbe~MA1~C01~23~And he took the silver and the gold and the precious vessels; and he took the hidden treasures which he found.
en_uk_webbe~MA1~C01~24~And when he had taken all, he went away into his own land, and he made a great slaughter, and spoke very presumptuously.
en_uk_webbe~MA1~C01~25~And there came great mourning upon Israel, in every place where they were;
en_uk_webbe~MA1~C01~26~and the rulers and elders groaned, the virgins and young men were made feeble, and the beauty of the women was changed.
en_uk_webbe~MA1~C01~27~Every bridegroom took up lamentation, she that sat in the marriage chamber was in heaviness.
en_uk_webbe~MA1~C01~28~And the land was moved for the inhabitants thereof, and all the house of Jacob was clothed with shame.
en_uk_webbe~MA1~C01~29~And after two full years the king sent a chief collector of tribute to the cities of Judah, and he came to Jerusalem with a great multitude.
en_uk_webbe~MA1~C01~30~And he spoke words of peace to them in subtlety, and they gave him credence: and he fell upon the city suddenly, and struck it very sore, and destroyed much people out of Israel.
en_uk_webbe~MA1~C01~31~And he took the spoils of the city, and set it on fire, and pulled down the houses thereof and the walls thereof on every side.
en_uk_webbe~MA1~C01~32~And they led captive the women and the children, and the cattle they took in possession.
en_uk_webbe~MA1~C01~33~And they built the city of David with a great and strong wall, with strong towers, and it became to them a citadel.
en_uk_webbe~MA1~C01~34~And they put there a sinful nation, transgressors of the law, and they strengthened themselves therein.
en_uk_webbe~MA1~C01~35~And they stored up arms and food, and gathering together the spoils of Jerusalem, they laid them up there, and they became a sore snare:
en_uk_webbe~MA1~C01~36~and it became a place to lie in wait in against the sanctuary, and an evil adversary to Israel continually.
en_uk_webbe~MA1~C01~37~And they shed innocent blood on every side of the sanctuary, and defiled the sanctuary.
en_uk_webbe~MA1~C01~38~And the inhabitants of Jerusalem fled because of them; and she became a habitation of strangers, and she became strange to those who were born in her, and her children forsook her.
en_uk_webbe~MA1~C01~39~Her sanctuary was laid waste like a wilderness, her feasts were turned into mourning, her Sabbaths into reproach, her honour into contempt.
en_uk_webbe~MA1~C01~40~According to her glory, so was her dishonour multiplied, and her high estate was turned into mourning.
en_uk_webbe~MA1~C01~41~And king Antiochus wrote to his whole kingdom, that all should be one people,
en_uk_webbe~MA1~C01~42~and that each should forsake his own laws. And all the nations agreed according to the word of the king;
en_uk_webbe~MA1~C01~43~and many of Israel consented to his worship, and sacrificed to the idols, and profaned the Sabbath.
en_uk_webbe~MA1~C01~44~And the king sent letters by the hand of messengers to Jerusalem and the cities of Judah, that they should follow laws strange to the land,
en_uk_webbe~MA1~C01~45~and should forbid whole burnt offerings and sacrifice and drink offerings in the sanctuary; and should profane the Sabbaths and feasts,
en_uk_webbe~MA1~C01~46~and pollute the sanctuary and those who were holy;
en_uk_webbe~MA1~C01~47~that they should build altars, and temples, and shrines for idols, and should sacrifice swine’s flesh and unclean beasts:
en_uk_webbe~MA1~C01~48~and that they should leave their sons uncircumcised, that they should make their souls abominable with all manner of uncleanness and profanation;
en_uk_webbe~MA1~C01~49~so that they might forget the law, and change all the ordinances.
en_uk_webbe~MA1~C01~50~And whoever shall not do according to the word of the king, he shall die.
en_uk_webbe~MA1~C01~51~According to all these words wrote he to his whole kingdom; and he appointed overseers over all the people, and he commanded the cities of Judah to sacrifice, city by city.
en_uk_webbe~MA1~C01~52~And from the people were gathered together to them many, every one that had forsaken the law; and they did evil things in the land;
en_uk_webbe~MA1~C01~53~and they made israel to hide themselves in every place of refuge which they had.
en_uk_webbe~MA1~C01~54~And on the fifteenth day of Chislev, in the hundred and forty and fifth year, they built an abomination of desolation upon the altar, and in the cities of Judah on every side they built [idol ] altars.
en_uk_webbe~MA1~C01~55~And at the doors of the houses and in the streets they burnt incense.
en_uk_webbe~MA1~C01~56~And they tore in pieces the books of the law which they found, and set them on fire.
en_uk_webbe~MA1~C01~57~And wherever was found with any a book of the covenant, and if any consented to the law, the king’s sentence delivered him to death.
en_uk_webbe~MA1~C01~58~Thus did they in their might to Israel, to those that were found month by month in the cities.
en_uk_webbe~MA1~C01~59~And on the five and twentieth day of the month they sacrificed upon the [idol ] altar [of God. ]
en_uk_webbe~MA1~C01~60~And the women that had circumcised their children they put to death according to the commandment.
en_uk_webbe~MA1~C01~61~And they hanged their babes about their necks, and [destroyed ] their houses, and those who had circumcised them.
en_uk_webbe~MA1~C01~62~And many in Israel were fully resolved and confirmed in themselves not to eat unclean things.
en_uk_webbe~MA1~C01~63~And they chose to die, that they might not be defiled with the meats, and that they might not profane the holy covenant: and they died.
en_uk_webbe~MA1~C01~64~And there came exceedingly great wrath upon Israel.
