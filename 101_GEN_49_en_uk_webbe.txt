en_uk_webbe~GEN~C49~01~Jacob called to his sons, and said: “Gather yourselves together, that I may tell you that which will happen to you in the days to come.
en_uk_webbe~GEN~C49~02~Assemble yourselves, and hear, you sons of Jacob. Listen to Israel, your father.
en_uk_webbe~GEN~C49~03~“Reuben, you are my firstborn, my might, and the beginning of my strength; excelling in dignity, and excelling in power.
en_uk_webbe~GEN~C49~04~Boiling over like water, you shall not excel; because you went up to your father’s bed, then defiled it. He went up to my couch.
en_uk_webbe~GEN~C49~05~“Simeon and Levi are brothers. Their swords are weapons of violence.
en_uk_webbe~GEN~C49~06~My soul, don’t come into their council. My glory, don’t be united to their assembly; for in their anger they killed men. In their self-will they hamstrung cattle.
en_uk_webbe~GEN~C49~07~Cursed be their anger, for it was fierce; and their wrath, for it was cruel. I will divide them in Jacob, and scatter them in Israel.
en_uk_webbe~GEN~C49~08~“Judah, your brothers will praise you. Your hand will be on the neck of your enemies. Your father’s sons will bow down before you.
en_uk_webbe~GEN~C49~09~Judah is a lion’s cub. From the prey, my son, you have gone up. He stooped down, he crouched as a lion, as a lioness. Who will rouse him up?
en_uk_webbe~GEN~C49~10~The sceptre will not depart from Judah, nor the ruler’s staff from between his feet, until he comes to whom it belongs. To him will the obedience of the peoples be.
en_uk_webbe~GEN~C49~11~Binding his foal to the vine, his donkey’s colt to the choice vine; he has washed his garments in wine, his robes in the blood of grapes.
en_uk_webbe~GEN~C49~12~His eyes will be red with wine, his teeth white with milk.
en_uk_webbe~GEN~C49~13~“Zebulun will dwell at the haven of the sea. He will be for a haven of ships. His border will be on Sidon.
en_uk_webbe~GEN~C49~14~“Issachar is a strong donkey, lying down between the saddlebags.
en_uk_webbe~GEN~C49~15~He saw a resting place, that it was good, the land, that it was pleasant. He bows his shoulder to the burden, and becomes a servant doing forced labour.
en_uk_webbe~GEN~C49~16~“Dan will judge his people, as one of the tribes of Israel.
en_uk_webbe~GEN~C49~17~Dan will be a serpent on the trail, an adder in the path, that bites the horse’s heels, so that his rider falls backward.
en_uk_webbe~GEN~C49~18~I have waited for your salvation, LORD.
en_uk_webbe~GEN~C49~19~“A troop will press on Gad, but he will press on their heel.
en_uk_webbe~GEN~C49~20~“Asher’s food will be rich. He will produce royal dainties.
en_uk_webbe~GEN~C49~21~“Naphtali is a doe set free, who bears beautiful fawns.
en_uk_webbe~GEN~C49~22~“Joseph is a fruitful vine, a fruitful vine by a spring. His branches run over the wall.
en_uk_webbe~GEN~C49~23~The archers have severely grieved him, shot at him, and persecuted him:
en_uk_webbe~GEN~C49~24~But his bow remained strong. The arms of his hands were made strong, by the hands of the Mighty One of Jacob, (from there is the shepherd, the stone of Israel),
en_uk_webbe~GEN~C49~25~even by the God of your father, who will help you, by the Almighty, who will bless you, with blessings of heaven above, blessings of the deep that lies below, blessings of the breasts, and of the womb.
en_uk_webbe~GEN~C49~26~The blessings of your father have prevailed above the blessings of your ancestors, above the boundaries of the ancient hills. They will be on the head of Joseph, on the crown of the head of him who is separated from his brothers.
en_uk_webbe~GEN~C49~27~“Benjamin is a ravenous wolf. In the morning he will devour the prey. At evening he will divide the plunder.”
en_uk_webbe~GEN~C49~28~All these are the twelve tribes of Israel, and this is what their father spoke to them, and blessed them. He blessed everyone according to his own blessing.
en_uk_webbe~GEN~C49~29~He instructed them, and said to them, “I am to be gathered to my people. Bury me with my fathers in the cave that is in the field of Ephron the Hittite,
en_uk_webbe~GEN~C49~30~in the cave that is in the field of Machpelah, which is before Mamre, in the land of Canaan, which Abraham bought with the field from Ephron the Hittite as a burial place.
en_uk_webbe~GEN~C49~31~There they buried Abraham and Sarah, his wife. There they buried Isaac and Rebekah, his wife, and there I buried Leah:
en_uk_webbe~GEN~C49~32~the field and the cave that is therein, which was purchased from the children of Heth.”
en_uk_webbe~GEN~C49~33~When Jacob finished charging his sons, he gathered up his feet into the bed, breathed his last breath, and was gathered to his people.
