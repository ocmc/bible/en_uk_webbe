en_uk_webbe~SIR~C50~01~[It was ] Simon, the son of Onias, the great priest, Who in his life repaired the house, And in his days strengthened the temple:
en_uk_webbe~SIR~C50~02~And by him was built from the foundation the height of the double [wall, ] The lofty underworks of the inclosure of the temple:
en_uk_webbe~SIR~C50~03~In his days the cistern of waters was diminished, The brazen vessel in compass as the sea.
en_uk_webbe~SIR~C50~04~[It was ] he that took thought for his people that they should not fall, And fortified the city against besieging:
en_uk_webbe~SIR~C50~05~How glorious was he when the people gathered round him At his coming forth out of the sanctuary!
en_uk_webbe~SIR~C50~06~As the morning star in the midst of a cloud, As the moon at the full:
en_uk_webbe~SIR~C50~07~As the sun shining forth upon the temple of the Most High, And as the rainbow giving light in clouds of glory:
en_uk_webbe~SIR~C50~08~As the flower of roses in the days of new [fruits, ] As lilies at the water spring, As the shoot of the frankincense tree in the time of summer:
en_uk_webbe~SIR~C50~09~As fire and incense in the censer, As a vessel all of beaten gold Adorned with all manner of precious stones:
en_uk_webbe~SIR~C50~10~As an olive tree budding forth fruits, And as a cypress growing high amongst the clouds.
en_uk_webbe~SIR~C50~11~When he took up the robe of glory, And put on the perfection of exultation, In the ascent of the holy altar, He made glorious the precinct of the sanctuary.
en_uk_webbe~SIR~C50~12~And when he received the portions out of the priests’ hands, Himself also standing by the hearth of the altar, His kindred as a garland round about him, He was as a young cedar in Libanus; And as stems of palm trees compassed they him round about,
en_uk_webbe~SIR~C50~13~And all the sons of Aaron in their glory, And the Lord’s offering in their hands, before all the congregation of Israel.
en_uk_webbe~SIR~C50~14~And finishing the service at the altars, That he might adorn the offering of the Most High, the Almighty,
en_uk_webbe~SIR~C50~15~He stretched out his hand to the cup, And poured out the cup of the grape; He poured out at the foot of the altar A sweet smelling savour to the Most High, the King of all.
en_uk_webbe~SIR~C50~16~Then shouted the sons of Aaron, They sounded the trumpets of beaten work, They made a great noise to be heard, For a remembrance before the Most High.
en_uk_webbe~SIR~C50~17~Then all the people together hurried, And fell down upon the earth on their faces To worship their Lord, the Almighty, God Most High.
en_uk_webbe~SIR~C50~18~The singers also praised him with their voices; In the whole house was there made sweet melody.
en_uk_webbe~SIR~C50~19~And the people implored the Lord Most High, In prayer before him that is merciful. Till the worship of the Lord should be ended; And so they accomplished his service.
en_uk_webbe~SIR~C50~20~Then he went down, and lifted up his hands Over the whole congregation of the children of Israel, To give blessing to the Lord with his lips, And to glory in his name.
en_uk_webbe~SIR~C50~21~And he bowed himself down in worship the second time, To declare the blessing from the Most High.
en_uk_webbe~SIR~C50~22~And now bless you° the God of all, Which everywhere does great things, Which exalts our days from the womb, And deals with us according to his mercy.
en_uk_webbe~SIR~C50~23~May he grant us joyfulness of heart, And that peace may be in our days in Israel for the days of eternity:
en_uk_webbe~SIR~C50~24~To entrust his mercy with us; And let him deliver us in his time!
en_uk_webbe~SIR~C50~25~With two nations is my soul vexed, And the third is no nation:
en_uk_webbe~SIR~C50~26~They that sit upon the mountain of Samaria, [and ] the Philistines, And that foolish people that dwells in Sichem.
en_uk_webbe~SIR~C50~27~I have written in this book the instruction of understanding and knowledge, I Jesus, the son of Sirach Eleazar, of Jerusalem, Who out of his heart poured forth wisdom.
en_uk_webbe~SIR~C50~28~Blessed is he that shall be exercised in these things; And he that lays them up in his heart shall become wise.
en_uk_webbe~SIR~C50~29~For if he do them, he shall be strong to all things: For the light of the Lord is his guide.
