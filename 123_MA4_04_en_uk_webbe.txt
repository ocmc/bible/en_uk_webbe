en_uk_webbe~MA4~C04~01~For a certain man named Simon, who was in opposition to Onias, who once held the high priesthood for life, and was an honourable and good man, after slandering him in every way, he could not injure him with the people, went away as an exile, with the intention of betraying his country.
en_uk_webbe~MA4~C04~02~Whence coming to Apollonius, the military governor of Syria, and Phoenicia, and Cilicia, he said,
en_uk_webbe~MA4~C04~03~Having good will to the king’s affairs, I am come to inform you that infinite private wealth is laid up in the treasuries of Jerusalem which do not belong to the temple, but pertain to king Seleucus.
en_uk_webbe~MA4~C04~04~Apollonius, acquainting himself with the particulars of this, praised Simon for his care of the king’s interests, and going up to Seleucus informed him of the treasure;
en_uk_webbe~MA4~C04~05~and getting authority about it, and quickly advancing into our country with the accursed Simon and a very heavy force,
en_uk_webbe~MA4~C04~06~he said that he came with the commands of the king that he should take the private money of the treasure.
en_uk_webbe~MA4~C04~07~And the nation, indignant at this proclamation, and replying to the effect that it was extremely unfair that those who had committed deposits to the sacred treasury should be deprived of them, resisted as well as they could.
en_uk_webbe~MA4~C04~08~But Appolonius went away with threats into the temple.
en_uk_webbe~MA4~C04~09~And the priests, with the women and children, having supplicated God to throw his shield over the holy, despised place,
en_uk_webbe~MA4~C04~10~and Appolonius going up with his armed force to the seizure of the treasure, —there appeared from heaven angels riding on horseback, all radiant in armour, filling them with much fear and trembling.
en_uk_webbe~MA4~C04~11~And Apollonius fell half dead upon the court which is open to all nations, and extended his hands to heaven, and implored the Hebrews, with tears, to pray for him, and propitiate the heavenly army.
en_uk_webbe~MA4~C04~12~For he said that he had sinned, so as to be consequently worthy of death; and that if he were saved, he would celebrate to all men the blessedness of the holy place.
en_uk_webbe~MA4~C04~13~Onias the high priest, induced by these words, although for other reasons anxious that king Seleucus should not suppose that Apollonius was slain by human device and not by Divine punishment, prayed for him;
en_uk_webbe~MA4~C04~14~and he being thus unexpectedly saved, departed to manifest to the king what had happened to him.
en_uk_webbe~MA4~C04~15~But on the death of Seleucus the king, his son Antiochus Epiphanes succeeds to the kingdom: a man of arrogant pride and terrible.
en_uk_webbe~MA4~C04~16~Who having deposed Onias from the high priesthood, appointed his brother Jason to be high priest:
en_uk_webbe~MA4~C04~17~who had made a covenant, if he would give him this authority, to pay yearly three thousand and six hundred and sixty talents.
en_uk_webbe~MA4~C04~18~And he committed to him the high priesthood and rulership over the nation.
en_uk_webbe~MA4~C04~19~And he both changed the manner of living of the people, and perverted their civil customs into all lawlessness.
en_uk_webbe~MA4~C04~20~So that he not only erected a gymnasium on the very citadel of our country, [but neglected] the guardianship of the temple.
en_uk_webbe~MA4~C04~21~At which Divine vengeance being grieved, instigated Antiochus himself against them.
en_uk_webbe~MA4~C04~22~For being at war with Ptolemy in Egypt, he heard that on a report of his death being spread abroad, the inhabitants of Jerusalem had exceedingly rejoiced, and he quickly marched against them.
en_uk_webbe~MA4~C04~23~And having subdued them, he established a decree that if any of them lived according to the laws of his country he should die.
en_uk_webbe~MA4~C04~24~And when he could by no means destroy by his decrees the obedience to the law of the nation, but saw all his threats and punishments without effect,
en_uk_webbe~MA4~C04~25~for even women, because they continued to circumcise their children, were flung down a precipice along with them, knowing beforehand of the punishment.
en_uk_webbe~MA4~C04~26~When, therefore, his decrees were disregarded by the people, he himself compelled by means of tortures every one of this race, by tasting forbidden meats, to renounce the Jewish religion.
