en_uk_webbe~MA4~C08~01~Then, indeed, vehemently swayed with passion, he commanded to bring others of the adult Hebrews, and if they would eat of the unclean thing, to let them go when they had eaten; but if they objected, to torment them more grievously.
en_uk_webbe~MA4~C08~02~The tyrant having given this charge, seven kindred were brought into his presence, along with their aged mother, handsome, and modest, and well-born, and altogether comely.
en_uk_webbe~MA4~C08~03~When the tyrant saw them encircling their mother as in a dance, he was pleased at them; and being struck with their becoming and innocent manner, smiled upon them, and calling them near, said:
en_uk_webbe~MA4~C08~04~O youths, with favourable feelings, I admire the beauty of each of you; and greatly honouring so numerous a band of kindred, I not only counsel you not to share the madness of the old man who has been tortured before,
en_uk_webbe~MA4~C08~05~but I do beg you to yield, and to enjoy my friendship; for I possess the power, not only of punishing those who disobey my commands, but of doing good to those who obey them.
en_uk_webbe~MA4~C08~06~Put confidence in me, then, and you shall receive places of authority in my government, if you forsake your national ordinance,
en_uk_webbe~MA4~C08~07~and, conforming to the Greek mode of life, alter your rule, and revel in youth’s delights.
en_uk_webbe~MA4~C08~08~For if you provoke me by your disobedience, you will compel me to destroy you, every one, with terrible punishments by tortures.
en_uk_webbe~MA4~C08~09~Have mercy, then, upon your own selves, whom I, although an enemy, compassionate for your age and attractive appearance.
en_uk_webbe~MA4~C08~10~Will you not reason upon this—that if you disobey, there will be nothing left for you but to die in tortures?
en_uk_webbe~MA4~C08~11~Thus speaking, he ordered the instruments of torture to be brought forward, that very fear might prevail upon them to eat unclean meat.
en_uk_webbe~MA4~C08~12~And when the spearman brought forward the wheels, and the racks, and the hooks, and catapults, and cauldrons, pans, and finger-racks, and iron hands and wedges, and bellows, the tyrant continue:
en_uk_webbe~MA4~C08~13~Fear, young men, and the righteousness which you° worship will be merciful to you if you err from compulsion.
en_uk_webbe~MA4~C08~14~Now they having listened to these words of persuasion, and seeing the fearful instruments, not only were not afraid, but even answered the arguments of the tyrant, and through their good reasoning destroyed his power.
en_uk_webbe~MA4~C08~15~Now let’s consider the matter: had any of them been weak-spirited and cowardly amongst them, what reasoning would they have employed but these?
en_uk_webbe~MA4~C08~16~O wretched that we are, and exceedingly senseless! when the king exhorts us, and calls us to his bounty, should we not obey him?
en_uk_webbe~MA4~C08~17~Why do we cheer ourselves with vain counsels, and venture upon a disobedience bringing death?
en_uk_webbe~MA4~C08~18~Shall we not fear, O kindred, the instruments of torture and weigh the threatenings of torment and shun this vain-glory and destructive pride?
en_uk_webbe~MA4~C08~19~Let’s have compassion upon our age and relent over the years of our mother.
en_uk_webbe~MA4~C08~20~And let’s bear in mind that we shall be dying as rebels.
en_uk_webbe~MA4~C08~21~And Divine Justice will pardon us if we fear the king through necessity.
en_uk_webbe~MA4~C08~22~Why withdraw ourselves from a most sweet life, and deprive ourselves of this pleasant world?
en_uk_webbe~MA4~C08~23~Let’s not oppose necessity, nor seek vain-glory by our own excruciation.
en_uk_webbe~MA4~C08~24~The law itself is not forward to put us to death, if we dread torture.
en_uk_webbe~MA4~C08~25~Whence has such angry zeal taken root in us, and such fatal obstinacy approved itself to us, when we might live unmolested by the king?
en_uk_webbe~MA4~C08~26~But nothing of this kind did the young men say or think when about to be tortured.
en_uk_webbe~MA4~C08~27~For they were well aware of the sufferings, and masters of the pains.
en_uk_webbe~MA4~C08~28~So that as soon as the tyrant had ceased counselling them to eat the unclean, they altogether with one voice, as from the same heart said:
