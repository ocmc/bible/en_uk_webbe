en_uk_webbe~PSA~C096~001~(C097:001) The LORD reigns! Let the earth rejoice! Let the multitude of islands be glad!
en_uk_webbe~PSA~C096~002~(C097:002) Clouds and darkness are around him. Righteousness and justice are the foundation of his throne.
en_uk_webbe~PSA~C096~003~(C097:003) A fire goes before him, and burns up his adversaries on every side.
en_uk_webbe~PSA~C096~004~(C097:004) His lightning lights up the world. The earth sees, and trembles.
en_uk_webbe~PSA~C096~005~(C097:005) The mountains melt like wax at the presence of the LORD, at the presence of the Lord of the whole earth.
en_uk_webbe~PSA~C096~006~(C097:006) The heavens declare his righteousness. All the peoples have seen his glory.
en_uk_webbe~PSA~C096~007~(C097:007) Let all them be shamed who serve engraved images, who boast in their idols. Worship him, all you gods!
en_uk_webbe~PSA~C096~008~(C097:008) Zion heard and was glad. The daughters of Judah rejoiced because of your judgements, LORD.
en_uk_webbe~PSA~C096~009~(C097:009) For you, LORD, are most high above all the earth. You are exalted far above all gods.
en_uk_webbe~PSA~C096~010~(C097:010) You who love the LORD, hate evil! He preserves the souls of his saints. He delivers them out of the hand of the wicked.
en_uk_webbe~PSA~C096~011~(C097:011) Light is sown for the righteous, and gladness for the upright in heart.
en_uk_webbe~PSA~C096~012~(C097:012) Be glad in the LORD, you righteous people! Give thanks to his holy Name.
