en_uk_webbe~JOB~C04~01~Then Eliphaz the Temanite answered,
en_uk_webbe~JOB~C04~02~“If someone ventures to talk with you, will you be grieved? But who can withhold himself from speaking?
en_uk_webbe~JOB~C04~03~Behold, you have instructed many, you have strengthened the weak hands.
en_uk_webbe~JOB~C04~04~Your words have supported him who was falling, You have made firm the feeble knees.
en_uk_webbe~JOB~C04~05~But now it has come to you, and you faint. It touches you, and you are troubled.
en_uk_webbe~JOB~C04~06~Isn’t your piety your confidence? Isn’t the integrity of your ways your hope?
en_uk_webbe~JOB~C04~07~“Remember, now, whoever perished, being innocent? Or where were the upright cut off?
en_uk_webbe~JOB~C04~08~According to what I have seen, those who plough iniquity, and sow trouble, reap the same.
en_uk_webbe~JOB~C04~09~By the breath of God they perish. By the blast of his anger are they consumed.
en_uk_webbe~JOB~C04~10~The roaring of the lion, and the voice of the fierce lion, the teeth of the young lions, are broken.
en_uk_webbe~JOB~C04~11~The old lion perishes for lack of prey. The cubs of the lioness are scattered abroad.
en_uk_webbe~JOB~C04~12~“Now a thing was secretly brought to me. My ear received a whisper of it.
en_uk_webbe~JOB~C04~13~In thoughts from the visions of the night, when deep sleep falls on men,
en_uk_webbe~JOB~C04~14~fear came on me, and trembling, which made all my bones shake.
en_uk_webbe~JOB~C04~15~Then a spirit passed before my face. The hair of my flesh stood up.
en_uk_webbe~JOB~C04~16~It stood still, but I couldn’t discern its appearance. A form was before my eyes. Silence, then I heard a voice, saying,
en_uk_webbe~JOB~C04~17~‘Shall mortal man be more just than God? Shall a man be more pure than his Maker?
en_uk_webbe~JOB~C04~18~Behold, he puts no trust in his servants. He charges his angels with error.
en_uk_webbe~JOB~C04~19~How much more, those who dwell in houses of clay, whose foundation is in the dust, who are crushed before the moth!
en_uk_webbe~JOB~C04~20~Between morning and evening they are destroyed. They perish forever without any regarding it.
en_uk_webbe~JOB~C04~21~Isn’t their tent cord plucked up within them? They die, and that without wisdom.’
