en_uk_webbe~WIS~C07~01~I myself am also mortal, like everyone else, and am a descendant of one formed first and born of the earth.
en_uk_webbe~WIS~C07~02~I moulded into flesh in the time of ten months in my mother’s womb, being compacted in blood from the seed of man and pleasure that came with sleep.
en_uk_webbe~WIS~C07~03~I also, when I was born, drew in the common air, and fell upon the kindred earth, uttering, like all, for my first voice, the same cry.
en_uk_webbe~WIS~C07~04~I was nursed with care in swaddling clothes.
en_uk_webbe~WIS~C07~05~For no king had any other first beginning;
en_uk_webbe~WIS~C07~06~but all men have one entrance into life, and a common departure.
en_uk_webbe~WIS~C07~07~For this cause I prayed, and understanding was given to me. I asked a spirit of wisdom came to me.
en_uk_webbe~WIS~C07~08~I preferred her before sceptres and thrones. I considered riches nothing in comparison to her.
en_uk_webbe~WIS~C07~09~Neither did I liken to her any priceless gem, because all gold in her presence is a little sand, and silver will be considered as clay before her.
en_uk_webbe~WIS~C07~10~I loved her more than health and beauty, and I chose to have her rather than light, because her bright shining is never laid to sleep.
en_uk_webbe~WIS~C07~11~All good things came to me with her, and innumerable riches are in her hands.
en_uk_webbe~WIS~C07~12~And I rejoiced over them all because wisdom leads them; although I didn’t know that she was their mother.
en_uk_webbe~WIS~C07~13~As I learnt without guile, I impart without grudging. I don’t hide her riches.
en_uk_webbe~WIS~C07~14~For she is a treasure for men that doesn’t fail, and those who use it obtain friendship with God, commended by the gifts which they present through discipline.
en_uk_webbe~WIS~C07~15~But may God grant that I may speak his judgement, and to conceive thoughts worthy of what has been given me; because he is one who guides even wisdom and who corrects the wise.
en_uk_webbe~WIS~C07~16~For both we and our words are in his hand, with all understanding and skill in various crafts.
en_uk_webbe~WIS~C07~17~For he himself gave me an unerring knowledge of the things that are, to know the structure of the universe and the operation of the elements;
en_uk_webbe~WIS~C07~18~the beginning, end, and middle of times; the alternations of the solstices and the changes of seasons;
en_uk_webbe~WIS~C07~19~the circuits of years and the positions of stars;
en_uk_webbe~WIS~C07~20~the natures of living creatures and the raging of wild beasts; The violence of winds and the thoughts of men; the diversities of plants and the virtues of roots.
en_uk_webbe~WIS~C07~21~All things that are either secret or manifest I learnt,
en_uk_webbe~WIS~C07~22~for wisdom, that is the architect of all things, taught me. For there is in her a spirit that is quick to understand, holy, unique, manifold, subtle, freely moving, clear in utterance, unpolluted, distinct, unharmed, loving what is good, keen, unhindered,
en_uk_webbe~WIS~C07~23~beneficent, loving towards man, steadfast, sure, free from care, all-powerful, all-surveying, and penetrating through all spirits that are quick to understand, pure, most subtle:
en_uk_webbe~WIS~C07~24~For wisdom is more mobile than any motion. Yes, she pervades and penetrates all things by reason of her purity.
en_uk_webbe~WIS~C07~25~For she is a breath of the power of God, and a clear effluence of the glory of the Almighty. Therefore nothing defiled can find entrance into her.
en_uk_webbe~WIS~C07~26~For she is a reflection of everlasting light, an unspotted mirror of the working of God, and an image of his goodness.
en_uk_webbe~WIS~C07~27~She, being one, has power to do all things. Remaining in herself, she renews all things. From generation to generation passing into holy souls, she makes friends of God and prophets.
en_uk_webbe~WIS~C07~28~For God loves nothing as much as one who dwells with wisdom.
en_uk_webbe~WIS~C07~29~For she is fairer than the sun, and above all the constellations of the stars. She is better than light.
en_uk_webbe~WIS~C07~30~For daylight succeeds night, but evil does not prevail against wisdom.
