en_uk_webbe~CO1~C01~01~Paul, called to be an apostle of Jesus Christ through the will of God, and our brother Sosthenes,
en_uk_webbe~CO1~C01~02~to the assembly of God which is at Corinth—those who are sanctified in Christ Jesus, called saints, with all who call on the name of our Lord Jesus Christ in every place, both theirs and ours:
en_uk_webbe~CO1~C01~03~Grace to you and peace from God our Father and the Lord Jesus Christ.
en_uk_webbe~CO1~C01~04~I always thank my God concerning you, for the grace of God which was given you in Christ Jesus;
en_uk_webbe~CO1~C01~05~that in everything you were enriched in him, in all speech and all knowledge;
en_uk_webbe~CO1~C01~06~even as the testimony of Christ was confirmed in you:
en_uk_webbe~CO1~C01~07~so that you come behind in no gift; waiting for the revelation of our Lord Jesus Christ;
en_uk_webbe~CO1~C01~08~who will also confirm you until the end, blameless in the day of our Lord Jesus Christ.
en_uk_webbe~CO1~C01~09~God is faithful, through whom you were called into the fellowship of his Son, Jesus Christ, our Lord.
en_uk_webbe~CO1~C01~10~Now I beg you, brothers, through the name of our Lord, Jesus Christ, that you all speak the same thing, and that there be no divisions amongst you, but that you be perfected together in the same mind and in the same judgement.
en_uk_webbe~CO1~C01~11~For it has been reported to me concerning you, my brothers, by those who are from Chloe’s household, that there are contentions amongst you.
en_uk_webbe~CO1~C01~12~Now I mean this, that each one of you says, “I follow Paul,” “I follow Apollos,” “I follow Cephas,” and, “I follow Christ.”
en_uk_webbe~CO1~C01~13~Is Christ divided? Was Paul crucified for you? Or were you baptised into the name of Paul?
en_uk_webbe~CO1~C01~14~I thank God that I baptised none of you, except Crispus and Gaius,
en_uk_webbe~CO1~C01~15~so that no one should say that I had baptised you into my own name.
en_uk_webbe~CO1~C01~16~(I also baptised the household of Stephanas; besides them, I don’t know whether I baptised any other.)
en_uk_webbe~CO1~C01~17~For Christ sent me not to baptise, but to preach the Good News—not in wisdom of words, so that the cross of Christ wouldn’t be made void.
en_uk_webbe~CO1~C01~18~For the word of the cross is foolishness to those who are dying, but to us who are being saved it is the power of God.
en_uk_webbe~CO1~C01~19~For it is written, “I will destroy the wisdom of the wise. I will bring the discernment of the discerning to nothing.”
en_uk_webbe~CO1~C01~20~Where is the wise? Where is the scribe? Where is the lawyer of this world? Hasn’t God made foolish the wisdom of this world?
en_uk_webbe~CO1~C01~21~For seeing that in the wisdom of God, the world through its wisdom didn’t know God, it was God’s good pleasure through the foolishness of the preaching to save those who believe.
en_uk_webbe~CO1~C01~22~For Jews ask for signs, Greeks seek after wisdom,
en_uk_webbe~CO1~C01~23~but we preach Christ crucified: a stumbling block to Jews, and foolishness to Greeks,
en_uk_webbe~CO1~C01~24~but to those who are called, both Jews and Greeks, Christ is the power of God and the wisdom of God;
en_uk_webbe~CO1~C01~25~because the foolishness of God is wiser than men, and the weakness of God is stronger than men.
en_uk_webbe~CO1~C01~26~For you see your calling, brothers, that not many are wise according to the flesh, not many mighty, and not many noble;
en_uk_webbe~CO1~C01~27~but God chose the foolish things of the world that he might put to shame those who are wise. God chose the weak things of the world that he might put to shame the things that are strong.
en_uk_webbe~CO1~C01~28~God chose the lowly things of the world, and the things that are despised, and the things that don’t exist, that he might bring to nothing the things that exist,
en_uk_webbe~CO1~C01~29~that no flesh should boast before God.
en_uk_webbe~CO1~C01~30~Because of him, you are in Christ Jesus, who was made to us wisdom from God, and righteousness and sanctification, and redemption:
en_uk_webbe~CO1~C01~31~that, as it is written, “He who boasts, let him boast in the Lord.”
