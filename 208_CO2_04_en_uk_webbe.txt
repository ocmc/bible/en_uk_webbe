en_uk_webbe~CO2~C04~01~Therefore seeing we have this ministry, even as we obtained mercy, we don’t faint.
en_uk_webbe~CO2~C04~02~But we have renounced the hidden things of shame, not walking in craftiness, nor handling the word of God deceitfully, but by the manifestation of the truth commending ourselves to every man’s conscience in the sight of God.
en_uk_webbe~CO2~C04~03~Even if our Good News is veiled, it is veiled in those who are dying,
en_uk_webbe~CO2~C04~04~in whom the god of this world has blinded the minds of the unbelieving, that the light of the Good News of the glory of Christ, who is the image of God, should not dawn on them.
en_uk_webbe~CO2~C04~05~For we don’t preach ourselves, but Christ Jesus as Lord, and ourselves as your servants for Jesus’ sake,
en_uk_webbe~CO2~C04~06~seeing it is God who said, “Light will shine out of darkness,” who has shone in our hearts to give the light of the knowledge of the glory of God in the face of Jesus Christ.
en_uk_webbe~CO2~C04~07~But we have this treasure in clay vessels, that the exceeding greatness of the power may be of God, and not from ourselves.
en_uk_webbe~CO2~C04~08~We are pressed on every side, yet not crushed; perplexed, yet not to despair;
en_uk_webbe~CO2~C04~09~pursued, yet not forsaken; struck down, yet not destroyed;
en_uk_webbe~CO2~C04~10~always carrying in the body the putting to death of the Lord Jesus, that the life of Jesus may also be revealed in our body.
en_uk_webbe~CO2~C04~11~For we who live are always delivered to death for Jesus’ sake, that the life also of Jesus may be revealed in our mortal flesh.
en_uk_webbe~CO2~C04~12~So then death works in us, but life in you.
en_uk_webbe~CO2~C04~13~But having the same spirit of faith, according to that which is written, “I believed, and therefore I spoke.” We also believe, and therefore we also speak;
en_uk_webbe~CO2~C04~14~knowing that he who raised the Lord Jesus will raise us also with Jesus, and will present us with you.
en_uk_webbe~CO2~C04~15~For all things are for your sakes, that the grace, being multiplied through the many, may cause the thanksgiving to abound to the glory of God.
en_uk_webbe~CO2~C04~16~Therefore we don’t faint, but though our outward man is decaying, yet our inward man is renewed day by day.
en_uk_webbe~CO2~C04~17~For our light affliction, which is for the moment, works for us more and more exceedingly an eternal weight of glory,
en_uk_webbe~CO2~C04~18~while we don’t look at the things which are seen, but at the things which are not seen. For the things which are seen are temporal, but the things which are not seen are eternal.
