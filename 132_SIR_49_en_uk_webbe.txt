en_uk_webbe~SIR~C49~01~The memorial of Josiah is like the composition of incense Prepared by the work of the apothecary: It shall be sweet as honey in every mouth, And as music at a banquet of wine.
en_uk_webbe~SIR~C49~02~He behaved himself uprightly in the conversion of the people, And took away the abominations of iniquity.
en_uk_webbe~SIR~C49~03~He set his heart right towards the Lord; In the days of wicked men he made godliness to prevail.
en_uk_webbe~SIR~C49~04~Except David and Hezekiah and Josiah, All committed trespass: For they forsook the law of the Most High; The kings of Judah failed.
en_uk_webbe~SIR~C49~05~For they gave their power to others, And their glory to a strange nation.
en_uk_webbe~SIR~C49~06~They set on fire the chosen city of the sanctuary, And made her streets desolate, [as it was written ] by the hand of Jeremiah.
en_uk_webbe~SIR~C49~07~For they entreated him evil; And yet he was sanctified in the womb to be a prophet, To root out, and to afflict, and to destroy; [And ] in like manner to build and to plant.
en_uk_webbe~SIR~C49~08~[It was ] Ezekiel who saw the vision of glory, Which [God ] showed him upon the chariot of the cherubim.
en_uk_webbe~SIR~C49~09~For truly he remembered the enemies in storm, And to do good to those who directed their ways aright.
en_uk_webbe~SIR~C49~10~Also of the twelve prophets May the bones flourish again out of their place. And he comforted Jacob, And delivered them by confidence of hope.
en_uk_webbe~SIR~C49~11~How shall we magnify Zerubbabel? And he was as a signet on the right hand:
en_uk_webbe~SIR~C49~12~So was Jesus the son of Josedek: Who in their days built the house, And exalted a people holy to the Lord, Prepared for everlasting glory.
en_uk_webbe~SIR~C49~13~Also of Nehemiah the memorial is great; Who raised up for us the walls that were fallen, And set up the gates and bars, And raised up our homes again.
en_uk_webbe~SIR~C49~14~No man was created upon the earth such as was Enoch; For he was taken up from the earth.
en_uk_webbe~SIR~C49~15~Neither was there a man born like to Joseph, A governor of his kindred, a stay of the people: Yes, his bones were visited.
en_uk_webbe~SIR~C49~16~Shem and Seth were glorified amongst men; And above every living thing in the creation is Adam.
