en_uk_webbe~LUK~C20~01~On one of those days, as he was teaching the people in the temple and preaching the Good News, the priests and scribes came to him with the elders.
en_uk_webbe~LUK~C20~02~They asked him, “Tell us: by what authority do you do these things? Or who is giving you this authority?”
en_uk_webbe~LUK~C20~03~He answered them, “I also will ask you one question. Tell me:
en_uk_webbe~LUK~C20~04~the baptism of John, was it from heaven, or from men?”
en_uk_webbe~LUK~C20~05~They reasoned with themselves, saying, “If we say, ‘From heaven,’ he will say, ‘Why didn’t you believe him?’
en_uk_webbe~LUK~C20~06~But if we say, ‘From men,’ all the people will stone us, for they are persuaded that John was a prophet.”
en_uk_webbe~LUK~C20~07~They answered that they didn’t know where it was from.
en_uk_webbe~LUK~C20~08~Jesus said to them, “Neither will I tell you by what authority I do these things.”
en_uk_webbe~LUK~C20~09~He began to tell the people this parable. “A man planted a vineyard, and rented it out to some farmers, and went into another country for a long time.
en_uk_webbe~LUK~C20~10~At the proper season, he sent a servant to the farmers to collect his share of the fruit of the vineyard. But the farmers beat him, and sent him away empty.
en_uk_webbe~LUK~C20~11~He sent yet another servant, and they also beat him, and treated him shamefully, and sent him away empty.
en_uk_webbe~LUK~C20~12~He sent yet a third, and they also wounded him, and threw him out.
en_uk_webbe~LUK~C20~13~The lord of the vineyard said, ‘What shall I do? I will send my beloved son. It may be that seeing him, they will respect him.’
en_uk_webbe~LUK~C20~14~“But when the farmers saw him, they reasoned amongst themselves, saying, ‘This is the heir. Come, let’s kill him, that the inheritance may be ours.’
en_uk_webbe~LUK~C20~15~They threw him out of the vineyard and killed him. What therefore will the lord of the vineyard do to them?
en_uk_webbe~LUK~C20~16~He will come and destroy these farmers, and will give the vineyard to others.” When they heard that, they said, “May that never be!”
en_uk_webbe~LUK~C20~17~But he looked at them and said, “Then what is this that is written, ‘The stone which the builders rejected was made the chief cornerstone?’
en_uk_webbe~LUK~C20~18~Everyone who falls on that stone will be broken to pieces, but it will crush whomever it falls on to dust.”
en_uk_webbe~LUK~C20~19~The chief priests and the scribes sought to lay hands on him that very hour, but they feared the people—for they knew he had spoken this parable against them.
en_uk_webbe~LUK~C20~20~They watched him and sent out spies, who pretended to be righteous, that they might trap him in something he said, so as to deliver him up to the power and authority of the governor.
en_uk_webbe~LUK~C20~21~They asked him, “Teacher, we know that you say and teach what is right, and aren’t partial to anyone, but truly teach the way of God.
en_uk_webbe~LUK~C20~22~Is it lawful for us to pay taxes to Caesar, or not?”
en_uk_webbe~LUK~C20~23~But he perceived their craftiness, and said to them, “Why do you test me?
en_uk_webbe~LUK~C20~24~Show me a denarius. Whose image and inscription are on it?” They answered, “Caesar’s.”
en_uk_webbe~LUK~C20~25~He said to them, “Then give to Caesar the things that are Caesar’s, and to God the things that are God’s.”
en_uk_webbe~LUK~C20~26~They weren’t able to trap him in his words before the people. They marvelled at his answer and were silent.
en_uk_webbe~LUK~C20~27~Some of the Sadducees came to him, those who deny that there is a resurrection.
en_uk_webbe~LUK~C20~28~They asked him, “Teacher, Moses wrote to us that if a man’s brother dies having a wife, and he is childless, his brother should take the wife and raise up children for his brother.
en_uk_webbe~LUK~C20~29~There were therefore seven brothers. The first took a wife, and died childless.
en_uk_webbe~LUK~C20~30~The second took her as wife, and he died childless.
en_uk_webbe~LUK~C20~31~The third took her, and likewise the seven all left no children, and died.
en_uk_webbe~LUK~C20~32~Afterward the woman also died.
en_uk_webbe~LUK~C20~33~Therefore in the resurrection whose wife of them will she be? For the seven had her as a wife.”
en_uk_webbe~LUK~C20~34~Jesus said to them, “The children of this age marry, and are given in marriage.
en_uk_webbe~LUK~C20~35~But those who are considered worthy to attain to that age and the resurrection from the dead neither marry nor are given in marriage.
en_uk_webbe~LUK~C20~36~For they can’t die any more, for they are like the angels, and are children of God, being children of the resurrection.
en_uk_webbe~LUK~C20~37~But that the dead are raised, even Moses showed at the bush, when he called the Lord ‘The God of Abraham, the God of Isaac, and the God of Jacob.’
en_uk_webbe~LUK~C20~38~Now he is not the God of the dead, but of the living, for all are alive to him.”
en_uk_webbe~LUK~C20~39~Some of the scribes answered, “Teacher, you speak well.”
en_uk_webbe~LUK~C20~40~They didn’t dare to ask him any more questions.
en_uk_webbe~LUK~C20~41~He said to them, “Why do they say that the Christ is David’s son?
en_uk_webbe~LUK~C20~42~David himself says in the book of Psalms, ‘The Lord said to my Lord, “Sit at my right hand,
en_uk_webbe~LUK~C20~43~until I make your enemies the footstool of your feet.”’
en_uk_webbe~LUK~C20~44~“David therefore calls him Lord, so how is he his son?”
en_uk_webbe~LUK~C20~45~In the hearing of all the people, he said to his disciples,
en_uk_webbe~LUK~C20~46~“Beware of those scribes who like to walk in long robes, and love greetings in the marketplaces, the best seats in the synagogues, and the best places at feasts;
en_uk_webbe~LUK~C20~47~who devour widows’ houses, and for a pretence make long prayers: these will receive greater condemnation.”
