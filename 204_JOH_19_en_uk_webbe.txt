en_uk_webbe~JOH~C19~01~So Pilate then took Jesus, and flogged him.
en_uk_webbe~JOH~C19~02~The soldiers twisted thorns into a crown, and put it on his head, and dressed him in a purple garment.
en_uk_webbe~JOH~C19~03~They kept saying, “Hail, King of the Jews!” and they kept slapping him.
en_uk_webbe~JOH~C19~04~Then Pilate went out again, and said to them, “Behold, I bring him out to you, that you may know that I find no basis for a charge against him.”
en_uk_webbe~JOH~C19~05~Jesus therefore came out, wearing the crown of thorns and the purple garment. Pilate said to them, “Behold, the man!”
en_uk_webbe~JOH~C19~06~When therefore the chief priests and the officers saw him, they shouted, saying, “Crucify! Crucify!” Pilate said to them, “Take him yourselves, and crucify him, for I find no basis for a charge against him.”
en_uk_webbe~JOH~C19~07~The Jews answered him, “We have a law, and by our law he ought to die, because he made himself the Son of God.”
en_uk_webbe~JOH~C19~08~When therefore Pilate heard this saying, he was more afraid.
en_uk_webbe~JOH~C19~09~He entered into the Praetorium again, and said to Jesus, “Where are you from?” But Jesus gave him no answer.
en_uk_webbe~JOH~C19~10~Pilate therefore said to him, “Aren’t you speaking to me? Don’t you know that I have power to release you and have power to crucify you?”
en_uk_webbe~JOH~C19~11~Jesus answered, “You would have no power at all against me, unless it were given to you from above. Therefore he who delivered me to you has greater sin.”
en_uk_webbe~JOH~C19~12~At this, Pilate was seeking to release him, but the Jews cried out, saying, “If you release this man, you aren’t Caesar’s friend! Everyone who makes himself a king speaks against Caesar!”
en_uk_webbe~JOH~C19~13~When Pilate therefore heard these words, he brought Jesus out and sat down on the judgement seat at a place called “The Pavement”, but in Hebrew, “Gabbatha.”
en_uk_webbe~JOH~C19~14~Now it was the Preparation Day of the Passover, at about the sixth hour. He said to the Jews, “Behold, your King!”
en_uk_webbe~JOH~C19~15~They cried out, “Away with him! Away with him! Crucify him!” Pilate said to them, “Shall I crucify your King?” The chief priests answered, “We have no king but Caesar!”
en_uk_webbe~JOH~C19~16~So then he delivered him to them to be crucified. So they took Jesus and led him away.
en_uk_webbe~JOH~C19~17~He went out, bearing his cross, to the place called “The Place of a Skull”, which is called in Hebrew, “Golgotha”,
en_uk_webbe~JOH~C19~18~where they crucified him, and with him two others, on either side one, and Jesus in the middle.
en_uk_webbe~JOH~C19~19~Pilate wrote a title also, and put it on the cross. There was written, “JESUS OF NAZARETH, THE KING OF THE JEWS.”
en_uk_webbe~JOH~C19~20~Therefore many of the Jews read this title, for the place where Jesus was crucified was near the city; and it was written in Hebrew, in Latin, and in Greek.
en_uk_webbe~JOH~C19~21~The chief priests of the Jews therefore said to Pilate, “Don’t write, ‘The King of the Jews,’ but, ‘he said, “I am King of the Jews.”’”
en_uk_webbe~JOH~C19~22~Pilate answered, “What I have written, I have written.”
en_uk_webbe~JOH~C19~23~Then the soldiers, when they had crucified Jesus, took his garments and made four parts, to every soldier a part; and also the coat. Now the coat was without seam, woven from the top throughout.
en_uk_webbe~JOH~C19~24~Then they said to one another, “Let’s not tear it, but cast lots for it to decide whose it will be,” that the Scripture might be fulfilled, which says, “They parted my garments amongst them. For my cloak they cast lots.” Therefore the soldiers did these things.
en_uk_webbe~JOH~C19~25~But standing by Jesus’ cross were his mother, his mother’s sister, Mary the wife of Clopas, and Mary Magdalene.
en_uk_webbe~JOH~C19~26~Therefore when Jesus saw his mother, and the disciple whom he loved standing there, he said to his mother, “Woman, behold, your son!”
en_uk_webbe~JOH~C19~27~Then he said to the disciple, “Behold, your mother!” From that hour, the disciple took her to his own home.
en_uk_webbe~JOH~C19~28~After this, Jesus, seeing that all things were now finished, that the Scripture might be fulfilled, said, “I am thirsty.”
en_uk_webbe~JOH~C19~29~Now a vessel full of vinegar was set there; so they put a sponge full of the vinegar on hyssop, and held it at his mouth.
en_uk_webbe~JOH~C19~30~When Jesus therefore had received the vinegar, he said, “It is finished.” He bowed his head, and gave up his spirit.
en_uk_webbe~JOH~C19~31~Therefore the Jews, because it was the Preparation Day, so that the bodies wouldn’t remain on the cross on the Sabbath (for that Sabbath was a special one), asked of Pilate that their legs might be broken, and that they might be taken away.
en_uk_webbe~JOH~C19~32~Therefore the soldiers came, and broke the legs of the first, and of the other who was crucified with him;
en_uk_webbe~JOH~C19~33~but when they came to Jesus, and saw that he was already dead, they didn’t break his legs.
en_uk_webbe~JOH~C19~34~However one of the soldiers pierced his side with a spear, and immediately blood and water came out.
en_uk_webbe~JOH~C19~35~He who has seen has testified, and his testimony is true. He knows that he tells the truth, that you may believe.
en_uk_webbe~JOH~C19~36~For these things happened that the Scripture might be fulfilled, “A bone of him will not be broken.”
en_uk_webbe~JOH~C19~37~Again another Scripture says, “They will look on him whom they pierced.”
en_uk_webbe~JOH~C19~38~After these things, Joseph of Arimathaea, being a disciple of Jesus, but secretly for fear of the Jews, asked of Pilate that he might take away Jesus’ body. Pilate gave him permission. He came therefore and took away his body.
en_uk_webbe~JOH~C19~39~Nicodemus, who at first came to Jesus by night, also came bringing a mixture of myrrh and aloes, about a hundred Roman pounds.
en_uk_webbe~JOH~C19~40~So they took Jesus’ body, and bound it in linen cloths with the spices, as the custom of the Jews is to bury.
en_uk_webbe~JOH~C19~41~Now in the place where he was crucified there was a garden. In the garden was a new tomb in which no man had ever yet been laid.
en_uk_webbe~JOH~C19~42~Then because of the Jews’ Preparation Day (for the tomb was near at hand) they laid Jesus there.
