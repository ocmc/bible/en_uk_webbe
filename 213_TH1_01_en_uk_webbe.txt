en_uk_webbe~TH1~C01~01~Paul, Silvanus, and Timothy, to the assembly of the Thessalonians in God the Father and the Lord Jesus Christ: Grace to you and peace from God our Father and the Lord Jesus Christ.
en_uk_webbe~TH1~C01~02~We always give thanks to God for all of you, mentioning you in our prayers,
en_uk_webbe~TH1~C01~03~remembering without ceasing your work of faith and labour of love and perseverance of hope in our Lord Jesus Christ, before our God and Father.
en_uk_webbe~TH1~C01~04~We know, brothers loved by God, that you are chosen,
en_uk_webbe~TH1~C01~05~and that our Good News came to you not in word only, but also in power, and in the Holy Spirit, and with much assurance. You know what kind of men we showed ourselves to be amongst you for your sake.
en_uk_webbe~TH1~C01~06~You became imitators of us and of the Lord, having received the word in much affliction, with joy of the Holy Spirit,
en_uk_webbe~TH1~C01~07~so that you became an example to all who believe in Macedonia and in Achaia.
en_uk_webbe~TH1~C01~08~For from you the word of the Lord has been declared, not only in Macedonia and Achaia, but also in every place your faith towards God has gone out, so that we need not to say anything.
en_uk_webbe~TH1~C01~09~For they themselves report concerning us what kind of a reception we had from you, and how you turned to God from idols, to serve a living and true God,
en_uk_webbe~TH1~C01~10~and to wait for his Son from heaven, whom he raised from the dead: Jesus, who delivers us from the wrath to come.
