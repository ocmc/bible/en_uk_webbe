en_uk_webbe~PSA~C062~001~(C063:000) A Psalm by David, when he was in the desert of Judah.
en_uk_webbe~PSA~C062~002~(C063:001) God, you are my God. I will earnestly seek you. My soul thirsts for you. My flesh longs for you, in a dry and weary land, where there is no water.
en_uk_webbe~PSA~C062~003~(C063:002) So I have seen you in the sanctuary, watching your power and your glory.
en_uk_webbe~PSA~C062~004~(C063:003) Because your loving kindness is better than life, my lips shall praise you.
en_uk_webbe~PSA~C062~005~(C063:004) So I will bless you while I live. I will lift up my hands in your name.
en_uk_webbe~PSA~C062~006~(C063:005) My soul shall be satisfied as with the richest food. My mouth shall praise you with joyful lips,
en_uk_webbe~PSA~C062~007~(C063:006) when I remember you on my bed, and think about you in the night watches.
en_uk_webbe~PSA~C062~008~(C063:007) For you have been my help. I will rejoice in the shadow of your wings.
en_uk_webbe~PSA~C062~009~(C063:008) My soul stays close to you. Your right hand holds me up.
en_uk_webbe~PSA~C062~010~(C063:009) But those who seek my soul to destroy it shall go into the lower parts of the earth.
en_uk_webbe~PSA~C062~011~(C063:010) They shall be given over to the power of the sword. They shall be jackal food.
en_uk_webbe~PSA~C062~012~(C063:011) But the king shall rejoice in God. Everyone who swears by him will praise him, for the mouth of those who speak lies shall be silenced.
