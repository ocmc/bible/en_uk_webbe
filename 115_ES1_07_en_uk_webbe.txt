en_uk_webbe~ES1~C07~01~Then Sisinnes the governor of Coelesyria and Phoenicia, and Sathrabuzanes, with their companions, following the commandments of king Darius,
en_uk_webbe~ES1~C07~02~did very carefully oversee the holy works, assisting the elders of the Jews and rulers of the temple.
en_uk_webbe~ES1~C07~03~And so the holy works prospered, while Aggaeus and Zacharias the prophets prophesied.
en_uk_webbe~ES1~C07~04~And they finished these things by the commandment of the Lord, the God of Israel, and with the consent of Cyrus, Darius, and Artaxerxes, kings of the Persians.
en_uk_webbe~ES1~C07~05~[And thus] was the house finished by the three and twentieth day of the month Adar, in the sixth year of king Darius.
en_uk_webbe~ES1~C07~06~And the children of Israel, the priests, and the Levites, and the other that were of the captivity, that were added [to them,] did according to the things [written] in the book of Moses.
en_uk_webbe~ES1~C07~07~And to the dedication of the temple of the Lord they offered a hundred bullocks, two hundred rams, four hundred lambs;
en_uk_webbe~ES1~C07~08~[and] twelve he-goats for the sin of all Israel, according to the number of the twelve princes of the tribes of Israel.
en_uk_webbe~ES1~C07~09~The priests also and the Levites stood arrayed in their vestments, according to their kindred, for the services of the Lord, the God of Israel, according to the book of Moses: and the gatekeepers at every gate.
en_uk_webbe~ES1~C07~10~And the children of Israel that came out of the captivity held the Passover the fourteenth day of the first month, when the priests and the Levites were sanctified together,
en_uk_webbe~ES1~C07~11~and all those who were of the captivity; for they were sanctified. For the Levites were all sanctified together,
en_uk_webbe~ES1~C07~12~and they offered the Passover for all them of the captivity, and for their kindred the priests, and for themselves.
en_uk_webbe~ES1~C07~13~And the children of Israel that came out of the captivity did eat, even all those who had separated themselves from the abominations of the heathen of the land, and sought the Lord.
en_uk_webbe~ES1~C07~14~And they kept the feast of unleavened bread seven days, making merry before the Lord,
en_uk_webbe~ES1~C07~15~for that he had turned the counsel of the king of Assyria towards them, to strengthen their hands in the works of the Lord, the God of Israel.
