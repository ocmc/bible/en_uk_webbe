en_uk_webbe~DEU~C20~01~When you go out to battle against your enemies, and see horses, chariots, and a people more numerous than you, you shall not be afraid of them; for the LORD your God is with you, who brought you up out of the land of Egypt.
en_uk_webbe~DEU~C20~02~It shall be, when you draw near to the battle, that the priest shall approach and speak to the people,
en_uk_webbe~DEU~C20~03~and shall tell them, “Hear, Israel, you draw near today to battle against your enemies. Don’t let your heart faint! Don’t be afraid, nor tremble, neither be scared of them;
en_uk_webbe~DEU~C20~04~for the LORD your God is he who goes with you, to fight for you against your enemies, to save you.”
en_uk_webbe~DEU~C20~05~The officers shall speak to the people, saying, “What man is there who has built a new house, and has not dedicated it? Let him go and return to his house, lest he die in the battle, and another man dedicate it.
en_uk_webbe~DEU~C20~06~What man is there who has planted a vineyard, and has not used its fruit? Let him go and return to his house, lest he die in the battle, and another man use its fruit.
en_uk_webbe~DEU~C20~07~What man is there who has pledged to be married to a wife, and has not taken her? Let him go and return to his house, lest he die in the battle, and another man take her.”
en_uk_webbe~DEU~C20~08~The officers shall speak further to the people, and they shall say, “What man is there who is fearful and faint-hearted? Let him go and return to his house, lest his brother’s heart melt as his heart.”
en_uk_webbe~DEU~C20~09~It shall be, when the officers have finished speaking to the people, that they shall appoint captains of armies at the head of the people.
en_uk_webbe~DEU~C20~10~When you draw near to a city to fight against it, then proclaim peace to it.
en_uk_webbe~DEU~C20~11~It shall be, if it gives you answer of peace and opens to you, then it shall be that all the people who are found therein shall become forced labourers to you, and shall serve you.
en_uk_webbe~DEU~C20~12~If it will make no peace with you, but will make war against you, then you shall besiege it.
en_uk_webbe~DEU~C20~13~When the LORD your God delivers it into your hand, you shall strike every male of it with the edge of the sword;
en_uk_webbe~DEU~C20~14~but the women, the little ones, the livestock, and all that is in the city, even all its plunder, you shall take for plunder for yourself. You may use the plunder of your enemies, which the LORD your God has given you.
en_uk_webbe~DEU~C20~15~Thus you shall do to all the cities which are very far off from you, which are not of the cities of these nations.
en_uk_webbe~DEU~C20~16~But of the cities of these peoples that the LORD your God gives you for an inheritance, you shall save alive nothing that breathes;
en_uk_webbe~DEU~C20~17~but you shall utterly destroy them: the Hittite, the Amorite, the Canaanite, the Perizzite, the Hivite, and the Jebusite, as the LORD your God has commanded you;
en_uk_webbe~DEU~C20~18~that they not teach you to follow all their abominations, which they have done for their gods; so would you sin against the LORD your God.
en_uk_webbe~DEU~C20~19~When you shall besiege a city a long time, in making war against it to take it, you shall not destroy its trees by wielding an axe against them; for you may eat of them. You shall not cut them down, for is the tree of the field man, that it should be besieged by you?
en_uk_webbe~DEU~C20~20~Only the trees that you know are not trees for food, you shall destroy and cut them down. You shall build bulwarks against the city that makes war with you, until it falls.
