en_uk_webbe~CH1~C26~01~For the divisions of the doorkeepers: of the Korahites, Meshelemiah the son of Kore, of the sons of Asaph.
en_uk_webbe~CH1~C26~02~Meshelemiah had sons: Zechariah the firstborn, Jediael the second, Zebadiah the third, Jathniel the fourth,
en_uk_webbe~CH1~C26~03~Elam the fifth, Jehohanan the sixth, and Eliehoenai the seventh.
en_uk_webbe~CH1~C26~04~Obed-Edom had sons: Shemaiah the firstborn, Jehozabad the second, Joah the third, Sacar the fourth, Nethanel the fifth,
en_uk_webbe~CH1~C26~05~Ammiel the sixth, Issachar the seventh, and Peullethai the eighth; for God blessed him.
en_uk_webbe~CH1~C26~06~Sons were also born to Shemaiah his son, who ruled over the house of their father; for they were mighty men of valour.
en_uk_webbe~CH1~C26~07~The sons of Shemaiah: Othni, Rephael, Obed, and Elzabad, whose brothers were valiant men, Elihu, and Semachiah.
en_uk_webbe~CH1~C26~08~All these were of the sons of Obed-Edom: they and their sons and their brothers, able men in strength for the service: sixty-two of Obed-Edom.
en_uk_webbe~CH1~C26~09~Meshelemiah had sons and brothers, valiant men, eighteen.
en_uk_webbe~CH1~C26~10~Also Hosah, of the children of Merari, had sons: Shimri the chief (for though he was not the firstborn, yet his father made him chief),
en_uk_webbe~CH1~C26~11~Hilkiah the second, Tebaliah the third, and Zechariah the fourth. All the sons and brothers of Hosah were thirteen.
en_uk_webbe~CH1~C26~12~Of these were the divisions of the doorkeepers, even of the chief men, having offices like their brothers, to minister in the LORD’s house.
en_uk_webbe~CH1~C26~13~They cast lots, the small as well as the great, according to their fathers’ houses, for every gate.
en_uk_webbe~CH1~C26~14~The lot eastward fell to Shelemiah. Then for Zechariah his son, a wise counsellor, they cast lots; and his lot came out northward.
en_uk_webbe~CH1~C26~15~To Obed-Edom southward; and to his sons the storehouse.
en_uk_webbe~CH1~C26~16~To Shuppim and Hosah westward, by the gate of Shallecheth, at the causeway that goes up, watchman opposite watchman.
en_uk_webbe~CH1~C26~17~Eastward were six Levites, northward four a day, southward four a day, and for the storehouse two and two.
en_uk_webbe~CH1~C26~18~For Parbar westward, four at the causeway, and two at Parbar.
en_uk_webbe~CH1~C26~19~These were the divisions of the doorkeepers; of the sons of the Korahites, and of the sons of Merari.
en_uk_webbe~CH1~C26~20~Of the Levites, Ahijah was over the treasures of God’s house and over the treasures of the dedicated things.
en_uk_webbe~CH1~C26~21~The sons of Ladan, the sons of the Gershonites belonging to Ladan, the heads of the fathers’ households belonging to Ladan the Gershonite: Jehieli.
en_uk_webbe~CH1~C26~22~The sons of Jehieli: Zetham, and Joel his brother, over the treasures of the LORD’s house.
en_uk_webbe~CH1~C26~23~Of the Amramites, of the Izharites, of the Hebronites, of the Uzzielites:
en_uk_webbe~CH1~C26~24~and Shebuel the son of Gershom, the son of Moses, was ruler over the treasures.
en_uk_webbe~CH1~C26~25~His brothers: of Eliezer, Rehabiah his son, and Jeshaiah his son, and Joram his son, and Zichri his son, and Shelomoth his son.
en_uk_webbe~CH1~C26~26~This Shelomoth and his brothers were over all the treasures of the dedicated things, which David the king, and the heads of the fathers’ households, the captains over thousands and hundreds, and the captains of the army, had dedicated.
en_uk_webbe~CH1~C26~27~They dedicated some of the plunder won in battles to repair the LORD’s house.
en_uk_webbe~CH1~C26~28~All that Samuel the seer, and Saul the son of Kish, and Abner the son of Ner, and Joab the son of Zeruiah, had dedicated, whoever had dedicated anything, it was under the hand of Shelomoth, and of his brothers.
en_uk_webbe~CH1~C26~29~Of the Izharites, Chenaniah and his sons were for the outward business over Israel, for officers and judges.
en_uk_webbe~CH1~C26~30~Of the Hebronites, Hashabiah and his brothers, men of valour, one thousand and seven hundred, had the oversight of Israel beyond the Jordan westward, for all the business of the LORD, and for the service of the king.
en_uk_webbe~CH1~C26~31~Of the Hebronites, Jerijah was the chief, even of the Hebronites, according to their generations by fathers’ households. They were sought for in the fortieth year of the reign of David, and mighty men of valour were found amongst them at Jazer of Gilead.
en_uk_webbe~CH1~C26~32~His brothers, men of valour, were two thousand and seven hundred, heads of fathers’ households, whom king David made overseers over the Reubenites, the Gadites, and the half-tribe of the Manassites, for every matter pertaining to God, and for the affairs of the king.
