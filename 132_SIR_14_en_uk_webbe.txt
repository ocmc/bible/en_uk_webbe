en_uk_webbe~SIR~C14~01~Blessed is the man that has not slipped with his mouth, And is not pricked with sorrow for sins.
en_uk_webbe~SIR~C14~02~Blessed is he whose soul does not condemn him, And who is not fallen from his hope.
en_uk_webbe~SIR~C14~03~Riches are not comely for a stingy person; And what should an envious man do with money?
en_uk_webbe~SIR~C14~04~He that gathers [by taking ] from his own soul gathers for others; And others will revel in his goods.
en_uk_webbe~SIR~C14~05~He that is evil to himself, to whom will he be good? And he will not rejoice in his possessions.
en_uk_webbe~SIR~C14~06~There is none more evil than he that envies himself; And this is a recompense of his wickedness.
en_uk_webbe~SIR~C14~07~Even if he does good, he does it in forgetfulness; And at the last he shows forth his wickedness.
en_uk_webbe~SIR~C14~08~Evil is he that envies with his eye, Turning away the face, and despising the souls [of men. ]
en_uk_webbe~SIR~C14~09~A covetous man’s eye is not satisfied with his portion; And wicked injustice dries up his soul.
en_uk_webbe~SIR~C14~10~An evil eye is grudging of bread, And he is miserly at his table.
en_uk_webbe~SIR~C14~11~My son, according as you have, do well to yourself, And bring offerings to the Lord worthily.
en_uk_webbe~SIR~C14~12~Remember that death will not wait, And that the covenant of the grave is not showed to you.
en_uk_webbe~SIR~C14~13~Do well to your friend before you die; And according to your ability stretch out [your hand ] and give to him.
en_uk_webbe~SIR~C14~14~Defraud not [yourself ] of a good day; And don’t let the portion of a good desire pass you by.
en_uk_webbe~SIR~C14~15~Shall you not leave your labours to another? And your toils to be divided by lot?
en_uk_webbe~SIR~C14~16~Give, and take, and deceive your soul; For there is no seeking of luxury in the grave.
en_uk_webbe~SIR~C14~17~All flesh waxes old as a garment; For the covenant from the beginning is, You shall die the death.
en_uk_webbe~SIR~C14~18~As of the leaves flourishing on a thick tree, Some it sheds, and some it makes to grow; So also of the generations of flesh and blood, One comes to an end, and another is born.
en_uk_webbe~SIR~C14~19~Every work rots and falls away, And the worker thereof shall depart with it.
en_uk_webbe~SIR~C14~20~Blessed is the man that shall meditate in wisdom, And that shall discourse by his understanding.
en_uk_webbe~SIR~C14~21~He that considers her ways in his heart Shall also have knowledge in her secrets.
en_uk_webbe~SIR~C14~22~Go forth after her as one who tracks, And lie in wait in her ways.
en_uk_webbe~SIR~C14~23~He that pries in at her windows Shall also listen at her doors.
en_uk_webbe~SIR~C14~24~He that lodges close to her house Shall also fasten a nail in her walls.
en_uk_webbe~SIR~C14~25~He shall pitch his tent near at hand to her, And will lodge in a lodging where good things are.
en_uk_webbe~SIR~C14~26~He will set his children under her shelter, And will rest under her branches.
en_uk_webbe~SIR~C14~27~By her he will be covered from heat, And will lodge in her glory.
