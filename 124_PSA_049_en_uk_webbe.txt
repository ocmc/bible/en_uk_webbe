en_uk_webbe~PSA~C049~001~(C050:001) (0) A Psalm by Asaph. (1) The Mighty One, God, The LORD, speaks, and calls the earth from sunrise to sunset.
en_uk_webbe~PSA~C049~002~(C050:002) Out of Zion, the perfection of beauty, God shines out.
en_uk_webbe~PSA~C049~003~(C050:003) Our God comes, and does not keep silent. A fire devours before him. It is very stormy around him.
en_uk_webbe~PSA~C049~004~(C050:004) He calls to the heavens above, to the earth, that he may judge his people:
en_uk_webbe~PSA~C049~005~(C050:005) “Gather my saints together to me, those who have made a covenant with me by sacrifice.”
en_uk_webbe~PSA~C049~006~(C050:006) The heavens shall declare his righteousness, for God himself is judge. Selah.
en_uk_webbe~PSA~C049~007~(C050:007) “Hear, my people, and I will speak. Israel, I will testify against you. I am God, your God.
en_uk_webbe~PSA~C049~008~(C050:008) I don’t rebuke you for your sacrifices. Your burnt offerings are continually before me.
en_uk_webbe~PSA~C049~009~(C050:009) I have no need for a bull from your stall, nor male goats from your pens.
en_uk_webbe~PSA~C049~010~(C050:010) For every animal of the forest is mine, and the livestock on a thousand hills.
en_uk_webbe~PSA~C049~011~(C050:011) I know all the birds of the mountains. The wild animals of the field are mine.
en_uk_webbe~PSA~C049~012~(C050:012) If I were hungry, I would not tell you, for the world is mine, and all that is in it.
en_uk_webbe~PSA~C049~013~(C050:013) Will I eat the meat of bulls, or drink the blood of goats?
en_uk_webbe~PSA~C049~014~(C050:014) Offer to God the sacrifice of thanksgiving. Pay your vows to the Most High.
en_uk_webbe~PSA~C049~015~(C050:015) Call on me in the day of trouble. I will deliver you, and you will honour me.”
en_uk_webbe~PSA~C049~016~(C050:016) But to the wicked God says, “What right do you have to declare my statutes, that you have taken my covenant on your lips,
en_uk_webbe~PSA~C049~017~(C050:017) since you hate instruction, and throw my words behind you?
en_uk_webbe~PSA~C049~018~(C050:018) When you saw a thief, you consented with him, and have participated with adulterers.
en_uk_webbe~PSA~C049~019~(C050:019) “You give your mouth to evil. Your tongue frames deceit.
en_uk_webbe~PSA~C049~020~(C050:020) You sit and speak against your brother. You slander your own mother’s son.
en_uk_webbe~PSA~C049~021~(C050:021) You have done these things, and I kept silent. You thought that I was just like you. I will rebuke you, and accuse you in front of your eyes.
en_uk_webbe~PSA~C049~022~(C050:022) “Now consider this, you who forget God, lest I tear you into pieces, and there be no one to deliver.
en_uk_webbe~PSA~C049~023~(C050:023) Whoever offers the sacrifice of thanksgiving glorifies me, and prepares his way so that I will show God’s salvation to him.”
