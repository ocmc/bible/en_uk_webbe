en_uk_webbe~PRM~C12~01~O LORD Almighty, that are in heaven, you God of our fathers, of Abraham, and Isaac, and Jacob, and of their righteous seed;
en_uk_webbe~PRM~C12~02~who have made heaven and earth, with all the ornament thereof;
en_uk_webbe~PRM~C12~03~who have bound the sea by the word of your commandment; who have shut up the deep, and sealed it by your terrible and glorious name;
en_uk_webbe~PRM~C12~04~whom all things fear, yes, tremble before your power;
en_uk_webbe~PRM~C12~05~for the majesty of your glory can’t be borne, and the anger of your threatening towards sinners is importable:
en_uk_webbe~PRM~C12~06~your merciful promise is unmeasurable and unsearchable;
en_uk_webbe~PRM~C12~07~for you are the Lord Most High, of great compassion, patient and abundant in mercy, and repent of bringing evils upon men.
en_uk_webbe~PRM~C12~08~You, O Lord, according to your great goodness have promised repentance and forgiveness to those who have sinned against you: and of your infinite mercies have appointed repentance to sinners, that they may be saved. You therefore, O Lord, that are the God of the just, have not appointed repentance to the just, to Abraham, and Isaac, and Jacob, which have not sinned against you; but you have appointed repentance to me that am a sinner:
en_uk_webbe~PRM~C12~09~for I have sinned above the number of the sands of the sea. My transgressions are multiplied, O Lord: my transgressions are multiplied, and I am not worthy to behold and see the height of heaven for the multitude of my iniquities.
en_uk_webbe~PRM~C12~10~I am bowed down with many iron bands, that I can’t lift up my head by reason of my sins, neither have I any respite: for I have provoked your wrath, and done that which is evil before you: I didn’t do your will, neither did I keep your commandments: I have set up abominations, and have multiplied detestable things.
en_uk_webbe~PRM~C12~11~Now therefore I bow the knee of my heart, beseeching you of grace.
en_uk_webbe~PRM~C12~12~I have sinned, O Lord, I have sinned, and I acknowledge my iniquities:
en_uk_webbe~PRM~C12~13~but, I humbly beseech you, forgive me, O Lord, forgive me, and destroy me not with my iniquities. Be not angry with me forever, by reserving evil for me; neither condemn me into the lower parts of the earth. For you, O Lord, are the God of those who repent;
en_uk_webbe~PRM~C12~14~and in me you will show all your goodness: for you will save me, that am unworthy, according to your great mercy.
en_uk_webbe~PRM~C12~15~And I will praise you forever all the days of my life: for all the army of heaven sings your praise, and yours is the glory forever and ever. Amen.
