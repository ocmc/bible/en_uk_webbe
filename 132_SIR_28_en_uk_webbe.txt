en_uk_webbe~SIR~C28~01~He that takes vengeance will find vengeance from the Lord; And he will surely make firm his sins.
en_uk_webbe~SIR~C28~02~Forgive your neighbour the hurt that he has done [you; ] And then your sins will be pardoned when you pray.
en_uk_webbe~SIR~C28~03~Man cherishes anger against man; And does he seek healing from the Lord?
en_uk_webbe~SIR~C28~04~Upon a man like himself he has no mercy; And does he make supplication for his own sins?
en_uk_webbe~SIR~C28~05~He being himself flesh nourishes wrath: Who will make atonement for his sins?
en_uk_webbe~SIR~C28~06~Remember your last end, and cease from enmity: [Remember ] corruption and death, and remain in the commandments.
en_uk_webbe~SIR~C28~07~Remember the commandments, and be not angry with your neighbour; And [remember ] the covenant of the Highest, and wink at ignorance.
en_uk_webbe~SIR~C28~08~Abstain from strife, and you will diminish your sins: For a passionate man will kindle strife;
en_uk_webbe~SIR~C28~09~And a man that is a sinner will trouble friends, And will make debate amongst those who are at peace.
en_uk_webbe~SIR~C28~10~As is the fuel of the fire, so will it burn; And as the stoutness of the strife is, [so ] will it burn: As is the strength of the man, [so ] will be his wrath; And as is his wealth, [so ] he will exalt his anger.
en_uk_webbe~SIR~C28~11~A contention begun in haste kindles a fire; And a hasty fighting sheds blood.
en_uk_webbe~SIR~C28~12~If you blow a spark, it will burn; And if you spit upon it, it will be quenched: And both these shall come out of your mouth.
en_uk_webbe~SIR~C28~13~Curse the whisperer and double-tongued: For he has destroyed many that were at peace.
en_uk_webbe~SIR~C28~14~A third person’s tongue has shaken many, And dispersed them from nation to nation; And it has pulled down strong cities, And overthrown the houses of great men.
en_uk_webbe~SIR~C28~15~A third person’s tongue has cast out brave women, And deprived them of their labours.
en_uk_webbe~SIR~C28~16~He that listens to it will not find rest, Nor shall he dwell quietly.
en_uk_webbe~SIR~C28~17~The stroke of a whip makes a mark in the flesh; But the stroke of a tongue will break bones.
en_uk_webbe~SIR~C28~18~Many have fallen by the edge of the sword: Yet not so many as those who have fallen because of the tongue.
en_uk_webbe~SIR~C28~19~Happy is he that is sheltered from it, That has not passed through the wrath thereof; That has not drawn its yoke, And has not been bound with its bands.
en_uk_webbe~SIR~C28~20~For the yoke thereof is a yoke of iron, And the bands thereof are bands of brass.
en_uk_webbe~SIR~C28~21~The death thereof is an evil death; And Hades were better than it.
en_uk_webbe~SIR~C28~22~It shall not have rule over godly men; And they shall not be burnt in its flame.
en_uk_webbe~SIR~C28~23~They that forsake the Lord shall fall into it; And it shall burn amongst them, and shall not be quenched: It shall be sent forth upon them as a lion; And as a leopard it shall destroy them.
en_uk_webbe~SIR~C28~24~Look that you hedge your possession about with thorns; Bind up your silver and your gold;
en_uk_webbe~SIR~C28~25~And make a balance and a weight for your words; And make a door and a bar for your mouth.
en_uk_webbe~SIR~C28~26~Take heed lest you slip therein; Lest you fall before one that lies in wait.
