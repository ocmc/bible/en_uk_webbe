en_uk_webbe~PRO~C18~01~A man who isolates himself pursues selfishness, and defies all sound judgement.
en_uk_webbe~PRO~C18~02~A fool has no delight in understanding, but only in revealing his own opinion.
en_uk_webbe~PRO~C18~03~When wickedness comes, contempt also comes, and with shame comes disgrace.
en_uk_webbe~PRO~C18~04~The words of a man’s mouth are like deep waters. The fountain of wisdom is like a flowing brook.
en_uk_webbe~PRO~C18~05~To be partial to the faces of the wicked is not good, nor to deprive the innocent of justice.
en_uk_webbe~PRO~C18~06~A fool’s lips come into strife, and his mouth invites beatings.
en_uk_webbe~PRO~C18~07~A fool’s mouth is his destruction, and his lips are a snare to his soul.
en_uk_webbe~PRO~C18~08~The words of a gossip are like dainty morsels: they go down into a person’s innermost parts.
en_uk_webbe~PRO~C18~09~One who is slack in his work is brother to him who is a master of destruction.
en_uk_webbe~PRO~C18~10~The LORD’s name is a strong tower: the righteous run to him, and are safe.
en_uk_webbe~PRO~C18~11~The rich man’s wealth is his strong city, like an unscalable wall in his own imagination.
en_uk_webbe~PRO~C18~12~Before destruction the heart of man is proud, but before honour is humility.
en_uk_webbe~PRO~C18~13~He who answers before he hears, that is folly and shame to him.
en_uk_webbe~PRO~C18~14~A man’s spirit will sustain him in sickness, but a crushed spirit, who can bear?
en_uk_webbe~PRO~C18~15~The heart of the discerning gets knowledge. The ear of the wise seeks knowledge.
en_uk_webbe~PRO~C18~16~A man’s gift makes room for him, and brings him before great men.
en_uk_webbe~PRO~C18~17~He who pleads his cause first seems right; until another comes and questions him.
en_uk_webbe~PRO~C18~18~The lot settles disputes, and keeps strong ones apart.
en_uk_webbe~PRO~C18~19~A brother offended is more difficult than a fortified city. Disputes are like the bars of a fortress.
en_uk_webbe~PRO~C18~20~A man’s stomach is filled with the fruit of his mouth. With the harvest of his lips he is satisfied.
en_uk_webbe~PRO~C18~21~Death and life are in the power of the tongue; those who love it will eat its fruit.
en_uk_webbe~PRO~C18~22~Whoever finds a wife finds a good thing, and obtains favour of the LORD.
en_uk_webbe~PRO~C18~23~The poor plead for mercy, but the rich answer harshly.
en_uk_webbe~PRO~C18~24~A man of many companions may be ruined, but there is a friend who sticks closer than a brother.
