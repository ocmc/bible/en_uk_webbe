en_uk_webbe~PSA~C036~001~(C037:001) (0) By David. (1) Don’t fret because of evildoers, neither be envious against those who work unrighteousness.
en_uk_webbe~PSA~C036~002~(C037:002) For they shall soon be cut down like the grass, and wither like the green herb.
en_uk_webbe~PSA~C036~003~(C037:003) Trust in the LORD, and do good. Dwell in the land, and enjoy safe pasture.
en_uk_webbe~PSA~C036~004~(C037:004) Also delight yourself in the LORD, and he will give you the desires of your heart.
en_uk_webbe~PSA~C036~005~(C037:005) Commit your way to the LORD. Trust also in him, and he will do this:
en_uk_webbe~PSA~C036~006~(C037:006) he will make your righteousness shine out like light, and your justice as the noon day sun.
en_uk_webbe~PSA~C036~007~(C037:007) Rest in the LORD, and wait patiently for him. Don’t fret because of him who prospers in his way, because of the man who makes wicked plots happen.
en_uk_webbe~PSA~C036~008~(C037:008) Cease from anger, and forsake wrath. Don’t fret; it leads only to evildoing.
en_uk_webbe~PSA~C036~009~(C037:009) For evildoers shall be cut off, but those who wait for the LORD shall inherit the land.
en_uk_webbe~PSA~C036~010~(C037:010) For yet a little while, and the wicked will be no more. Yes, though you look for his place, he isn’t there.
en_uk_webbe~PSA~C036~011~(C037:011) But the humble shall inherit the land, and shall delight themselves in the abundance of peace.
en_uk_webbe~PSA~C036~012~(C037:012) The wicked plots against the just, and gnashes at him with his teeth.
en_uk_webbe~PSA~C036~013~(C037:013) The Lord will laugh at him, for he sees that his day is coming.
en_uk_webbe~PSA~C036~014~(C037:014) The wicked have drawn out the sword, and have bent their bow, to cast down the poor and needy, to kill those who are upright on the path.
en_uk_webbe~PSA~C036~015~(C037:015) Their sword shall enter into their own heart. Their bows shall be broken.
en_uk_webbe~PSA~C036~016~(C037:016) Better is a little that the righteous has, than the abundance of many wicked.
en_uk_webbe~PSA~C036~017~(C037:017) For the arms of the wicked shall be broken, but the LORD upholds the righteous.
en_uk_webbe~PSA~C036~018~(C037:018) The LORD knows the days of the perfect. Their inheritance shall be forever.
en_uk_webbe~PSA~C036~019~(C037:019) They shall not be disappointed in the time of evil. In the days of famine they shall be satisfied.
en_uk_webbe~PSA~C036~020~(C037:020) But the wicked shall perish. The enemies of the LORD shall be like the beauty of the fields. They will vanish— vanish like smoke.
en_uk_webbe~PSA~C036~021~(C037:021) The wicked borrow, and don’t pay back, but the righteous give generously.
en_uk_webbe~PSA~C036~022~(C037:022) For such as are blessed by him shall inherit the land. Those who are cursed by him shall be cut off.
en_uk_webbe~PSA~C036~023~(C037:023) A man’s steps are established by the LORD. He delights in his way.
en_uk_webbe~PSA~C036~024~(C037:024) Though he stumble, he shall not fall, for the LORD holds him up with his hand.
en_uk_webbe~PSA~C036~025~(C037:025) I have been young, and now am old, yet I have not seen the righteous forsaken, nor his children begging for bread.
en_uk_webbe~PSA~C036~026~(C037:026) All day long he deals graciously, and lends. His offspring is blessed.
en_uk_webbe~PSA~C036~027~(C037:027) Depart from evil, and do good. Live securely forever.
en_uk_webbe~PSA~C036~028~(C037:028) For the LORD loves justice, and doesn’t forsake his saints. They are preserved forever, but the children of the wicked shall be cut off.
en_uk_webbe~PSA~C036~029~(C037:029) The righteous shall inherit the land, and live in it forever.
en_uk_webbe~PSA~C036~030~(C037:030) The mouth of the righteous talks of wisdom. His tongue speaks justice.
en_uk_webbe~PSA~C036~031~(C037:031) The law of his God is in his heart. None of his steps shall slide.
en_uk_webbe~PSA~C036~032~(C037:032) The wicked watch the righteous, and seek to kill him.
en_uk_webbe~PSA~C036~033~(C037:033) The LORD will not leave him in his hand, nor condemn him when he is judged.
en_uk_webbe~PSA~C036~034~(C037:034) Wait for the LORD, and keep his way, and he will exalt you to inherit the land. When the wicked are cut off, you shall see it.
en_uk_webbe~PSA~C036~035~(C037:035) I have seen the wicked in great power, spreading himself like a green tree in its native soil.
en_uk_webbe~PSA~C036~036~(C037:036) But he passed away, and behold, he was not. Yes, I sought him, but he could not be found.
en_uk_webbe~PSA~C036~037~(C037:037) Mark the perfect man, and see the upright, for there is a future for the man of peace.
en_uk_webbe~PSA~C036~038~(C037:038) As for transgressors, they shall be destroyed together. The future of the wicked shall be cut off.
en_uk_webbe~PSA~C036~039~(C037:039) But the salvation of the righteous is from the LORD. He is their stronghold in the time of trouble.
en_uk_webbe~PSA~C036~040~(C037:040) The LORD helps them and rescues them. He rescues them from the wicked and saves them, because they have taken refuge in him.
