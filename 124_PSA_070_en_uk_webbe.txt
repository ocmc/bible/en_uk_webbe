en_uk_webbe~PSA~C070~001~(C071:001) In you, LORD, I take refuge. Never let me be disappointed.
en_uk_webbe~PSA~C070~002~(C071:002) Deliver me in your righteousness, and rescue me. Turn your ear to me, and save me.
en_uk_webbe~PSA~C070~003~(C071:003) Be to me a rock of refuge to which I may always go. Give the command to save me, for you are my rock and my fortress.
en_uk_webbe~PSA~C070~004~(C071:004) Rescue me, my God, from the hand of the wicked, from the hand of the unrighteous and cruel man.
en_uk_webbe~PSA~C070~005~(C071:005) For you are my hope, Lord GOD, my confidence from my youth.
en_uk_webbe~PSA~C070~006~(C071:006) I have relied on you from the womb. You are he who took me out of my mother’s womb. I will always praise you.
en_uk_webbe~PSA~C070~007~(C071:007) I am a marvel to many, but you are my strong refuge.
en_uk_webbe~PSA~C070~008~(C071:008) My mouth shall be filled with your praise, with your honour all day long.
en_uk_webbe~PSA~C070~009~(C071:009) Don’t reject me in my old age. Don’t forsake me when my strength fails.
en_uk_webbe~PSA~C070~010~(C071:010) For my enemies talk about me. Those who watch for my soul conspire together,
en_uk_webbe~PSA~C070~011~(C071:011) saying, “God has forsaken him. Pursue and take him, for no one will rescue him.”
en_uk_webbe~PSA~C070~012~(C071:012) God, don’t be far from me. My God, hurry to help me.
en_uk_webbe~PSA~C070~013~(C071:013) Let my accusers be disappointed and consumed. Let them be covered with disgrace and scorn who want to harm me.
en_uk_webbe~PSA~C070~014~(C071:014) But I will always hope, and will add to all of your praise.
en_uk_webbe~PSA~C070~015~(C071:015) My mouth will tell about your righteousness, and of your salvation all day, though I don’t know its full measure.
en_uk_webbe~PSA~C070~016~(C071:016) I will come with the mighty acts of the Lord GOD. I will make mention of your righteousness, even of yours alone.
en_uk_webbe~PSA~C070~017~(C071:017) God, you have taught me from my youth. Until now, I have declared your wondrous works.
en_uk_webbe~PSA~C070~018~(C071:018) Yes, even when I am old and grey-haired, God, don’t forsake me, until I have declared your strength to the next generation,
en_uk_webbe~PSA~C070~019~(C071:018) your might to everyone who is to come.
en_uk_webbe~PSA~C070~020~(C071:020) You, who have shown us many and bitter troubles, you will let me live. You will bring us up again from the depths of the earth.
en_uk_webbe~PSA~C070~021~(C071:021) Increase my honour and comfort me again.
en_uk_webbe~PSA~C070~022~(C071:022) I will also praise you with the harp for your faithfulness, my God. I sing praises to you with the lyre, Holy One of Israel.
en_uk_webbe~PSA~C070~023~(C071:023) My lips shall shout for joy! My soul, which you have redeemed, sings praises to you!
en_uk_webbe~PSA~C070~024~(C071:024) My tongue will also talk about your righteousness all day long, for they are disappointed, and they are confounded, who want to harm me.
