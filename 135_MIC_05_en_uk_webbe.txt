en_uk_webbe~MIC~C05~01~Now you shall gather yourself in troops, daughter of troops. He has laid siege against us. They will strike the judge of Israel with a rod on the cheek.
en_uk_webbe~MIC~C05~02~But you, Bethlehem Ephrathah, being small amongst the clans of Judah, out of you one will come out to me that is to be ruler in Israel; whose goings out are from of old, from ancient times.
en_uk_webbe~MIC~C05~03~Therefore he will abandon them until the time that she who is in labour gives birth. Then the rest of his brothers will return to the children of Israel.
en_uk_webbe~MIC~C05~04~He shall stand, and shall shepherd in the strength of the LORD, in the majesty of the name of the LORD his God: and they will live, for then he will be great to the ends of the earth.
en_uk_webbe~MIC~C05~05~He will be our peace when Assyria invades our land, and when he marches through our fortresses, then we will raise against him seven shepherds, and eight leaders of men.
en_uk_webbe~MIC~C05~06~They will rule the land of Assyria with the sword, and the land of Nimrod in its gates. He will deliver us from the Assyrian, when he invades our land, and when he marches within our border.
en_uk_webbe~MIC~C05~07~The remnant of Jacob will be amongst many peoples, like dew from the LORD, like showers on the grass, that don’t wait for man, nor wait for the sons of men.
en_uk_webbe~MIC~C05~08~The remnant of Jacob will be amongst the nations, amongst many peoples, like a lion amongst the animals of the forest, like a young lion amongst the flocks of sheep; who, if he goes through, treads down and tears in pieces, and there is no one to deliver.
en_uk_webbe~MIC~C05~09~Let your hand be lifted up above your adversaries, and let all of your enemies be cut off.
en_uk_webbe~MIC~C05~10~“It will happen in that day”, says the LORD, “that I will cut off your horses out from amongst you, and will destroy your chariots.
en_uk_webbe~MIC~C05~11~I will cut off the cities of your land, and will tear down all your strongholds.
en_uk_webbe~MIC~C05~12~I will destroy witchcraft from your hand; and you shall have no soothsayers.
en_uk_webbe~MIC~C05~13~I will cut off your engraved images and your pillars out from amongst you; and you shall no more worship the work of your hands.
en_uk_webbe~MIC~C05~14~I will uproot your Asherah poles out from amongst you; and I will destroy your cities.
en_uk_webbe~MIC~C05~15~I will execute vengeance in anger, and wrath on the nations that didn’t listen.”
