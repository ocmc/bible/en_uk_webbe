en_uk_webbe~PSA~C113~001~(C114:001) When Israel went out of Egypt, the house of Jacob from a people of foreign language,
en_uk_webbe~PSA~C113~002~(C114:002) Judah became his sanctuary, Israel his dominion.
en_uk_webbe~PSA~C113~003~(C114:003) The sea saw it, and fled. The Jordan was driven back.
en_uk_webbe~PSA~C113~004~(C114:004) The mountains skipped like rams, the little hills like lambs.
en_uk_webbe~PSA~C113~005~(C114:005) What was it, you sea, that you fled? You Jordan, that you turned back?
en_uk_webbe~PSA~C113~006~(C114:006) You mountains, that you skipped like rams; you little hills, like lambs?
en_uk_webbe~PSA~C113~007~(C114:007) Tremble, you earth, at the presence of the Lord, at the presence of the God of Jacob,
en_uk_webbe~PSA~C113~008~(C114:008) who turned the rock into a pool of water, the flint into a spring of waters.
en_uk_webbe~PSA~C113~009~(C115:001) Not to us, LORD, not to us, but to your name give glory, for your loving kindness, and for your truth’s sake.
en_uk_webbe~PSA~C113~010~(C115:002) Why should the nations say, “Where is their God, now?”
en_uk_webbe~PSA~C113~011~(C115:003) But our God is in the heavens. He does whatever he pleases.
en_uk_webbe~PSA~C113~012~(C115:004) Their idols are silver and gold, the work of men’s hands.
en_uk_webbe~PSA~C113~013~(C115:005) They have mouths, but they don’t speak. They have eyes, but they don’t see.
en_uk_webbe~PSA~C113~014~(C115:006) They have ears, but they don’t hear. They have noses, but they don’t smell.
en_uk_webbe~PSA~C113~015~(C115:007) They have hands, but they don’t feel. They have feet, but they don’t walk, neither do they speak through their throat.
en_uk_webbe~PSA~C113~016~(C115:008) Those who make them will be like them; yes, everyone who trusts in them.
en_uk_webbe~PSA~C113~017~(C115:009) Israel, trust in the LORD! He is their help and their shield.
en_uk_webbe~PSA~C113~018~(C115:010) House of Aaron, trust in the LORD! He is their help and their shield.
en_uk_webbe~PSA~C113~019~(C115:011) You who fear the LORD, trust in the LORD! He is their help and their shield.
en_uk_webbe~PSA~C113~020~(C115:012) The LORD remembers us. He will bless us. He will bless the house of Israel. He will bless the house of Aaron.
en_uk_webbe~PSA~C113~021~(C115:013) He will bless those who fear the LORD, both small and great.
en_uk_webbe~PSA~C113~022~(C115:014) May the LORD increase you more and more, you and your children.
en_uk_webbe~PSA~C113~023~(C115:015) Blessed are you by the LORD, who made heaven and earth.
en_uk_webbe~PSA~C113~024~(C115:016) The heavens are the LORD’s heavens, but he has given the earth to the children of men.
en_uk_webbe~PSA~C113~025~(C115:017) The dead don’t praise the LORD, neither any who go down into silence;
en_uk_webbe~PSA~C113~026~(C115:018) but we will bless the LORD, from this time forward and forever more. Praise the LORD!
