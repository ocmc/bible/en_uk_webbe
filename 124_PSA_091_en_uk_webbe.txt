en_uk_webbe~PSA~C091~001~(C092:000) A Psalm. A song for the Sabbath day.
en_uk_webbe~PSA~C091~002~(C092:001) It is a good thing to give thanks to the LORD, to sing praises to your name, Most High,
en_uk_webbe~PSA~C091~003~(C092:002) to proclaim your loving kindness in the morning, and your faithfulness every night,
en_uk_webbe~PSA~C091~004~(C092:003) with the ten-stringed lute, with the harp, and with the melody of the lyre.
en_uk_webbe~PSA~C091~005~(C092:004) For you, LORD, have made me glad through your work. I will triumph in the works of your hands.
en_uk_webbe~PSA~C091~006~(C092:005) How great are your works, LORD! Your thoughts are very deep.
en_uk_webbe~PSA~C091~007~(C092:006) A senseless man doesn’t know, neither does a fool understand this:
en_uk_webbe~PSA~C091~008~(C092:007) though the wicked spring up as the grass, and all the evildoers flourish, they will be destroyed forever.
en_uk_webbe~PSA~C091~009~(C092:008) But you, LORD, are on high forever more.
en_uk_webbe~PSA~C091~010~(C092:009) For, behold, your enemies, LORD, for, behold, your enemies shall perish. All the evildoers will be scattered.
en_uk_webbe~PSA~C091~011~(C092:010) But you have exalted my horn like that of the wild ox. I am anointed with fresh oil.
en_uk_webbe~PSA~C091~012~(C092:011) My eye has also seen my enemies. My ears have heard of the wicked enemies who rise up against me.
en_uk_webbe~PSA~C091~013~(C092:012) The righteous shall flourish like the palm tree. He will grow like a cedar in Lebanon.
en_uk_webbe~PSA~C091~014~(C092:013) They are planted in the LORD’s house. They will flourish in our God’s courts.
en_uk_webbe~PSA~C091~015~(C092:014) They will still produce fruit in old age. They will be full of sap and green,
en_uk_webbe~PSA~C091~016~(C092:015) to show that the LORD is upright. He is my rock, and there is no unrighteousness in him.
