en_uk_webbe~ZEC~C08~01~The word of the LORD of Armies came to me.
en_uk_webbe~ZEC~C08~02~The LORD of Armies says: “I am jealous for Zion with great jealousy, and I am jealous for her with great wrath.”
en_uk_webbe~ZEC~C08~03~The LORD says: “I have returned to Zion, and will dwell in the middle of Jerusalem. Jerusalem shall be called ‘The City of Truth;’ and the mountain of the LORD of Armies, ‘The Holy Mountain.’”
en_uk_webbe~ZEC~C08~04~The LORD of Armies says: “Old men and old women will again dwell in the streets of Jerusalem, every man with his staff in his hand for very age.
en_uk_webbe~ZEC~C08~05~The streets of the city will be full of boys and girls playing in its streets.”
en_uk_webbe~ZEC~C08~06~The LORD of Armies says: “If it is marvellous in the eyes of the remnant of this people in those days, should it also be marvellous in my eyes?” says the LORD of Armies.
en_uk_webbe~ZEC~C08~07~The LORD of Armies says: “Behold, I will save my people from the east country, and from the west country;
en_uk_webbe~ZEC~C08~08~and I will bring them, and they will dwell within Jerusalem; and they will be my people, and I will be their God, in truth and in righteousness.”
en_uk_webbe~ZEC~C08~09~The LORD of Armies says: “Let your hands be strong, you who hear in these days these words from the mouth of the prophets who were in the day that the foundation of the house of the LORD of Armies was laid, even the temple, that it might be built.
en_uk_webbe~ZEC~C08~10~For before those days there was no wages for man, nor any wages for an animal; neither was there any peace to him who went out or came in, because of the adversary. For I set all men everyone against his neighbour.
en_uk_webbe~ZEC~C08~11~But now I will not be to the remnant of this people as in the former days,” says the LORD of Armies.
en_uk_webbe~ZEC~C08~12~“For the seed of peace and the vine will yield its fruit, and the ground will give its increase, and the heavens will give their dew; and I will cause the remnant of this people to inherit all these things.
en_uk_webbe~ZEC~C08~13~It shall come to pass that, as you were a curse amongst the nations, house of Judah and house of Israel, so I will save you, and you shall be a blessing. Don’t be afraid. Let your hands be strong.”
en_uk_webbe~ZEC~C08~14~For the LORD of Armies says: “As I thought to do evil to you, when your fathers provoked me to wrath,” says the LORD of Armies, “and I didn’t repent;
en_uk_webbe~ZEC~C08~15~so again I have thought in these days to do good to Jerusalem and to the house of Judah. Don’t be afraid.
en_uk_webbe~ZEC~C08~16~These are the things that you shall do: speak every man the truth with his neighbour. Execute the judgement of truth and peace in your gates,
en_uk_webbe~ZEC~C08~17~and let none of you devise evil in your hearts against his neighbour, and love no false oath: for all these are things that I hate,” says the LORD.
en_uk_webbe~ZEC~C08~18~The word of the LORD of Armies came to me.
en_uk_webbe~ZEC~C08~19~The LORD of Armies says: “The fasts of the fourth, fifth, seventh, and tenth months shall be for the house of Judah joy and gladness, and cheerful feasts. Therefore love truth and peace.”
en_uk_webbe~ZEC~C08~20~The LORD of Armies says: “Many peoples, and the inhabitants of many cities will yet come;
en_uk_webbe~ZEC~C08~21~and the inhabitants of one shall go to another, saying, ‘Let’s go speedily to entreat the favour of the LORD, and to seek the LORD of Armies. I will go also.’
en_uk_webbe~ZEC~C08~22~Yes, many peoples and strong nations will come to seek the LORD of Armies in Jerusalem, and to entreat the favour of the LORD.”
en_uk_webbe~ZEC~C08~23~The LORD of Armies says: “In those days, ten men will take hold, out of all the languages of the nations, they will take hold of the skirt of him who is a Jew, saying, ‘We will go with you, for we have heard that God is with you.’”
