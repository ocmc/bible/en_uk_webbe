en_uk_webbe~CO1~C13~01~If I speak with the languages of men and of angels, but don’t have love, I have become sounding brass, or a clanging cymbal.
en_uk_webbe~CO1~C13~02~If I have the gift of prophecy, and know all mysteries and all knowledge; and if I have all faith, so as to remove mountains, but don’t have love, I am nothing.
en_uk_webbe~CO1~C13~03~If I give away all my goods to feed the poor, and if I give my body to be burnt, but don’t have love, it profits me nothing.
en_uk_webbe~CO1~C13~04~Love is patient and is kind. Love doesn’t envy. Love doesn’t brag, is not proud,
en_uk_webbe~CO1~C13~05~doesn’t behave itself inappropriately, doesn’t seek its own way, is not provoked, takes no account of evil;
en_uk_webbe~CO1~C13~06~doesn’t rejoice in unrighteousness, but rejoices with the truth;
en_uk_webbe~CO1~C13~07~bears all things, believes all things, hopes all things, and endures all things.
en_uk_webbe~CO1~C13~08~Love never fails. But where there are prophecies, they will be done away with. Where there are various languages, they will cease. Where there is knowledge, it will be done away with.
en_uk_webbe~CO1~C13~09~For we know in part and we prophesy in part;
en_uk_webbe~CO1~C13~10~but when that which is complete has come, then that which is partial will be done away with.
en_uk_webbe~CO1~C13~11~When I was a child, I spoke as a child, I felt as a child, I thought as a child. Now that I have become a man, I have put away childish things.
en_uk_webbe~CO1~C13~12~For now we see in a mirror, dimly, but then face to face. Now I know in part, but then I will know fully, even as I was also fully known.
en_uk_webbe~CO1~C13~13~But now faith, hope, and love remain—these three. The greatest of these is love.
