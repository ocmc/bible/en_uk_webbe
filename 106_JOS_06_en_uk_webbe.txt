en_uk_webbe~JOS~C06~01~Now Jericho was tightly shut up because of the children of Israel. No one went out, and no one came in.
en_uk_webbe~JOS~C06~02~The LORD said to Joshua, “Behold, I have given Jericho into your hand, with its king and the mighty men of valour.
en_uk_webbe~JOS~C06~03~All of your men of war shall march around the city, going around the city once. You shall do this six days.
en_uk_webbe~JOS~C06~04~Seven priests shall bear seven trumpets of rams’ horns before the ark. On the seventh day, you shall march around the city seven times, and the priests shall blow the trumpets.
en_uk_webbe~JOS~C06~05~It shall be that when they make a long blast with the ram’s horn, and when you hear the sound of the trumpet, all the people shall shout with a great shout; and the city wall shall fall down flat, and the people shall go up, every man straight in front of him.”
en_uk_webbe~JOS~C06~06~Joshua the son of Nun called the priests, and said to them, “Take up the ark of the covenant, and let seven priests bear seven trumpets of rams’ horns before the LORD’s ark.”
en_uk_webbe~JOS~C06~07~They said to the people, “Advance! March around the city, and let the armed men pass on before the LORD’s ark.”
en_uk_webbe~JOS~C06~08~It was so, that when Joshua had spoken to the people, the seven priests bearing the seven trumpets of rams’ horns before the LORD advanced and blew the trumpets, and the ark of the LORD’s covenant followed them.
en_uk_webbe~JOS~C06~09~The armed men went before the priests who blew the trumpets, and the ark went after them. The trumpets sounded as they went.
en_uk_webbe~JOS~C06~10~Joshua commanded the people, saying, “You shall not shout nor let your voice be heard, neither shall any word proceed out of your mouth until the day I tell you to shout. Then you shall shout.”
en_uk_webbe~JOS~C06~11~So he caused the LORD’s ark to go around the city, circling it once. Then they came into the camp, and stayed in the camp.
en_uk_webbe~JOS~C06~12~Joshua rose early in the morning, and the priests took up the LORD’s ark.
en_uk_webbe~JOS~C06~13~The seven priests bearing the seven trumpets of rams’ horns in front of the LORD’s ark went on continually, and blew the trumpets. The armed men went in front of them. The rear guard came after the LORD’s ark. The trumpets sounded as they went.
en_uk_webbe~JOS~C06~14~The second day they marched around the city once, and returned into the camp. They did this six days.
en_uk_webbe~JOS~C06~15~On the seventh day, they rose early at the dawning of the day, and marched around the city in the same way seven times. On this day only they marched around the city seven times.
en_uk_webbe~JOS~C06~16~At the seventh time, when the priests blew the trumpets, Joshua said to the people, “Shout, for the LORD has given you the city!
en_uk_webbe~JOS~C06~17~The city shall be devoted, even it and all that is in it, to the LORD. Only Rahab the prostitute shall live, she and all who are with her in the house, because she hid the messengers that we sent.
en_uk_webbe~JOS~C06~18~But as for you, only keep yourselves from what is devoted to destruction, lest when you have devoted it, you take of the devoted thing; so you would make the camp of Israel accursed and trouble it.
en_uk_webbe~JOS~C06~19~But all the silver, gold, and vessels of bronze and iron are holy to the LORD. They shall come into the LORD’s treasury.”
en_uk_webbe~JOS~C06~20~So the people shouted and the priests blew the trumpets. When the people heard the sound of the trumpet, the people shouted with a great shout, and the wall fell down flat, so that the people went up into the city, every man straight in front of him, and they took the city.
en_uk_webbe~JOS~C06~21~They utterly destroyed all that was in the city, both man and woman, both young and old, and ox, sheep, and donkey, with the edge of the sword.
en_uk_webbe~JOS~C06~22~Joshua said to the two men who had spied out the land, “Go into the prostitute’s house, and bring the woman and all that she has out from there, as you swore to her.”
en_uk_webbe~JOS~C06~23~The young men who were spies went in, and brought out Rahab with her father, her mother, her brothers, and all that she had. They also brought out all of her relatives, and they set them outside of the camp of Israel.
en_uk_webbe~JOS~C06~24~They burnt the city with fire, and all that was in it. Only they put the silver, the gold, and the vessels of bronze and of iron into the treasury of the LORD’s house.
en_uk_webbe~JOS~C06~25~But Rahab the prostitute, her father’s household, and all that she had, Joshua saved alive. She lives in the middle of Israel to this day, because she hid the messengers whom Joshua sent to spy out Jericho.
en_uk_webbe~JOS~C06~26~Joshua commanded them with an oath at that time, saying, “Cursed is the man before the LORD who rises up and builds this city Jericho. With the loss of his firstborn he will lay its foundation, and with the loss of his youngest son he will set up its gates.”
en_uk_webbe~JOS~C06~27~So the LORD was with Joshua; and his fame was in all the land.
