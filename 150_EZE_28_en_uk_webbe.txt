en_uk_webbe~EZE~C28~01~The LORD’s word came again to me, saying,
en_uk_webbe~EZE~C28~02~“Son of man, tell the prince of Tyre, ‘The Lord GOD says: “Because your heart is lifted up, and you have said, ‘I am a god, I sit in the seat of God, in the middle of the seas;’ yet you are man, and not God, though you set your heart as the heart of God—
en_uk_webbe~EZE~C28~03~behold, you are wiser than Daniel; there is no secret that is hidden from you;
en_uk_webbe~EZE~C28~04~by your wisdom and by your understanding you have gotten yourself riches, and have gotten gold and silver into your treasures;
en_uk_webbe~EZE~C28~05~by your great wisdom and by your trading you have increased your riches, and your heart is lifted up because of your riches—”
en_uk_webbe~EZE~C28~06~“‘therefore the Lord GOD says: “Because you have set your heart as the heart of God,
en_uk_webbe~EZE~C28~07~therefore, behold, I will bring strangers on you, the terrible of the nations. They will draw their swords against the beauty of your wisdom. They will defile your brightness.
en_uk_webbe~EZE~C28~08~They will bring you down to the pit. You will die the death of those who are slain in the heart of the seas.
en_uk_webbe~EZE~C28~09~Will you yet say before him who kills you, ‘I am God’? But you are man, and not God, in the hand of him who wounds you.
en_uk_webbe~EZE~C28~10~You will die the death of the uncircumcised by the hand of strangers; for I have spoken it,” says the Lord GOD.’”
en_uk_webbe~EZE~C28~11~Moreover the LORD’s word came to me, saying,
en_uk_webbe~EZE~C28~12~“Son of man, take up a lamentation over the king of Tyre, and tell him, ‘The Lord GOD says: “You were the seal of full measure, full of wisdom, and perfect in beauty.
en_uk_webbe~EZE~C28~13~You were in Eden, the garden of God. Every precious stone adorned you: ruby, topaz, emerald, chrysolite, onyx, jasper, sapphire, turquoise, and beryl. Gold work of tambourines and of pipes was in you. They were prepared in the day that you were created.
en_uk_webbe~EZE~C28~14~You were the anointed cherub who covers. Then I set you up on the holy mountain of God. You have walked up and down in the middle of the stones of fire.
en_uk_webbe~EZE~C28~15~You were perfect in your ways from the day that you were created, until unrighteousness was found in you.
en_uk_webbe~EZE~C28~16~By the abundance of your commerce, your insides were filled with violence, and you have sinned. Therefore I have cast you as profane out of God’s mountain. I have destroyed you, covering cherub, from the middle of the stones of fire.
en_uk_webbe~EZE~C28~17~Your heart was lifted up because of your beauty. You have corrupted your wisdom by reason of your splendour. I have cast you to the ground. I have laid you before kings, that they may see you.
en_uk_webbe~EZE~C28~18~By the multitude of your iniquities, in the unrighteousness of your commerce, you have profaned your sanctuaries. Therefore I have brought out a fire from the middle of you. It has devoured you. I have turned you to ashes on the earth in the sight of all those who see you.
en_uk_webbe~EZE~C28~19~All those who know you amongst the peoples will be astonished at you. You have become a terror, and you will exist no more.”’”
en_uk_webbe~EZE~C28~20~The LORD’s word came to me, saying,
en_uk_webbe~EZE~C28~21~“Son of man, set your face towards Sidon, and prophesy against it,
en_uk_webbe~EZE~C28~22~and say, ‘The Lord GOD says: “Behold, I am against you, Sidon. I will be glorified amongst you. Then they will know that I am the LORD, when I have executed judgements in her, and am sanctified in her.
en_uk_webbe~EZE~C28~23~For I will send pestilence into her, and blood into her streets. The wounded will fall within her, with the sword on her on every side. Then they will know that I am the LORD.
en_uk_webbe~EZE~C28~24~“‘“There will be no more a pricking brier to the house of Israel, nor a hurting thorn of any that are around them that scorned them. Then they will know that I am the Lord GOD.”
en_uk_webbe~EZE~C28~25~“‘The Lord GOD says: “When I have gathered the house of Israel from the peoples amongst whom they are scattered, and am sanctified in them in the sight of the nations, then they will dwell in their own land which I gave to my servant Jacob.
en_uk_webbe~EZE~C28~26~They will dwell in it securely. Yes, they will build houses, plant vineyards, and will dwell securely, when I have executed judgements on all those who scorn them all around. Then they will know that I am the LORD their God.”’”
