en_uk_webbe~ISA~C43~01~But now the LORD who created you, Jacob, and he who formed you, Israel, says: “Don’t be afraid, for I have redeemed you. I have called you by your name. You are mine.
en_uk_webbe~ISA~C43~02~When you pass through the waters, I will be with you, and through the rivers, they will not overflow you. When you walk through the fire, you will not be burnt, and flame will not scorch you.
en_uk_webbe~ISA~C43~03~For I am the LORD your God, the Holy One of Israel, your Saviour. I have given Egypt as your ransom, Ethiopia and Seba in your place.
en_uk_webbe~ISA~C43~04~Since you have been precious and honoured in my sight, and I have loved you, therefore I will give people in your place, and nations instead of your life.
en_uk_webbe~ISA~C43~05~Don’t be afraid, for I am with you. I will bring your offspring from the east, and gather you from the west.
en_uk_webbe~ISA~C43~06~I will tell the north, ‘Give them up!’ and tell the south, ‘Don’t hold them back! Bring my sons from far away, and my daughters from the ends of the earth—
en_uk_webbe~ISA~C43~07~everyone who is called by my name, and whom I have created for my glory, whom I have formed, yes, whom I have made.’”
en_uk_webbe~ISA~C43~08~Bring out the blind people who have eyes, and the deaf who have ears.
en_uk_webbe~ISA~C43~09~Let all the nations be gathered together, and let the peoples be assembled. Who amongst them can declare this, and show us former things? Let them bring their witnesses, that they may be justified, or let them hear, and say, “That is true.”
en_uk_webbe~ISA~C43~10~“You are my witnesses,” says the LORD, “With my servant whom I have chosen; that you may know and believe me, and understand that I am he. Before me there was no God formed, neither will there be after me.
en_uk_webbe~ISA~C43~11~I myself am the LORD. Besides me, there is no saviour.
en_uk_webbe~ISA~C43~12~I have declared, I have saved, and I have shown, and there was no strange god amongst you. Therefore you are my witnesses”, says the LORD, “and I am God.
en_uk_webbe~ISA~C43~13~Yes, since the day was, I am he. There is no one who can deliver out of my hand. I will work, and who can hinder it?”
en_uk_webbe~ISA~C43~14~The LORD, your Redeemer, the Holy One of Israel says: “For your sake, I have sent to Babylon, and I will bring all of them down as fugitives, even the Chaldeans, in the ships of their rejoicing.
en_uk_webbe~ISA~C43~15~I am the LORD, your Holy One, the Creator of Israel, your King.”
en_uk_webbe~ISA~C43~16~The LORD, who makes a way in the sea, and a path in the mighty waters,
en_uk_webbe~ISA~C43~17~who brings out the chariot and horse, the army and the mighty man (they lie down together, they shall not rise; they are extinct, they are quenched like a wick) says:
en_uk_webbe~ISA~C43~18~“Don’t remember the former things, and don’t consider the things of old.
en_uk_webbe~ISA~C43~19~Behold, I will do a new thing. It springs out now. Don’t you know it? I will even make a way in the wilderness, and rivers in the desert.
en_uk_webbe~ISA~C43~20~The animals of the field, the jackals and the ostriches, shall honour me, because I give water in the wilderness and rivers in the desert, to give drink to my people, my chosen,
en_uk_webbe~ISA~C43~21~the people which I formed for myself, that they might declare my praise.
en_uk_webbe~ISA~C43~22~Yet you have not called on me, Jacob; but you have been weary of me, Israel.
en_uk_webbe~ISA~C43~23~You have not brought me any of your sheep for burnt offerings, neither have you honoured me with your sacrifices. I have not burdened you with offerings, nor wearied you with frankincense.
en_uk_webbe~ISA~C43~24~You have bought me no sweet cane with money, nor have you filled me with the fat of your sacrifices, but you have burdened me with your sins. You have wearied me with your iniquities.
en_uk_webbe~ISA~C43~25~I, even I, am he who blots out your transgressions for my own sake; and I will not remember your sins.
en_uk_webbe~ISA~C43~26~Put me in remembrance. Let us plead together. Declare your case, that you may be justified.
en_uk_webbe~ISA~C43~27~Your first father sinned, and your teachers have transgressed against me.
en_uk_webbe~ISA~C43~28~Therefore I will profane the princes of the sanctuary; and I will make Jacob a curse, and Israel an insult.”
