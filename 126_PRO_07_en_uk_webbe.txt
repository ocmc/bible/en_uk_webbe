en_uk_webbe~PRO~C07~01~My son, keep my words. Lay up my commandments within you.
en_uk_webbe~PRO~C07~02~Keep my commandments and live! Guard my teaching as the apple of your eye.
en_uk_webbe~PRO~C07~03~Bind them on your fingers. Write them on the tablet of your heart.
en_uk_webbe~PRO~C07~04~Tell wisdom, “You are my sister.” Call understanding your relative,
en_uk_webbe~PRO~C07~05~that they may keep you from the strange woman, from the foreigner who flatters with her words.
en_uk_webbe~PRO~C07~06~For at the window of my house, I looked out through my lattice.
en_uk_webbe~PRO~C07~07~I saw amongst the simple ones. I discerned amongst the youths a young man void of understanding,
en_uk_webbe~PRO~C07~08~passing through the street near her corner, he went the way to her house,
en_uk_webbe~PRO~C07~09~in the twilight, in the evening of the day, in the middle of the night and in the darkness.
en_uk_webbe~PRO~C07~10~Behold, there a woman met him with the attire of a prostitute, and with crafty intent.
en_uk_webbe~PRO~C07~11~She is loud and defiant. Her feet don’t stay in her house.
en_uk_webbe~PRO~C07~12~Now she is in the streets, now in the squares, and lurking at every corner.
en_uk_webbe~PRO~C07~13~So she caught him, and kissed him. With an impudent face she said to him:
en_uk_webbe~PRO~C07~14~“Sacrifices of peace offerings are with me. Today I have paid my vows.
en_uk_webbe~PRO~C07~15~Therefore I came out to meet you, to diligently seek your face, and I have found you.
en_uk_webbe~PRO~C07~16~I have spread my couch with carpets of tapestry, with striped cloths of the yarn of Egypt.
en_uk_webbe~PRO~C07~17~I have perfumed my bed with myrrh, aloes, and cinnamon.
en_uk_webbe~PRO~C07~18~Come, let’s take our fill of loving until the morning. Let’s solace ourselves with loving.
en_uk_webbe~PRO~C07~19~For my husband isn’t at home. He has gone on a long journey.
en_uk_webbe~PRO~C07~20~He has taken a bag of money with him. He will come home at the full moon.”
en_uk_webbe~PRO~C07~21~With persuasive words, she led him astray. With the flattering of her lips, she seduced him.
en_uk_webbe~PRO~C07~22~He followed her immediately, as an ox goes to the slaughter, as a fool stepping into a noose.
en_uk_webbe~PRO~C07~23~Until an arrow strikes through his liver, as a bird hurries to the snare, and doesn’t know that it will cost his life.
en_uk_webbe~PRO~C07~24~Now therefore, sons, listen to me. Pay attention to the words of my mouth.
en_uk_webbe~PRO~C07~25~Don’t let your heart turn to her ways. Don’t go astray in her paths,
en_uk_webbe~PRO~C07~26~for she has thrown down many wounded. Yes, all her slain are a mighty army.
en_uk_webbe~PRO~C07~27~Her house is the way to Sheol, going down to the rooms of death.
