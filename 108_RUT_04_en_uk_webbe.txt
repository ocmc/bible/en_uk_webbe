en_uk_webbe~RUT~C04~01~Now Boaz went up to the gate and sat down there. Behold, the near kinsman of whom Boaz spoke came by. Boaz said to him, “Come over here, friend, and sit down!” He came over, and sat down.
en_uk_webbe~RUT~C04~02~Boaz took ten men of the elders of the city, and said, “Sit down here,” and they sat down.
en_uk_webbe~RUT~C04~03~He said to the near kinsman, “Naomi, who has come back out of the country of Moab, is selling the parcel of land, which was our brother Elimelech’s.
en_uk_webbe~RUT~C04~04~I thought I should tell you, saying, ‘Buy it before those who sit here, and before the elders of my people.’ If you will redeem it, redeem it; but if you will not redeem it, then tell me, that I may know. For there is no one to redeem it besides you; and I am after you.” He said, “I will redeem it.”
en_uk_webbe~RUT~C04~05~Then Boaz said, “On the day you buy the field from the hand of Naomi, you must buy it also from Ruth the Moabitess, the wife of the dead, to raise up the name of the dead on his inheritance.”
en_uk_webbe~RUT~C04~06~The near kinsman said, “I can’t redeem it for myself, lest I endanger my own inheritance. Take my right of redemption for yourself; for I can’t redeem it.”
en_uk_webbe~RUT~C04~07~Now this was the custom in former time in Israel concerning redeeming and concerning exchanging, to confirm all things: a man took off his sandal, and gave it to his neighbour; and this was the way of formalizing transactions in Israel.
en_uk_webbe~RUT~C04~08~So the near kinsman said to Boaz, “Buy it for yourself,” then he took off his sandal.
en_uk_webbe~RUT~C04~09~Boaz said to the elders and to all the people, “You are witnesses today, that I have bought all that was Elimelech’s, and all that was Chilion’s and Mahlon’s, from the hand of Naomi.
en_uk_webbe~RUT~C04~10~Moreover, Ruth the Moabitess, the wife of Mahlon, I have purchased to be my wife, to raise up the name of the dead on his inheritance, that the name of the dead may not be cut off from amongst his brothers and from the gate of his place. You are witnesses today.”
en_uk_webbe~RUT~C04~11~All the people who were in the gate, and the elders, said, “We are witnesses. May the LORD make the woman who has come into your house like Rachel and like Leah, which both built the house of Israel; and treat you worthily in Ephrathah, and be famous in Bethlehem.
en_uk_webbe~RUT~C04~12~Let your house be like the house of Perez, whom Tamar bore to Judah, of the offspring which the LORD will give you by this young woman.”
en_uk_webbe~RUT~C04~13~So Boaz took Ruth and she became his wife; and he went in to her, and the LORD enabled her to conceive, and she bore a son.
en_uk_webbe~RUT~C04~14~The women said to Naomi, “Blessed be the LORD, who has not left you today without a near kinsman. Let his name be famous in Israel.
en_uk_webbe~RUT~C04~15~He shall be to you a restorer of life and sustain you in your old age; for your daughter-in-law, who loves you, who is better to you than seven sons, has given birth to him.”
en_uk_webbe~RUT~C04~16~Naomi took the child, laid him in her bosom, and became nurse to him.
en_uk_webbe~RUT~C04~17~The women, her neighbours, gave him a name, saying, “A son is born to Naomi”. They named him Obed. He is the father of Jesse, the father of David.
en_uk_webbe~RUT~C04~18~Now this is the history of the generations of Perez: Perez became the father of Hezron,
en_uk_webbe~RUT~C04~19~and Hezron became the father of Ram, and Ram became the father of Amminadab,
en_uk_webbe~RUT~C04~20~and Amminadab became the father of Nahshon, and Nahshon became the father of Salmon,
en_uk_webbe~RUT~C04~21~and Salmon became the father of Boaz, and Boaz became the father of Obed,
en_uk_webbe~RUT~C04~22~and Obed became the father of Jesse, and Jesse became the father of David.
