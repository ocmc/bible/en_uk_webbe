en_uk_webbe~SIR~C15~01~He that fears the Lord will do this; And he that has possession of the law will obtain her.
en_uk_webbe~SIR~C15~02~And as a mother shall she meet him, And receive him as a wife married in her virginity.
en_uk_webbe~SIR~C15~03~She will feed him with bread of understanding, And give him water of wisdom to drink.
en_uk_webbe~SIR~C15~04~He will be stayed upon her, and will not be moved; And will rely upon her, and will not be confounded.
en_uk_webbe~SIR~C15~05~And she will exalt him above his neighbours; And she will open his mouth in the midst of the congregation.
en_uk_webbe~SIR~C15~06~He will inherit joy, and a crown of gladness, And an everlasting name.
en_uk_webbe~SIR~C15~07~Foolish men will not obtain her; And sinners will not see her.
en_uk_webbe~SIR~C15~08~She is far from pride; And liars will not remember her.
en_uk_webbe~SIR~C15~09~Praise is not comely in the mouth of a sinner; For it was not sent him from the Lord.
en_uk_webbe~SIR~C15~10~For praise will be spoken in wisdom; And the Lord will prosper it.
en_uk_webbe~SIR~C15~11~Don’t say you, It is through the Lord that I fell away; For you shall not do the things that he hates.
en_uk_webbe~SIR~C15~12~Don’t say you, It is he that caused me to err; For he has no need of a sinful man.
en_uk_webbe~SIR~C15~13~The Lord hates every abomination; And those who fear him love it not.
en_uk_webbe~SIR~C15~14~He himself made man from the beginning, And left him in the hand of his own counsel.
en_uk_webbe~SIR~C15~15~If you will, you shall keep the commandments; And to perform faithfulness is of [your own ] good pleasure.
en_uk_webbe~SIR~C15~16~He has set fire and water before you: You will stretch forth your hand to whichever you will.
en_uk_webbe~SIR~C15~17~Before man is life and death; And whichever he likes, it shall be given him.
en_uk_webbe~SIR~C15~18~For great is the wisdom of the Lord: He is mighty in power, and sees all things;
en_uk_webbe~SIR~C15~19~And his eyes are upon those who fear him; And he will take knowledge of every work of man.
en_uk_webbe~SIR~C15~20~He has not commanded any man to be ungodly; And he has not given any man licence to sin.
