en_uk_webbe~JDT~C16~01~And Judith began to sing this song of thanksgiving in all Israel, and all the people sang with loud voices this song of praise.
en_uk_webbe~JDT~C16~02~Judith said, “Begin a song to my God with timbrels. Sing to my Lord with cymbals. Make melody to him psalm and praise. Exalt him, and call upon his name.
en_uk_webbe~JDT~C16~03~For the Lord is the God that crushes battles. For in his armies in the midst of the people, he delivered me out of the hand of those who persecuted me.
en_uk_webbe~JDT~C16~04~Asshur came out of the mountains from the north. He came with ten thousands of his army. Its multitude stopped the torrents. Their horsemen covered the hills.
en_uk_webbe~JDT~C16~05~He said that he would burn up my borders, kill my young men with the sword, throw my nursing children to the ground, give my infants up as prey, and make my virgins a plunder.
en_uk_webbe~JDT~C16~06~“The Almighty Lord brought them to nothing by the hand of a woman.
en_uk_webbe~JDT~C16~07~For their mighty one didn’t fall by young men, neither did sons of the Titans strike him. Tall giants didn’t attack him, but Judith the daughter of Merari made him weak with the beauty of her countenance.
en_uk_webbe~JDT~C16~08~“For she put off the apparel of her widowhood for the exaltation of those who were distressed in Israel. She anointed her face with ointment, bound her hair in a tiara, and took a linen garment to deceive him.
en_uk_webbe~JDT~C16~09~Her sandal ravished his eye. Her beauty took his soul prisoner. The scimitar passed through his neck.
en_uk_webbe~JDT~C16~10~“The Persians quaked at her daring. The Medes were daunted at her boldness.
en_uk_webbe~JDT~C16~11~“Then my lowly ones shouted aloud. My weak ones were terrified and trembled for fear. They lifted up their voice, and they fled.
en_uk_webbe~JDT~C16~12~The sons of ladies pierced them through, and wounded them as fugitives’ children. They perished by the battle of my Lord.
en_uk_webbe~JDT~C16~13~“I will sing to my God a new song: O Lord, you are great and glorious, marvellous in strength, invincible.
en_uk_webbe~JDT~C16~14~Let all your creation serve you; for you spoke, and they were made. You sent out your spirit, and it built them. There is no one who can resist your voice.
en_uk_webbe~JDT~C16~15~For the mountains will be moved from their foundations with the waters, and the rocks will melt as wax at your presence: But you are yet merciful to those who fear you.
en_uk_webbe~JDT~C16~16~For all sacrifice is little for a sweet savour, And all the fat is very little for a whole burnt offering to you; But he who fears the Lord is great continually.
en_uk_webbe~JDT~C16~17~“Woe to the nations who rise up against my race! The Lord Almighty will take vengeance on them in the day of judgement, to put fire and worms in their flesh; and they will weep and feel their pain forever.”
en_uk_webbe~JDT~C16~18~Now when they came to Jerusalem, they worshipped God. When the people were purified, they offered their whole burnt offerings, their free will offerings, and their gifts.
en_uk_webbe~JDT~C16~19~Judith dedicated all Holofernes’ stuff, which the people had given her, and gave the canopy, which she had taken for herself out of his bedchamber, for a gift to the Lord.
en_uk_webbe~JDT~C16~20~And the people continued feasting in Jerusalem before the sanctuary for three months, and Judith remained with them.
en_uk_webbe~JDT~C16~21~But after these days, everyone departed to his own inheritance. Judith went away to Bethulia, and remained in her own possession, and was honourable in her time in all the land.
en_uk_webbe~JDT~C16~22~Many desired her, and no man knew her all the days of her life, from the day that Manasses her husband died and was gathered to his people.
en_uk_webbe~JDT~C16~23~She increased in greatness exceedingly; and she grew old in her husband’s house, to one hundred and five years, and let her maid go free. Then she died in Bethulia. They buried her in the cave of her husband Manasses.
en_uk_webbe~JDT~C16~24~The house of Israel mourned for her seven days. She distributed her goods before she died to all those who were nearest of kin to Manasses her husband, and to those who were nearest of her own kindred.
en_uk_webbe~JDT~C16~25~There was no one that made the children of Israel afraid any more in the days of Judith, nor a long time after her death.
