en_uk_webbe~REV~C05~01~I saw, in the right hand of him who sat on the throne, a book written inside and outside, sealed shut with seven seals.
en_uk_webbe~REV~C05~02~I saw a mighty angel proclaiming with a loud voice, “Who is worthy to open the book, and to break its seals?”
en_uk_webbe~REV~C05~03~No one in heaven above, or on the earth, or under the earth, was able to open the book or to look in it.
en_uk_webbe~REV~C05~04~Then I wept much, because no one was found worthy to open the book or to look in it.
en_uk_webbe~REV~C05~05~One of the elders said to me, “Don’t weep. Behold, the Lion who is of the tribe of Judah, the Root of David, has overcome: he who opens the book and its seven seals.”
en_uk_webbe~REV~C05~06~I saw in the middle of the throne and of the four living creatures, and in the middle of the elders, a Lamb standing, as though it had been slain, having seven horns and seven eyes, which are the seven Spirits of God, sent out into all the earth.
en_uk_webbe~REV~C05~07~Then he came, and he took it out of the right hand of him who sat on the throne.
en_uk_webbe~REV~C05~08~Now when he had taken the book, the four living creatures and the twenty-four elders fell down before the Lamb, each one having a harp, and golden bowls full of incense, which are the prayers of the saints.
en_uk_webbe~REV~C05~09~They sang a new song, saying, “You are worthy to take the book and to open its seals: for you were killed, and bought us for God with your blood out of every tribe, language, people, and nation,
en_uk_webbe~REV~C05~10~and made us kings and priests to our God, and we will reign on the earth.”
en_uk_webbe~REV~C05~11~I saw, and I heard something like a voice of many angels around the throne, the living creatures, and the elders. The number of them was ten thousands of ten thousands, and thousands of thousands;
en_uk_webbe~REV~C05~12~saying with a loud voice, “Worthy is the Lamb who has been killed to receive the power, wealth, wisdom, strength, honour, glory, and blessing!”
en_uk_webbe~REV~C05~13~I heard every created thing which is in heaven, on the earth, under the earth, on the sea, and everything in them, saying, “To him who sits on the throne, and to the Lamb be the blessing, the honour, the glory, and the dominion, forever and ever! Amen!”
en_uk_webbe~REV~C05~14~The four living creatures said, “Amen!” Then the elders fell down and worshipped.
