en_uk_webbe~ACT~C06~01~Now in those days, when the number of the disciples was multiplying, a complaint arose from the Hellenists against the Hebrews, because their widows were neglected in the daily service.
en_uk_webbe~ACT~C06~02~The twelve summoned the multitude of the disciples and said, “It is not appropriate for us to forsake the word of God and serve tables.
en_uk_webbe~ACT~C06~03~Therefore select from amongst you, brothers, seven men of good report, full of the Holy Spirit and of wisdom, whom we may appoint over this business.
en_uk_webbe~ACT~C06~04~But we will continue steadfastly in prayer and in the ministry of the word.”
en_uk_webbe~ACT~C06~05~These words pleased the whole multitude. They chose Stephen, a man full of faith and of the Holy Spirit, Philip, Prochorus, Nicanor, Timon, Parmenas, and Nicolaus, a proselyte of Antioch;
en_uk_webbe~ACT~C06~06~whom they set before the apostles. When they had prayed, they laid their hands on them.
en_uk_webbe~ACT~C06~07~The word of God increased and the number of the disciples greatly multiplied in Jerusalem. A great company of the priests were obedient to the faith.
en_uk_webbe~ACT~C06~08~Stephen, full of faith and power, performed great wonders and signs amongst the people.
en_uk_webbe~ACT~C06~09~But some of those who were of the synagogue called “The Libertines”, and of the Cyrenians, of the Alexandrians, and of those of Cilicia and Asia arose, disputing with Stephen.
en_uk_webbe~ACT~C06~10~They weren’t able to withstand the wisdom and the Spirit by which he spoke.
en_uk_webbe~ACT~C06~11~Then they secretly induced men to say, “We have heard him speak blasphemous words against Moses and God.”
en_uk_webbe~ACT~C06~12~They stirred up the people, the elders, and the scribes, and came against him and seized him, then brought him in to the council,
en_uk_webbe~ACT~C06~13~and set up false witnesses who said, “This man never stops speaking blasphemous words against this holy place and the law.
en_uk_webbe~ACT~C06~14~For we have heard him say that this Jesus of Nazareth will destroy this place, and will change the customs which Moses delivered to us.”
en_uk_webbe~ACT~C06~15~All who sat in the council, fastening their eyes on him, saw his face like it was the face of an angel.
