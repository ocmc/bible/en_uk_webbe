en_uk_webbe~ACT~C15~01~Some men came down from Judea and taught the brothers, “Unless you are circumcised after the custom of Moses, you can’t be saved.”
en_uk_webbe~ACT~C15~02~Therefore when Paul and Barnabas had no small discord and discussion with them, they appointed Paul and Barnabas, and some others of them, to go up to Jerusalem to the apostles and elders about this question.
en_uk_webbe~ACT~C15~03~They, being sent on their way by the assembly, passed through both Phoenicia and Samaria, declaring the conversion of the Gentiles. They caused great joy to all the brothers.
en_uk_webbe~ACT~C15~04~When they had come to Jerusalem, they were received by the assembly and the apostles and the elders, and they reported everything that God had done with them.
en_uk_webbe~ACT~C15~05~But some of the sect of the Pharisees who believed rose up, saying, “It is necessary to circumcise them, and to command them to keep the law of Moses.”
en_uk_webbe~ACT~C15~06~The apostles and the elders were gathered together to see about this matter.
en_uk_webbe~ACT~C15~07~When there had been much discussion, Peter rose up and said to them, “Brothers, you know that a good while ago God made a choice amongst you that by my mouth the nations should hear the word of the Good News and believe.
en_uk_webbe~ACT~C15~08~God, who knows the heart, testified about them, giving them the Holy Spirit, just like he did to us.
en_uk_webbe~ACT~C15~09~He made no distinction between us and them, cleansing their hearts by faith.
en_uk_webbe~ACT~C15~10~Now therefore why do you tempt God, that you should put a yoke on the neck of the disciples which neither our fathers nor we were able to bear?
en_uk_webbe~ACT~C15~11~But we believe that we are saved through the grace of the Lord Jesus, just as they are.”
en_uk_webbe~ACT~C15~12~All the multitude kept silence, and they listened to Barnabas and Paul reporting what signs and wonders God had done amongst the nations through them.
en_uk_webbe~ACT~C15~13~After they were silent, James answered, “Brothers, listen to me.
en_uk_webbe~ACT~C15~14~Simeon has reported how God first visited the nations to take out of them a people for his name.
en_uk_webbe~ACT~C15~15~This agrees with the words of the prophets. As it is written,
en_uk_webbe~ACT~C15~16~‘After these things I will return. I will again build the tabernacle of David, which has fallen. I will again build its ruins. I will set it up
en_uk_webbe~ACT~C15~17~that the rest of men may seek after the Lord; all the Gentiles who are called by my name, says the Lord, who does all these things.’
en_uk_webbe~ACT~C15~18~“All of God’s works are known to him from eternity.
en_uk_webbe~ACT~C15~19~Therefore my judgement is that we don’t trouble those from amongst the Gentiles who turn to God,
en_uk_webbe~ACT~C15~20~but that we write to them that they abstain from the pollution of idols, from sexual immorality, from what is strangled, and from blood.
en_uk_webbe~ACT~C15~21~For Moses from generations of old has in every city those who preach him, being read in the synagogues every Sabbath.”
en_uk_webbe~ACT~C15~22~Then it seemed good to the apostles and the elders, with the whole assembly, to choose men out of their company, and send them to Antioch with Paul and Barnabas: Judas called Barsabbas, and Silas, chief men amongst the brothers.
en_uk_webbe~ACT~C15~23~They wrote these things by their hand: “The apostles, the elders, and the brothers, to the brothers who are of the Gentiles in Antioch, Syria, and Cilicia: greetings.
en_uk_webbe~ACT~C15~24~Because we have heard that some who went out from us have troubled you with words, unsettling your souls, saying, ‘You must be circumcised and keep the law,’ to whom we gave no commandment;
en_uk_webbe~ACT~C15~25~it seemed good to us, having come to one accord, to choose out men and send them to you with our beloved Barnabas and Paul,
en_uk_webbe~ACT~C15~26~men who have risked their lives for the name of our Lord Jesus Christ.
en_uk_webbe~ACT~C15~27~We have sent therefore Judas and Silas, who themselves will also tell you the same things by word of mouth.
en_uk_webbe~ACT~C15~28~For it seemed good to the Holy Spirit, and to us, to lay no greater burden on you than these necessary things:
en_uk_webbe~ACT~C15~29~that you abstain from things sacrificed to idols, from blood, from things strangled, and from sexual immorality, from which if you keep yourselves, it will be well with you. Farewell.”
en_uk_webbe~ACT~C15~30~So, when they were sent off, they came to Antioch. Having gathered the multitude together, they delivered the letter.
en_uk_webbe~ACT~C15~31~When they had read it, they rejoiced over the encouragement.
en_uk_webbe~ACT~C15~32~Judas and Silas, also being prophets themselves, encouraged the brothers with many words and strengthened them.
en_uk_webbe~ACT~C15~33~After they had spent some time there, they were sent back with greetings from the brothers to the apostles.
en_uk_webbe~ACT~C15~34~(*) But it seemed good to Silas to stay there.
en_uk_webbe~ACT~C15~35~But Paul and Barnabas stayed in Antioch, teaching and preaching the word of the Lord, with many others also.
en_uk_webbe~ACT~C15~36~After some days Paul said to Barnabas, “Let’s return now and visit our brothers in every city in which we proclaimed the word of the Lord, to see how they are doing.”
en_uk_webbe~ACT~C15~37~Barnabas planned to take John, who was called Mark, with them also.
en_uk_webbe~ACT~C15~38~But Paul didn’t think that it was a good idea to take with them someone who had withdrawn from them in Pamphylia, and didn’t go with them to do the work.
en_uk_webbe~ACT~C15~39~Then the contention grew so sharp that they separated from each other. Barnabas took Mark with him and sailed away to Cyprus,
en_uk_webbe~ACT~C15~40~but Paul chose Silas and went out, being commended by the brothers to the grace of God.
en_uk_webbe~ACT~C15~41~He went through Syria and Cilicia, strengthening the assemblies.
