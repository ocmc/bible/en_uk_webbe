en_uk_webbe~WIS~C17~01~For your judgements are great, and hard to interpret; therefore undisciplined souls went astray.
en_uk_webbe~WIS~C17~02~For when lawless men had supposed that they held a holy nation in their power, they, prisoners of darkness, and bound in the fetters of a long night, kept close beneath their roofs, lay exiled from the eternal providence.
en_uk_webbe~WIS~C17~03~For while they thought that they were unseen in [their ] secret sins, they were divided from one another by a dark curtain of forgetfulness, stricken with terrible awe, and very troubled by apparitions.
en_uk_webbe~WIS~C17~04~For neither did the dark recesses that held them guard them from fears, But terrifying sounds rang around them, and dismal phantoms appeared with unsmiling faces.
en_uk_webbe~WIS~C17~05~And no force of fire prevailed to give light, neither were the brightest flames of the stars strong enough to illuminate that gloomy night;
en_uk_webbe~WIS~C17~06~but only the glimmering of a self-kindled fire appeared to them, full of fear. In terror, they considered the things which they saw to be worse than that sight, on which they could not gaze.
en_uk_webbe~WIS~C17~07~The mockeries of their magic arts were powerless, now, and a shameful rebuke of their boasted understanding:
en_uk_webbe~WIS~C17~08~For those who promised to drive away terrors and disorders from a sick soul, these were sick with a ludicrous fearfulness.
en_uk_webbe~WIS~C17~09~For even if no troubling thing frighted them, yet, scared with the creeping of vermin and hissing of serpents,
en_uk_webbe~WIS~C17~10~they perished trembling in fear, refusing even to look at the air, which could not be escaped on any side.
en_uk_webbe~WIS~C17~11~For wickedness, condemned by a witness within, is a coward thing, and, being pressed hard by conscience, always has added forecasts of the worst.
en_uk_webbe~WIS~C17~12~For fear is nothing else but a surrender of the help which reason offers;
en_uk_webbe~WIS~C17~13~and from within, the expectation of being less makes of greater account the ignorance of the cause that brings the torment.
en_uk_webbe~WIS~C17~14~But they, all through the night which was powerless indeed, and which came upon them out of the recesses of powerless Hades, sleeping the same sleep,
en_uk_webbe~WIS~C17~15~now were haunted by monstrous apparitions, and now were paralysed by their soul’s surrendering; for sudden and unexpected fear came upon them.
en_uk_webbe~WIS~C17~16~So then whoever it might be, sinking down in his place, was kept captive, shut up in that prison which was not barred with iron;
en_uk_webbe~WIS~C17~17~for whether he was a farmer, or a shepherd, or a labourer whose toils were in the wilderness, he was overtaken, and endured that inevitable necessity; for they were all bound with one chain of darkness.
en_uk_webbe~WIS~C17~18~Whether there was a whistling wind, or a melodious sound of birds amongst the spreading branches, or a measured fall of water running violently,
en_uk_webbe~WIS~C17~19~or a harsh crashing of rocks hurled down, or the swift course of animals bounding along unseen, or the voice of wild beasts harshly roaring, or an echo rebounding from the hollows of the mountains, all these things paralysed them with terror.
en_uk_webbe~WIS~C17~20~For the whole world was illuminated with clear light, and was occupied with unhindered works,
en_uk_webbe~WIS~C17~21~while over them alone was spread a heavy night, an image of the darkness that should afterward receive them; but to themselves, they were heavier than darkness.
