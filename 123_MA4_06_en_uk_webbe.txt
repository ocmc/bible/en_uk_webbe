en_uk_webbe~MA4~C06~01~When Eleazar had in this manner answered the exhortations of the tyrant, the spearbearers came up, and rudely haled Eleazar to the instruments of torture.
en_uk_webbe~MA4~C06~02~And first, they stripped the old man, adorned as he was with the beauty of piety.
en_uk_webbe~MA4~C06~03~Then tying back his arms and hands, they disdainfully used him with stripes;
en_uk_webbe~MA4~C06~04~a herald opposite crying out, Obey the commands of the king.
en_uk_webbe~MA4~C06~05~But Eleazar, the high-minded and truly noble, as one tortured in a dream, regarded it not all.
en_uk_webbe~MA4~C06~06~But raising his eyes on high to heaven, the old man’s flesh was stripped off by the scourges, and his blood streamed down, and his sides were pierced through.
en_uk_webbe~MA4~C06~07~And falling upon the ground, from his body having no power to support the pains, he yet kept his reasoning upright and unbending.
en_uk_webbe~MA4~C06~08~then one of the harsh spearbearers leapt upon his belly as he was falling, to force him upright.
en_uk_webbe~MA4~C06~09~But he endured the pains, and despised the cruelty, and persevered through the indignities;
en_uk_webbe~MA4~C06~10~and like a noble athlete, the old man, when struck, vanquished his torturers.
en_uk_webbe~MA4~C06~11~His countenance sweating, and he panting for breath, he was admired by the very torturers for his courage.
en_uk_webbe~MA4~C06~12~Wherefore, partly in pity for his old age,
en_uk_webbe~MA4~C06~13~partly from the sympathy of acquaintance, and partly in admiration of his endurance, some of the attendants of the king said,
en_uk_webbe~MA4~C06~14~Why do you unreasonably destroy yourself, O Eleazar, with these miseries?
en_uk_webbe~MA4~C06~15~We will bring you some meat cooked by yourself, and do you save yourself by pretending that you have eaten swine’s flesh.
en_uk_webbe~MA4~C06~16~And Eleazar, as though the advice more painfully tortured him, cried out,
en_uk_webbe~MA4~C06~17~Let not us who are children of Abraham be so evil advised as by giving way to make use of an unbecoming pretence;
en_uk_webbe~MA4~C06~18~for it were irrational, if having lived up to old age in all truth, and having scrupulously guarded our character for it, we should now turn back,
en_uk_webbe~MA4~C06~19~and ourselves should become a pattern of impiety to the young, as being an example of pollution eating.
en_uk_webbe~MA4~C06~20~It would be disgraceful if we should live on some short time, and that scorned by all men for cowardice,
en_uk_webbe~MA4~C06~21~and be condemned by the tyrant for unmanliness, by not contending to the death for our divine law.
en_uk_webbe~MA4~C06~22~Wherefore do you, O children of Abraham, die nobly for your religion.
en_uk_webbe~MA4~C06~23~You° spearbearers of the tyrant, why do you° linger?
en_uk_webbe~MA4~C06~24~Beholding him so high-minded against misery, and not changing at their pity, they led him to the fire:
en_uk_webbe~MA4~C06~25~then with their wickedly contrived instruments they burnt him on the fire, and poured stinking fluids down into his nostrils.
en_uk_webbe~MA4~C06~26~And he being at length burnt down to the bones, and about to expire, raised his eyes Godward, and said,
en_uk_webbe~MA4~C06~27~You know, O God, that when I might have been saved, I am slain for the sake of the law by tortures of fire.
en_uk_webbe~MA4~C06~28~Be merciful to your people, and be satisfied with the punishment of me on their account.
en_uk_webbe~MA4~C06~29~Let my blood be a purification for them, and take my life in recompense for theirs.
en_uk_webbe~MA4~C06~30~Thus speaking, the holy man departed, noble in his torments, and even to the agonies of death resisted in his reasoning for the sake of the law.
en_uk_webbe~MA4~C06~31~Confessedly, therefore, religious reasoning is master of the passions.
en_uk_webbe~MA4~C06~32~For had the passions been superior to reasoning, I would have given them the witness of this mastery.
en_uk_webbe~MA4~C06~33~But now, since reasoning conquered the passions, we befittingly awared it the authority of first place.
en_uk_webbe~MA4~C06~34~And it is but fair that we should allow, that the power belongs to reasoning, since it masters external miseries.
en_uk_webbe~MA4~C06~35~Ridiculous would it be were it not so; and I prove that reasoning has not only mastered pains, but that it is also superior to the pleasures, and withstands them.
