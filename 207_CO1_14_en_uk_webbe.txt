en_uk_webbe~CO1~C14~01~Follow after love and earnestly desire spiritual gifts, but especially that you may prophesy.
en_uk_webbe~CO1~C14~02~For he who speaks in another language speaks not to men, but to God; for no one understands; but in the Spirit he speaks mysteries.
en_uk_webbe~CO1~C14~03~But he who prophesies speaks to men for their edification, exhortation, and consolation.
en_uk_webbe~CO1~C14~04~He who speaks in another language edifies himself, but he who prophesies edifies the assembly.
en_uk_webbe~CO1~C14~05~Now I desire to have you all speak with other languages, but rather that you would prophesy. For he is greater who prophesies than he who speaks with other languages, unless he interprets, that the assembly may be built up.
en_uk_webbe~CO1~C14~06~But now, brothers, if I come to you speaking with other languages, what would I profit you, unless I speak to you either by way of revelation, or of knowledge, or of prophesying, or of teaching?
en_uk_webbe~CO1~C14~07~Even things without life, giving a voice, whether pipe or harp, if they didn’t give a distinction in the sounds, how would it be known what is piped or harped?
en_uk_webbe~CO1~C14~08~For if the trumpet gave an uncertain sound, who would prepare himself for war?
en_uk_webbe~CO1~C14~09~So also you, unless you uttered by the tongue words easy to understand, how would it be known what is spoken? For you would be speaking into the air.
en_uk_webbe~CO1~C14~10~There are, it may be, so many kinds of sounds in the world, and none of them is without meaning.
en_uk_webbe~CO1~C14~11~If then I don’t know the meaning of the sound, I would be to him who speaks a foreigner, and he who speaks would be a foreigner to me.
en_uk_webbe~CO1~C14~12~So also you, since you are zealous for spiritual gifts, seek that you may abound to the building up of the assembly.
en_uk_webbe~CO1~C14~13~Therefore let him who speaks in another language pray that he may interpret.
en_uk_webbe~CO1~C14~14~For if I pray in another language, my spirit prays, but my understanding is unfruitful.
en_uk_webbe~CO1~C14~15~What is it then? I will pray with the spirit, and I will pray with the understanding also. I will sing with the spirit, and I will sing with the understanding also.
en_uk_webbe~CO1~C14~16~Otherwise if you bless with the spirit, how will he who fills the place of the unlearned say the “Amen” at your giving of thanks, seeing he doesn’t know what you say?
en_uk_webbe~CO1~C14~17~For you most certainly give thanks well, but the other person is not built up.
en_uk_webbe~CO1~C14~18~I thank my God, I speak with other languages more than you all.
en_uk_webbe~CO1~C14~19~However in the assembly I would rather speak five words with my understanding, that I might instruct others also, than ten thousand words in another language.
en_uk_webbe~CO1~C14~20~Brothers, don’t be children in thoughts, yet in malice be babies, but in thoughts be mature.
en_uk_webbe~CO1~C14~21~In the law it is written, “By men of strange languages and by the lips of strangers I will speak to this people. They won’t even hear me that way, says the Lord.”
en_uk_webbe~CO1~C14~22~Therefore other languages are for a sign, not to those who believe, but to the unbelieving; but prophesying is for a sign, not to the unbelieving, but to those who believe.
en_uk_webbe~CO1~C14~23~If therefore the whole assembly is assembled together and all speak with other languages, and unlearned or unbelieving people come in, won’t they say that you are crazy?
en_uk_webbe~CO1~C14~24~But if all prophesy, and someone unbelieving or unlearned comes in, he is reproved by all, and he is judged by all.
en_uk_webbe~CO1~C14~25~And thus the secrets of his heart are revealed. So he will fall down on his face and worship God, declaring that God is amongst you indeed.
en_uk_webbe~CO1~C14~26~What is it then, brothers? When you come together, each one of you has a psalm, has a teaching, has a revelation, has another language, or has an interpretation. Let all things be done to build each other up.
en_uk_webbe~CO1~C14~27~If any man speaks in another language, let it be two, or at the most three, and in turn; and let one interpret.
en_uk_webbe~CO1~C14~28~But if there is no interpreter, let him keep silent in the assembly, and let him speak to himself, and to God.
en_uk_webbe~CO1~C14~29~Let the prophets speak, two or three, and let the others discern.
en_uk_webbe~CO1~C14~30~But if a revelation is made to another sitting by, let the first keep silent.
en_uk_webbe~CO1~C14~31~For you all can prophesy one by one, that all may learn, and all may be exhorted.
en_uk_webbe~CO1~C14~32~The spirits of the prophets are subject to the prophets,
en_uk_webbe~CO1~C14~33~for God is not a God of confusion, but of peace, as in all the assemblies of the saints.
en_uk_webbe~CO1~C14~34~Let the wives be quiet in the assemblies, for it has not been permitted for them to be talking except in submission, as the law also says,
en_uk_webbe~CO1~C14~35~if they desire to learn anything. “Let them ask their own husbands at home, for it is shameful for a wife to be talking in the assembly.”
en_uk_webbe~CO1~C14~36~What!? Was it from you that the word of God went out? Or did it come to you alone?
en_uk_webbe~CO1~C14~37~If any man thinks himself to be a prophet, or spiritual, let him recognise the things which I write to you, that they are the commandment of the Lord.
en_uk_webbe~CO1~C14~38~But if anyone is ignorant, let him be ignorant.
en_uk_webbe~CO1~C14~39~Therefore, brothers, desire earnestly to prophesy, and don’t forbid speaking with other languages.
en_uk_webbe~CO1~C14~40~Let all things be done decently and in order.
