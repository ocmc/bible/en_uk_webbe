en_uk_webbe~PRO~C17~01~Better is a dry morsel with quietness, than a house full of feasting with strife.
en_uk_webbe~PRO~C17~02~A servant who deals wisely will rule over a son who causes shame, and shall have a part in the inheritance amongst the brothers.
en_uk_webbe~PRO~C17~03~The refining pot is for silver, and the furnace for gold, but the LORD tests the hearts.
en_uk_webbe~PRO~C17~04~An evildoer heeds wicked lips. A liar gives ear to a mischievous tongue.
en_uk_webbe~PRO~C17~05~Whoever mocks the poor reproaches his Maker. He who is glad at calamity shall not be unpunished.
en_uk_webbe~PRO~C17~06~Children’s children are the crown of old men; the glory of children are their parents.
en_uk_webbe~PRO~C17~07~Arrogant speech isn’t fitting for a fool, much less do lying lips fit a prince.
en_uk_webbe~PRO~C17~08~A bribe is a precious stone in the eyes of him who gives it; wherever he turns, he prospers.
en_uk_webbe~PRO~C17~09~He who covers an offence promotes love; but he who repeats a matter separates best friends.
en_uk_webbe~PRO~C17~10~A rebuke enters deeper into one who has understanding than a hundred lashes into a fool.
en_uk_webbe~PRO~C17~11~An evil man seeks only rebellion; therefore a cruel messenger shall be sent against him.
en_uk_webbe~PRO~C17~12~Let a bear robbed of her cubs meet a man, rather than a fool in his folly.
en_uk_webbe~PRO~C17~13~Whoever rewards evil for good, evil shall not depart from his house.
en_uk_webbe~PRO~C17~14~The beginning of strife is like breaching a dam, therefore stop contention before quarrelling breaks out.
en_uk_webbe~PRO~C17~15~He who justifies the wicked, and he who condemns the righteous, both of them alike are an abomination to the LORD.
en_uk_webbe~PRO~C17~16~Why is there money in the hand of a fool to buy wisdom, since he has no understanding?
en_uk_webbe~PRO~C17~17~A friend loves at all times; and a brother is born for adversity.
en_uk_webbe~PRO~C17~18~A man void of understanding strikes hands, and becomes collateral in the presence of his neighbour.
en_uk_webbe~PRO~C17~19~He who loves disobedience loves strife. One who builds a high gate seeks destruction.
en_uk_webbe~PRO~C17~20~One who has a perverse heart doesn’t find prosperity, and one who has a deceitful tongue falls into trouble.
en_uk_webbe~PRO~C17~21~He who becomes the father of a fool grieves. The father of a fool has no joy.
en_uk_webbe~PRO~C17~22~A cheerful heart makes good medicine, but a crushed spirit dries up the bones.
en_uk_webbe~PRO~C17~23~A wicked man receives a bribe in secret, to pervert the ways of justice.
en_uk_webbe~PRO~C17~24~Wisdom is before the face of one who has understanding, but the eyes of a fool wander to the ends of the earth.
en_uk_webbe~PRO~C17~25~A foolish son brings grief to his father, and bitterness to her who bore him.
en_uk_webbe~PRO~C17~26~Also to punish the righteous is not good, nor to flog officials for their integrity.
en_uk_webbe~PRO~C17~27~He who spares his words has knowledge. He who is even tempered is a man of understanding.
en_uk_webbe~PRO~C17~28~Even a fool, when he keeps silent, is counted wise. When he shuts his lips, he is thought to be discerning.
