en_uk_webbe~MAT~C08~01~When he came down from the mountain, great multitudes followed him.
en_uk_webbe~MAT~C08~02~Behold, a leper came to him and worshipped him, saying, “Lord, if you want to, you can make me clean.”
en_uk_webbe~MAT~C08~03~Jesus stretched out his hand, and touched him, saying, “I want to. Be made clean.” Immediately his leprosy was cleansed.
en_uk_webbe~MAT~C08~04~Jesus said to him, “See that you tell nobody, but go, show yourself to the priest, and offer the gift that Moses commanded, as a testimony to them.”
en_uk_webbe~MAT~C08~05~When he came into Capernaum, a centurion came to him, asking him,
en_uk_webbe~MAT~C08~06~and saying, “Lord, my servant lies in the house paralysed, grievously tormented.”
en_uk_webbe~MAT~C08~07~Jesus said to him, “I will come and heal him.”
en_uk_webbe~MAT~C08~08~The centurion answered, “Lord, I’m not worthy for you to come under my roof. Just say the word, and my servant will be healed.
en_uk_webbe~MAT~C08~09~For I am also a man under authority, having under myself soldiers. I tell this one, ‘Go,’ and he goes; and tell another, ‘Come,’ and he comes; and tell my servant, ‘Do this,’ and he does it.”
en_uk_webbe~MAT~C08~10~When Jesus heard it, he marvelled, and said to those who followed, “Most certainly I tell you, I haven’t found so great a faith, not even in Israel.
en_uk_webbe~MAT~C08~11~I tell you that many will come from the east and the west, and will sit down with Abraham, Isaac, and Jacob in the Kingdom of Heaven,
en_uk_webbe~MAT~C08~12~but the children of the Kingdom will be thrown out into the outer darkness. There will be weeping and gnashing of teeth.”
en_uk_webbe~MAT~C08~13~Jesus said to the centurion, “Go your way. Let it be done for you as you have believed.” His servant was healed in that hour.
en_uk_webbe~MAT~C08~14~When Jesus came into Peter’s house, he saw his wife’s mother lying sick with a fever.
en_uk_webbe~MAT~C08~15~He touched her hand, and the fever left her. She got up and served him.
en_uk_webbe~MAT~C08~16~When evening came, they brought to him many possessed with demons. He cast out the spirits with a word, and healed all who were sick;
en_uk_webbe~MAT~C08~17~that it might be fulfilled which was spoken through Isaiah the prophet, saying, “He took our infirmities, and bore our diseases.”
en_uk_webbe~MAT~C08~18~Now when Jesus saw great multitudes around him, he gave the order to depart to the other side.
en_uk_webbe~MAT~C08~19~A scribe came, and said to him, “Teacher, I will follow you wherever you go.”
en_uk_webbe~MAT~C08~20~Jesus said to him, “The foxes have holes, and the birds of the sky have nests, but the Son of Man has nowhere to lay his head.”
en_uk_webbe~MAT~C08~21~Another of his disciples said to him, “Lord, allow me first to go and bury my father.”
en_uk_webbe~MAT~C08~22~But Jesus said to him, “Follow me, and leave the dead to bury their own dead.”
en_uk_webbe~MAT~C08~23~When he got into a boat, his disciples followed him.
en_uk_webbe~MAT~C08~24~Behold, a violent storm came up on the sea, so much that the boat was covered with the waves, but he was asleep.
en_uk_webbe~MAT~C08~25~They came to him, and woke him up, saying, “Save us, Lord! We are dying!”
en_uk_webbe~MAT~C08~26~He said to them, “Why are you fearful, O you of little faith?” Then he got up, rebuked the wind and the sea, and there was a great calm.
en_uk_webbe~MAT~C08~27~The men marvelled, saying, “What kind of man is this, that even the wind and the sea obey him?”
en_uk_webbe~MAT~C08~28~When he came to the other side, into the country of the Gergesenes, two people possessed by demons met him there, coming out of the tombs, exceedingly fierce, so that nobody could pass that way.
en_uk_webbe~MAT~C08~29~Behold, they cried out, saying, “What do we have to do with you, Jesus, Son of God? Have you come here to torment us before the time?”
en_uk_webbe~MAT~C08~30~Now there was a herd of many pigs feeding far away from them.
en_uk_webbe~MAT~C08~31~The demons begged him, saying, “If you cast us out, permit us to go away into the herd of pigs.”
en_uk_webbe~MAT~C08~32~He said to them, “Go!” They came out, and went into the herd of pigs: and behold, the whole herd of pigs rushed down the cliff into the sea, and died in the water.
en_uk_webbe~MAT~C08~33~Those who fed them fled, and went away into the city, and told everything, including what happened to those who were possessed with demons.
en_uk_webbe~MAT~C08~34~Behold, all the city came out to meet Jesus. When they saw him, they begged that he would depart from their borders.
