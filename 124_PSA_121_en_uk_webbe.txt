en_uk_webbe~PSA~C121~001~(C122:001) (0) A Song of Ascents. By David. (1) I was glad when they said to me, “Let’s go to the LORD’s house!”
en_uk_webbe~PSA~C121~002~(C122:002) Our feet are standing within your gates, Jerusalem,
en_uk_webbe~PSA~C121~003~(C122:003) Jerusalem, that is built as a city that is compact together,
en_uk_webbe~PSA~C121~004~(C122:004) where the tribes go up, even the LORD’s tribes, according to an ordinance for Israel, to give thanks to the LORD’s name.
en_uk_webbe~PSA~C121~005~(C122:005) For there are set thrones for judgement, the thrones of David’s house.
en_uk_webbe~PSA~C121~006~(C122:006) Pray for the peace of Jerusalem. Those who love you will prosper.
en_uk_webbe~PSA~C121~007~(C122:007) Peace be within your walls, and prosperity within your palaces.
en_uk_webbe~PSA~C121~008~(C122:008) For my brothers’ and companions’ sakes, I will now say, “Peace be within you.”
en_uk_webbe~PSA~C121~009~(C122:009) For the sake of the house of the LORD our God, I will seek your good.
