en_uk_webbe~LUK~C10~01~Now after these things, the Lord also appointed seventy others, and sent them two by two ahead of him into every city and place where he was about to come.
en_uk_webbe~LUK~C10~02~Then he said to them, “The harvest is indeed plentiful, but the labourers are few. Pray therefore to the Lord of the harvest, that he may send out labourers into his harvest.
en_uk_webbe~LUK~C10~03~Go your ways. Behold, I send you out as lambs amongst wolves.
en_uk_webbe~LUK~C10~04~Carry no purse, nor wallet, nor sandals. Greet no one on the way.
en_uk_webbe~LUK~C10~05~Into whatever house you enter, first say, ‘Peace be to this house.’
en_uk_webbe~LUK~C10~06~If a son of peace is there, your peace will rest on him; but if not, it will return to you.
en_uk_webbe~LUK~C10~07~Remain in that same house, eating and drinking the things they give, for the labourer is worthy of his wages. Don’t go from house to house.
en_uk_webbe~LUK~C10~08~Into whatever city you enter, and they receive you, eat the things that are set before you.
en_uk_webbe~LUK~C10~09~Heal the sick who are there, and tell them, ‘God’s Kingdom has come near to you.’
en_uk_webbe~LUK~C10~10~But into whatever city you enter, and they don’t receive you, go out into its streets and say,
en_uk_webbe~LUK~C10~11~‘Even the dust from your city that clings to us, we wipe off against you. Nevertheless know this, that God’s Kingdom has come near to you.’
en_uk_webbe~LUK~C10~12~I tell you, it will be more tolerable in that day for Sodom than for that city.
en_uk_webbe~LUK~C10~13~“Woe to you, Chorazin! Woe to you, Bethsaida! For if the mighty works had been done in Tyre and Sidon which were done in you, they would have repented long ago, sitting in sackcloth and ashes.
en_uk_webbe~LUK~C10~14~But it will be more tolerable for Tyre and Sidon in the judgement than for you.
en_uk_webbe~LUK~C10~15~You, Capernaum, who are exalted to heaven, will be brought down to Hades.
en_uk_webbe~LUK~C10~16~Whoever listens to you listens to me, and whoever rejects you rejects me. Whoever rejects me rejects him who sent me.”
en_uk_webbe~LUK~C10~17~The seventy returned with joy, saying, “Lord, even the demons are subject to us in your name!”
en_uk_webbe~LUK~C10~18~He said to them, “I saw Satan having fallen like lightning from heaven.
en_uk_webbe~LUK~C10~19~Behold, I give you authority to tread on serpents and scorpions, and over all the power of the enemy. Nothing will in any way hurt you.
en_uk_webbe~LUK~C10~20~Nevertheless, don’t rejoice in this, that the spirits are subject to you, but rejoice that your names are written in heaven.”
en_uk_webbe~LUK~C10~21~In that same hour Jesus rejoiced in the Holy Spirit, and said, “I thank you, O Father, Lord of heaven and earth, that you have hidden these things from the wise and understanding, and revealed them to little children. Yes, Father, for so it was well-pleasing in your sight.”
en_uk_webbe~LUK~C10~22~Turning to the disciples, he said, “All things have been delivered to me by my Father. No one knows who the Son is, except the Father, and who the Father is, except the Son, and he to whomever the Son desires to reveal him.”
en_uk_webbe~LUK~C10~23~Turning to the disciples, he said privately, “Blessed are the eyes which see the things that you see,
en_uk_webbe~LUK~C10~24~for I tell you that many prophets and kings desired to see the things which you see, and didn’t see them, and to hear the things which you hear, and didn’t hear them.”
en_uk_webbe~LUK~C10~25~Behold, a certain lawyer stood up and tested him, saying, “Teacher, what shall I do to inherit eternal life?”
en_uk_webbe~LUK~C10~26~He said to him, “What is written in the law? How do you read it?”
en_uk_webbe~LUK~C10~27~He answered, “You shall love the Lord your God with all your heart, with all your soul, with all your strength, and with all your mind; and your neighbour as yourself.”
en_uk_webbe~LUK~C10~28~He said to him, “You have answered correctly. Do this, and you will live.”
en_uk_webbe~LUK~C10~29~But he, desiring to justify himself, asked Jesus, “Who is my neighbour?”
en_uk_webbe~LUK~C10~30~Jesus answered, “A certain man was going down from Jerusalem to Jericho, and he fell amongst robbers, who both stripped him and beat him, and departed, leaving him half dead.
en_uk_webbe~LUK~C10~31~By chance a certain priest was going down that way. When he saw him, he passed by on the other side.
en_uk_webbe~LUK~C10~32~In the same way a Levite also, when he came to the place, and saw him, passed by on the other side.
en_uk_webbe~LUK~C10~33~But a certain Samaritan, as he travelled, came where he was. When he saw him, he was moved with compassion,
en_uk_webbe~LUK~C10~34~came to him, and bound up his wounds, pouring on oil and wine. He set him on his own animal, brought him to an inn, and took care of him.
en_uk_webbe~LUK~C10~35~On the next day, when he departed, he took out two denarii, gave them to the host, and said to him, ‘Take care of him. Whatever you spend beyond that, I will repay you when I return.’
en_uk_webbe~LUK~C10~36~Now which of these three do you think seemed to be a neighbour to him who fell amongst the robbers?”
en_uk_webbe~LUK~C10~37~He said, “He who showed mercy on him.” Then Jesus said to him, “Go and do likewise.”
en_uk_webbe~LUK~C10~38~As they went on their way, he entered into a certain village, and a certain woman named Martha received him into her house.
en_uk_webbe~LUK~C10~39~She had a sister called Mary, who also sat at Jesus’ feet, and heard his word.
en_uk_webbe~LUK~C10~40~But Martha was distracted with much serving, and she came up to him, and said, “Lord, don’t you care that my sister left me to serve alone? Ask her therefore to help me.”
en_uk_webbe~LUK~C10~41~Jesus answered her, “Martha, Martha, you are anxious and troubled about many things,
en_uk_webbe~LUK~C10~42~but one thing is needed. Mary has chosen the good part, which will not be taken away from her.”
