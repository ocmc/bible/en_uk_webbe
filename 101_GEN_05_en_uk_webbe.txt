en_uk_webbe~GEN~C05~01~This is the book of the generations of Adam. In the day that God created man, he made him in God’s likeness.
en_uk_webbe~GEN~C05~02~He created them male and female, and blessed them. On the day they were created, he named them Adam.
en_uk_webbe~GEN~C05~03~Adam lived one hundred and thirty years, and became the father of a son in his own likeness, after his image, and named him Seth.
en_uk_webbe~GEN~C05~04~The days of Adam after he became the father of Seth were eight hundred years, and he became the father of other sons and daughters.
en_uk_webbe~GEN~C05~05~All the days that Adam lived were nine hundred and thirty years, then he died.
en_uk_webbe~GEN~C05~06~Seth lived one hundred and five years, then became the father of Enosh.
en_uk_webbe~GEN~C05~07~Seth lived after he became the father of Enosh eight hundred and seven years, and became the father of other sons and daughters.
en_uk_webbe~GEN~C05~08~All of the days of Seth were nine hundred and twelve years, then he died.
en_uk_webbe~GEN~C05~09~Enosh lived ninety years, and became the father of Kenan.
en_uk_webbe~GEN~C05~10~Enosh lived after he became the father of Kenan eight hundred and fifteen years, and became the father of other sons and daughters.
en_uk_webbe~GEN~C05~11~All of the days of Enosh were nine hundred and five years, then he died.
en_uk_webbe~GEN~C05~12~Kenan lived seventy years, then became the father of Mahalalel.
en_uk_webbe~GEN~C05~13~Kenan lived after he became the father of Mahalalel eight hundred and forty years, and became the father of other sons and daughters
en_uk_webbe~GEN~C05~14~and all of the days of Kenan were nine hundred and ten years, then he died.
en_uk_webbe~GEN~C05~15~Mahalalel lived sixty-five years, then became the father of Jared.
en_uk_webbe~GEN~C05~16~Mahalalel lived after he became the father of Jared eight hundred and thirty years, and became the father of other sons and daughters.
en_uk_webbe~GEN~C05~17~All of the days of Mahalalel were eight hundred and ninety-five years, then he died.
en_uk_webbe~GEN~C05~18~Jared lived one hundred and sixty-two years, then became the father of Enoch.
en_uk_webbe~GEN~C05~19~Jared lived after he became the father of Enoch eight hundred years, and became the father of other sons and daughters.
en_uk_webbe~GEN~C05~20~All of the days of Jared were nine hundred and sixty-two years, then he died.
en_uk_webbe~GEN~C05~21~Enoch lived sixty-five years, then became the father of Methuselah.
en_uk_webbe~GEN~C05~22~After Methuselah’s birth, Enoch walked with God for three hundred years, and became the father of more sons and daughters.
en_uk_webbe~GEN~C05~23~All the days of Enoch were three hundred and sixty-five years.
en_uk_webbe~GEN~C05~24~Enoch walked with God, and he was not found, for God took him.
en_uk_webbe~GEN~C05~25~Methuselah lived one hundred and eighty-seven years, then became the father of Lamech.
en_uk_webbe~GEN~C05~26~Methuselah lived after he became the father of Lamech seven hundred and eighty-two years, and became the father of other sons and daughters.
en_uk_webbe~GEN~C05~27~All the days of Methuselah were nine hundred and sixty-nine years, then he died.
en_uk_webbe~GEN~C05~28~Lamech lived one hundred and eighty-two years, then became the father of a son.
en_uk_webbe~GEN~C05~29~He named him Noah, saying, “This one will comfort us in our work and in the toil of our hands, caused by the ground which the LORD has cursed.”
en_uk_webbe~GEN~C05~30~Lamech lived after he became the father of Noah five hundred and ninety-five years, and became the father of other sons and daughters.
en_uk_webbe~GEN~C05~31~All the days of Lamech were seven hundred and seventy-seven years, then he died.
en_uk_webbe~GEN~C05~32~Noah was five hundred years old, then Noah became the father of Shem, Ham, and Japheth.
