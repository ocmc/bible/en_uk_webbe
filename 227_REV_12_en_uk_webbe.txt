en_uk_webbe~REV~C12~01~A great sign was seen in heaven: a woman clothed with the sun, and the moon under her feet, and on her head a crown of twelve stars.
en_uk_webbe~REV~C12~02~She was with child. She cried out in pain, labouring to give birth.
en_uk_webbe~REV~C12~03~Another sign was seen in heaven. Behold, a great red dragon, having seven heads and ten horns, and on his heads seven crowns.
en_uk_webbe~REV~C12~04~His tail drew one third of the stars of the sky, and threw them to the earth. The dragon stood before the woman who was about to give birth, so that when she gave birth he might devour her child.
en_uk_webbe~REV~C12~05~She gave birth to a son, a male child, who is to rule all the nations with a rod of iron. Her child was caught up to God, and to his throne.
en_uk_webbe~REV~C12~06~The woman fled into the wilderness, where she has a place prepared by God, that there they may nourish her one thousand and two hundred and sixty days.
en_uk_webbe~REV~C12~07~There was war in the sky. Michael and his angels made war on the dragon. The dragon and his angels made war.
en_uk_webbe~REV~C12~08~They didn’t prevail. No place was found for them any more in heaven.
en_uk_webbe~REV~C12~09~The great dragon was thrown down, the old serpent, he who is called the devil and Satan, the deceiver of the whole world. He was thrown down to the earth, and his angels were thrown down with him.
en_uk_webbe~REV~C12~10~I heard a loud voice in heaven, saying, “Now the salvation, the power, and the Kingdom of our God, and the authority of his Christ has come; for the accuser of our brothers has been thrown down, who accuses them before our God day and night.
en_uk_webbe~REV~C12~11~They overcame him because of the Lamb’s blood, and because of the word of their testimony. They didn’t love their life, even to death.
en_uk_webbe~REV~C12~12~Therefore rejoice, heavens, and you who dwell in them. Woe to the earth and to the sea, because the devil has gone down to you, having great wrath, knowing that he has but a short time.”
en_uk_webbe~REV~C12~13~When the dragon saw that he was thrown down to the earth, he persecuted the woman who gave birth to the male child.
en_uk_webbe~REV~C12~14~Two wings of the great eagle were given to the woman, that she might fly into the wilderness to her place, so that she might be nourished for a time, and times, and half a time, from the face of the serpent.
en_uk_webbe~REV~C12~15~The serpent spewed water out of his mouth after the woman like a river, that he might cause her to be carried away by the stream.
en_uk_webbe~REV~C12~16~The earth helped the woman, and the earth opened its mouth and swallowed up the river which the dragon spewed out of his mouth.
en_uk_webbe~REV~C12~17~The dragon grew angry with the woman, and went away to make war with the rest of her offspring, who keep God’s commandments and hold Jesus’ testimony.
