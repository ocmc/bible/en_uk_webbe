en_uk_webbe~EXO~C35~01~Moses assembled all the congregation of the children of Israel, and said to them, “These are the words which the LORD has commanded, that you should do them.
en_uk_webbe~EXO~C35~02~‘Six days shall work be done, but on the seventh day there shall be a holy day for you, a Sabbath of solemn rest to the LORD: whoever does any work in it shall be put to death.
en_uk_webbe~EXO~C35~03~You shall kindle no fire throughout your habitations on the Sabbath day.’”
en_uk_webbe~EXO~C35~04~Moses spoke to all the congregation of the children of Israel, saying, “This is the thing which the LORD commanded, saying,
en_uk_webbe~EXO~C35~05~‘Take from amongst you an offering to the LORD. Whoever is of a willing heart, let him bring it as the LORD’s offering: gold, silver, bronze,
en_uk_webbe~EXO~C35~06~blue, purple, scarlet, fine linen, goats’ hair,
en_uk_webbe~EXO~C35~07~rams’ skins dyed red, sea cow hides, acacia wood,
en_uk_webbe~EXO~C35~08~oil for the light, spices for the anointing oil and for the sweet incense,
en_uk_webbe~EXO~C35~09~onyx stones, and stones to be set for the ephod and for the breastplate.
en_uk_webbe~EXO~C35~10~“‘Let every wise-hearted man amongst you come, and make all that the LORD has commanded:
en_uk_webbe~EXO~C35~11~the tabernacle, its outer covering, its roof, its clasps, its boards, its bars, its pillars, and its sockets;
en_uk_webbe~EXO~C35~12~the ark, and its poles, the mercy seat, the veil of the screen;
en_uk_webbe~EXO~C35~13~the table with its poles and all its vessels, and the show bread;
en_uk_webbe~EXO~C35~14~the lamp stand also for the light, with its vessels, its lamps, and the oil for the light;
en_uk_webbe~EXO~C35~15~and the altar of incense with its poles, the anointing oil, the sweet incense, the screen for the door, at the door of the tabernacle;
en_uk_webbe~EXO~C35~16~the altar of burnt offering, with its grating of bronze, its poles, and all its vessels, the basin and its base;
en_uk_webbe~EXO~C35~17~the hangings of the court, its pillars, their sockets, and the screen for the gate of the court;
en_uk_webbe~EXO~C35~18~the pins of the tabernacle, the pins of the court, and their cords;
en_uk_webbe~EXO~C35~19~the finely worked garments for ministering in the holy place—the holy garments for Aaron the priest, and the garments of his sons—to minister in the priest’s office.’”
en_uk_webbe~EXO~C35~20~All the congregation of the children of Israel departed from the presence of Moses.
en_uk_webbe~EXO~C35~21~They came, everyone whose heart stirred him up, and everyone whom his spirit made willing, and brought the LORD’s offering for the work of the Tent of Meeting, and for all of its service, and for the holy garments.
en_uk_webbe~EXO~C35~22~They came, both men and women, as many as were willing-hearted, and brought brooches, earrings, signet rings, and armlets, all jewels of gold; even every man who offered an offering of gold to the LORD.
en_uk_webbe~EXO~C35~23~Everyone with whom was found blue, purple, scarlet, fine linen, goats’ hair, rams’ skins dyed red, and sea cow hides, brought them.
en_uk_webbe~EXO~C35~24~Everyone who offered an offering of silver and bronze brought the LORD’s offering; and everyone with whom was found acacia wood for any work of the service, brought it.
en_uk_webbe~EXO~C35~25~All the women who were wise-hearted spun with their hands, and brought that which they had spun: the blue, the purple, the scarlet, and the fine linen.
en_uk_webbe~EXO~C35~26~All the women whose heart stirred them up in wisdom spun the goats’ hair.
en_uk_webbe~EXO~C35~27~The rulers brought the onyx stones and the stones to be set for the ephod and for the breastplate;
en_uk_webbe~EXO~C35~28~with the spice and the oil for the light, for the anointing oil, and for the sweet incense.
en_uk_webbe~EXO~C35~29~The children of Israel brought a free will offering to the LORD; every man and woman whose heart made them willing to bring for all the work, which the LORD had commanded to be made by Moses.
en_uk_webbe~EXO~C35~30~Moses said to the children of Israel, “Behold, the LORD has called by name Bezalel the son of Uri, the son of Hur, of the tribe of Judah.
en_uk_webbe~EXO~C35~31~He has filled him with the Spirit of God, in wisdom, in understanding, in knowledge, and in all kinds of workmanship;
en_uk_webbe~EXO~C35~32~and to make skilful works, to work in gold, in silver, in bronze,
en_uk_webbe~EXO~C35~33~in cutting of stones for setting, and in carving of wood, to work in all kinds of skilful workmanship.
en_uk_webbe~EXO~C35~34~He has put in his heart that he may teach, both he and Oholiab, the son of Ahisamach, of the tribe of Dan.
en_uk_webbe~EXO~C35~35~He has filled them with wisdom of heart to work all kinds of workmanship, of the engraver, of the skilful workman, and of the embroiderer, in blue, in purple, in scarlet, and in fine linen, and of the weaver, even of those who do any workmanship, and of those who make skilful works.
