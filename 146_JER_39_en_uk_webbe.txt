en_uk_webbe~JER~C39~01~In the ninth year of Zedekiah king of Judah, in the tenth month, Nebuchadnezzar king of Babylon and all his army came against Jerusalem, and besieged it.
en_uk_webbe~JER~C39~02~In the eleventh year of Zedekiah, in the fourth month, the ninth day of the month, a breach was made in the city.
en_uk_webbe~JER~C39~03~All the princes of the king of Babylon came in, and sat in the middle gate, Nergal Sharezer, Samgarnebo, Sarsechim, Rabsaris, Nergal Sharezer, Rabmag, with all the rest of the princes of the king of Babylon.
en_uk_webbe~JER~C39~04~When Zedekiah the king of Judah and all the men of war saw them, then they fled, and went out of the city by night, by the way of the king’s garden, through the gate between the two walls; and he went out towards the Arabah.
en_uk_webbe~JER~C39~05~But the army of the Chaldeans pursued them, and overtook Zedekiah in the plains of Jericho. When they had taken him, they brought him up to Nebuchadnezzar king of Babylon to Riblah in the land of Hamath; and he pronounced judgement on him.
en_uk_webbe~JER~C39~06~Then the king of Babylon killed Zedekiah’s sons in Riblah before his eyes. The king of Babylon also killed all the nobles of Judah.
en_uk_webbe~JER~C39~07~Moreover he put out Zedekiah’s eyes and bound him in fetters, to carry him to Babylon.
en_uk_webbe~JER~C39~08~The Chaldeans burnt the king’s house, and the houses of the people, with fire, and broke down the walls of Jerusalem.
en_uk_webbe~JER~C39~09~Then Nebuzaradan the captain of the guard carried away captive into Babylon the residue of the people who remained in the city, the deserters also who fell away to him, and the residue of the people who remained.
en_uk_webbe~JER~C39~10~But Nebuzaradan the captain of the guard left of the poor of the people, who had nothing, in the land of Judah, and gave them vineyards and fields at the same time.
en_uk_webbe~JER~C39~11~Now Nebuchadnezzar king of Babylon commanded Nebuzaradan the captain of the guard concerning Jeremiah, saying,
en_uk_webbe~JER~C39~12~“Take him, and take care of him. Do him no harm; but do to him even as he tells you.”
en_uk_webbe~JER~C39~13~So Nebuzaradan the captain of the guard sent, with Nebushazban, Rabsaris, and Nergal Sharezer, Rabmag, and all the chief officers of the king of Babylon;
en_uk_webbe~JER~C39~14~they sent, and took Jeremiah out of the court of the guard, and committed him to Gedaliah the son of Ahikam, the son of Shaphan, that he should carry him home. So he lived amongst the people.
en_uk_webbe~JER~C39~15~Now the LORD’s word came to Jeremiah, while he was shut up in the court of the guard, saying,
en_uk_webbe~JER~C39~16~“Go, and speak to Ebedmelech the Ethiopian, saying, ‘The LORD of Armies, the God of Israel, says: “Behold, I will bring my words on this city for evil, and not for good; and they will be accomplished before you in that day.
en_uk_webbe~JER~C39~17~But I will deliver you in that day,” says the LORD; “and you will not be given into the hand of the men of whom you are afraid.
en_uk_webbe~JER~C39~18~For I will surely save you, and you won’t fall by the sword, but you will escape with your life; because you have put your trust in me,” says the LORD.’”
