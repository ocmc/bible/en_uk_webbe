en_uk_webbe~SIR~C31~01~Wakefulness that comes of riches consumes the flesh, And the anxiety thereof puts away sleep.
en_uk_webbe~SIR~C31~02~Wakeful anxiety will crave slumber; And in a sore disease sleep will be broken.
en_uk_webbe~SIR~C31~03~A rich man toils in gathering money together; And when he rests, he is filled with his good things.
en_uk_webbe~SIR~C31~04~A poor man toils in lack of substance; And when he rests, he becomes needy.
en_uk_webbe~SIR~C31~05~He that loves gold shall not be justified; And he that follows destruction shall himself have his fill [of it. ]
en_uk_webbe~SIR~C31~06~Many have been given over to ruin for the sake of gold; And their perdition meets them face to face.
en_uk_webbe~SIR~C31~07~It is a stumbling block to those who sacrifice to it; And every fool shall be taken therewith.
en_uk_webbe~SIR~C31~08~Blessed is the rich that is found without blemish, And that goes not after gold.
en_uk_webbe~SIR~C31~09~Who is he? and we will call him blessed: For wonderful things has he done amongst his people.
en_uk_webbe~SIR~C31~10~Who has been tried by it, and found perfect? Then let him glory. Who has had the power to transgress, and has not transgressed? And to do evil, and has not done it?
en_uk_webbe~SIR~C31~11~His goods shall be made sure, And the congregation shall declare his alms.
en_uk_webbe~SIR~C31~12~Sittest you at a great table? be not greedy upon it, And say not, Many are the things upon it.
en_uk_webbe~SIR~C31~13~Remember that an evil eye is a wicked thing: What has been created more evil than an eye? Therefore it sheds tears from every face.
en_uk_webbe~SIR~C31~14~Stretch not your hand wherever it looks, And thrust not yourself with it into the dish.
en_uk_webbe~SIR~C31~15~Consider your neighbour’s [liking ] by your own; And be discreet in every point.
en_uk_webbe~SIR~C31~16~Eat, as [becomes ] a man, those things which are set before you; And eat not greedily, lest you be hated.
en_uk_webbe~SIR~C31~17~Be first to leave off for manners’ sake; And be not insatiable, lest you offend.
en_uk_webbe~SIR~C31~18~And if you sit amongst many, Reach not out your hand before them.
en_uk_webbe~SIR~C31~19~How sufficient to a well-mannered man is a very little, And he does not breathe hard upon his bed.
en_uk_webbe~SIR~C31~20~Healthy sleep comes of moderate eating; He rises early, and his wits are with him: The pain of wakefulness, and colic, And griping, are with an insatiable man.
en_uk_webbe~SIR~C31~21~And if you have been forced to eat, Rise up in the midst thereof, and you shall have rest.
en_uk_webbe~SIR~C31~22~Hear me, my son, and despise me not, And at the last you shall find my words [true: ] In all your works be quick, And no disease shall come to you.
en_uk_webbe~SIR~C31~23~Him that is liberal of his meat the lips shall bless; And the testimony of his excellence shall be believed.
en_uk_webbe~SIR~C31~24~The city will murmer at he who is a stingy with his meat; And the testimony of his stinginess will be sure.
en_uk_webbe~SIR~C31~25~Show not yourself valiant in wine; For wine has destroyed many.
en_uk_webbe~SIR~C31~26~The furnace proves the temper [of steel ] by dipping; So does wine [prove ] hearts in the quarrelling of the proud.
en_uk_webbe~SIR~C31~27~Wine is as good as life to men, If you drink it in its measure: What life is there to a man that is without wine? And it has been created to make men glad.
en_uk_webbe~SIR~C31~28~Wine drunk in season [and ] to satisfy Is joy of heart, and gladness of soul:
en_uk_webbe~SIR~C31~29~Wine drunk largely is bitterness of soul, With provocation and conflict.
en_uk_webbe~SIR~C31~30~Drunkenness increases the rage of a fool to his hurt; It diminishes strength, and adds wounds.
en_uk_webbe~SIR~C31~31~Rebuke not your neighbour at a banquet of wine, Neither set him at nothing in his mirth: Speak not to him a word of reproach, And press not upon him by asking back [a debt. ]
