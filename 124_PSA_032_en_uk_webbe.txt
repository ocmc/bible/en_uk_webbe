en_uk_webbe~PSA~C032~001~(C033:001) Rejoice in the LORD, you righteous! Praise is fitting for the upright.
en_uk_webbe~PSA~C032~002~(C033:002) Give thanks to the LORD with the lyre. Sing praises to him with the harp of ten strings.
en_uk_webbe~PSA~C032~003~(C033:003) Sing to him a new song. Play skilfully with a shout of joy!
en_uk_webbe~PSA~C032~004~(C033:004) For the LORD’s word is right. All his work is done in faithfulness.
en_uk_webbe~PSA~C032~005~(C033:005) He loves righteousness and justice. The earth is full of the loving kindness of the LORD.
en_uk_webbe~PSA~C032~006~(C033:006) By the LORD’s word, the heavens were made: all their army by the breath of his mouth.
en_uk_webbe~PSA~C032~007~(C033:007) He gathers the waters of the sea together as a heap. He lays up the deeps in storehouses.
en_uk_webbe~PSA~C032~008~(C033:008) Let all the earth fear the LORD. Let all the inhabitants of the world stand in awe of him.
en_uk_webbe~PSA~C032~009~(C033:009) For he spoke, and it was done. He commanded, and it stood firm.
en_uk_webbe~PSA~C032~010~(C033:010) The LORD brings the counsel of the nations to nothing. He makes the thoughts of the peoples to be of no effect.
en_uk_webbe~PSA~C032~011~(C033:011) The counsel of the LORD stands fast forever, the thoughts of his heart to all generations.
en_uk_webbe~PSA~C032~012~(C033:012) Blessed is the nation whose God is the LORD, the people whom he has chosen for his own inheritance.
en_uk_webbe~PSA~C032~013~(C033:013) The LORD looks from heaven. He sees all the sons of men.
en_uk_webbe~PSA~C032~014~(C033:014) From the place of his habitation he looks out on all the inhabitants of the earth,
en_uk_webbe~PSA~C032~015~(C033:015) he who fashions all of their hearts; and he considers all of their works.
en_uk_webbe~PSA~C032~016~(C033:016) There is no king saved by the multitude of an army. A mighty man is not delivered by great strength.
en_uk_webbe~PSA~C032~017~(C033:017) A horse is a vain thing for safety, neither does he deliver any by his great power.
en_uk_webbe~PSA~C032~018~(C033:018) Behold, the LORD’s eye is on those who fear him, on those who hope in his loving kindness,
en_uk_webbe~PSA~C032~019~(C033:019) to deliver their soul from death, to keep them alive in famine.
en_uk_webbe~PSA~C032~020~(C033:020) Our soul has waited for the LORD. He is our help and our shield.
en_uk_webbe~PSA~C032~021~(C033:021) For our heart rejoices in him, because we have trusted in his holy name.
en_uk_webbe~PSA~C032~022~(C033:022) Let your loving kindness be on us, LORD, since we have hoped in you.
