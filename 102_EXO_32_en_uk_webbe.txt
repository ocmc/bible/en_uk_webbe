en_uk_webbe~EXO~C32~01~When the people saw that Moses delayed coming down from the mountain, the people gathered themselves together to Aaron, and said to him, “Come, make us gods, which shall go before us; for as for this Moses, the man who brought us up out of the land of Egypt, we don’t know what has become of him.”
en_uk_webbe~EXO~C32~02~Aaron said to them, “Take off the golden rings, which are in the ears of your wives, of your sons, and of your daughters, and bring them to me.”
en_uk_webbe~EXO~C32~03~All the people took off the golden rings which were in their ears, and brought them to Aaron.
en_uk_webbe~EXO~C32~04~He received what they handed him, fashioned it with an engraving tool, and made it a moulded calf. Then they said, “These are your gods, Israel, which brought you up out of the land of Egypt.”
en_uk_webbe~EXO~C32~05~When Aaron saw this, he built an altar before it; and Aaron made a proclamation, and said, “Tomorrow shall be a feast to the LORD.”
en_uk_webbe~EXO~C32~06~They rose up early on the next day, and offered burnt offerings, and brought peace offerings; and the people sat down to eat and to drink, and rose up to play.
en_uk_webbe~EXO~C32~07~The LORD spoke to Moses, “Go, get down; for your people, who you brought up out of the land of Egypt, have corrupted themselves!
en_uk_webbe~EXO~C32~08~They have turned away quickly out of the way which I commanded them. They have made themselves a moulded calf, and have worshipped it, and have sacrificed to it, and said, ‘These are your gods, Israel, which brought you up out of the land of Egypt.’”
en_uk_webbe~EXO~C32~09~The LORD said to Moses, “I have seen these people, and behold, they are a stiff-necked people.
en_uk_webbe~EXO~C32~10~Now therefore leave me alone, that my wrath may burn hot against them, and that I may consume them; and I will make of you a great nation.”
en_uk_webbe~EXO~C32~11~Moses begged the LORD his God, and said, “The LORD, why does your wrath burn hot against your people, that you have brought out of the land of Egypt with great power and with a mighty hand?
en_uk_webbe~EXO~C32~12~Why should the Egyptians talk, saying, ‘He brought them out for evil, to kill them in the mountains, and to consume them from the surface of the earth?’ Turn from your fierce wrath, and turn away from this evil against your people.
en_uk_webbe~EXO~C32~13~Remember Abraham, Isaac, and Israel, your servants, to whom you swore by your own self, and said to them, ‘I will multiply your offspring as the stars of the sky, and all this land that I have spoken of I will give to your offspring, and they shall inherit it forever.’”
en_uk_webbe~EXO~C32~14~So The LORD turned away from the evil which he said he would do to his people.
en_uk_webbe~EXO~C32~15~Moses turned, and went down from the mountain, with the two tablets of the covenant in his hand; tablets that were written on both their sides. They were written on one side and on the other.
en_uk_webbe~EXO~C32~16~The tablets were the work of God, and the writing was the writing of God, engraved on the tablets.
en_uk_webbe~EXO~C32~17~When Joshua heard the noise of the people as they shouted, he said to Moses, “There is the noise of war in the camp.”
en_uk_webbe~EXO~C32~18~He said, “It isn’t the voice of those who shout for victory. It is not the voice of those who cry for being overcome; but the noise of those who sing that I hear.”
en_uk_webbe~EXO~C32~19~As soon as he came near to the camp, he saw the calf and the dancing. Then Moses’ anger grew hot, and he threw the tablets out of his hands, and broke them beneath the mountain.
en_uk_webbe~EXO~C32~20~He took the calf which they had made, and burnt it with fire, ground it to powder, and scattered it on the water, and made the children of Israel drink it.
en_uk_webbe~EXO~C32~21~Moses said to Aaron, “What did these people do to you, that you have brought a great sin on them?”
en_uk_webbe~EXO~C32~22~Aaron said, “Don’t let the anger of my lord grow hot. You know the people, that they are set on evil.
en_uk_webbe~EXO~C32~23~For they said to me, ‘Make us gods, which shall go before us. As for this Moses, the man who brought us up out of the land of Egypt, we don’t know what has become of him.’
en_uk_webbe~EXO~C32~24~I said to them, ‘Whoever has any gold, let them take it off.’ So they gave it to me; and I threw it into the fire, and out came this calf.”
en_uk_webbe~EXO~C32~25~When Moses saw that the people were out of control, (for Aaron had let them lose control, causing derision amongst their enemies),
en_uk_webbe~EXO~C32~26~then Moses stood in the gate of the camp, and said, “Whoever is on the LORD’s side, come to me!” All the sons of Levi gathered themselves together to him.
en_uk_webbe~EXO~C32~27~He said to them, “The LORD, the God of Israel, says, ‘Every man put his sword on his thigh, and go back and forth from gate to gate throughout the camp, and every man kill his brother, and every man his companion, and every man his neighbour.’”
en_uk_webbe~EXO~C32~28~The sons of Levi did according to the word of Moses. About three thousand men fell of the people that day.
en_uk_webbe~EXO~C32~29~Moses said, “Consecrate yourselves today to the LORD, for every man was against his son and against his brother, that he may give you a blessing today.”
en_uk_webbe~EXO~C32~30~On the next day, Moses said to the people, “You have sinned a great sin. Now I will go up to the LORD. Perhaps I shall make atonement for your sin.”
en_uk_webbe~EXO~C32~31~Moses returned to the LORD, and said, “Oh, this people have sinned a great sin, and have made themselves gods of gold.
en_uk_webbe~EXO~C32~32~Yet now, if you will, forgive their sin—and if not, please blot me out of your book which you have written.”
en_uk_webbe~EXO~C32~33~The LORD said to Moses, “Whoever has sinned against me, I will blot him out of my book.
en_uk_webbe~EXO~C32~34~Now go, lead the people to the place of which I have spoken to you. Behold, my angel shall go before you. Nevertheless, in the day when I punish, I will punish them for their sin.”
en_uk_webbe~EXO~C32~35~The LORD struck the people, because of what they did with the calf, which Aaron made.
