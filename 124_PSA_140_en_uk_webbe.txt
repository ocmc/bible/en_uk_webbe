en_uk_webbe~PSA~C140~001~(C141:001) (0) A Psalm by David. (1) LORD, I have called on you. Come to me quickly! Listen to my voice when I call to you.
en_uk_webbe~PSA~C140~002~(C141:002) Let my prayer be set before you like incense; the lifting up of my hands like the evening sacrifice.
en_uk_webbe~PSA~C140~003~(C141:003) Set a watch, LORD, before my mouth. Keep the door of my lips.
en_uk_webbe~PSA~C140~004~(C141:004) Don’t incline my heart to any evil thing, to practise deeds of wickedness with men who work iniquity. Don’t let me eat of their delicacies.
en_uk_webbe~PSA~C140~005~(C141:005) Let the righteous strike me, it is kindness; let him reprove me, it is like oil on the head; don’t let my head refuse it; Yet my prayer is always against evil deeds.
en_uk_webbe~PSA~C140~006~(C141:006) Their judges are thrown down by the sides of the rock. They will hear my words, for they are well spoken.
en_uk_webbe~PSA~C140~007~(C141:007) “As when one ploughs and breaks up the earth, our bones are scattered at the mouth of Sheol.”
en_uk_webbe~PSA~C140~008~(C141:008) For my eyes are on you, LORD, the Lord. In you, I take refuge. Don’t leave my soul destitute.
en_uk_webbe~PSA~C140~009~(C141:009) Keep me from the snare which they have laid for me, from the traps of the workers of iniquity.
en_uk_webbe~PSA~C140~010~(C141:010) Let the wicked fall together into their own nets while I pass by.
