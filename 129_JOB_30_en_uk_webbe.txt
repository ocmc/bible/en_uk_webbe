en_uk_webbe~JOB~C30~01~“But now those who are younger than I have me in derision, whose fathers I considered unworthy to put with my sheep dogs.
en_uk_webbe~JOB~C30~02~Of what use is the strength of their hands to me, men in whom ripe age has perished?
en_uk_webbe~JOB~C30~03~They are gaunt from lack and famine. They gnaw the dry ground, in the gloom of waste and desolation.
en_uk_webbe~JOB~C30~04~They pluck salt herbs by the bushes. The roots of the broom tree are their food.
en_uk_webbe~JOB~C30~05~They are driven out from amongst men. They cry after them as after a thief;
en_uk_webbe~JOB~C30~06~So that they dwell in frightful valleys, and in holes of the earth and of the rocks.
en_uk_webbe~JOB~C30~07~They bray amongst the bushes. They are gathered together under the nettles .
en_uk_webbe~JOB~C30~08~They are children of fools, yes, children of wicked men. They were flogged out of the land.
en_uk_webbe~JOB~C30~09~“Now I have become their song. Yes, I am a byword to them.
en_uk_webbe~JOB~C30~10~They abhor me, they stand aloof from me, and don’t hesitate to spit in my face.
en_uk_webbe~JOB~C30~11~For he has untied his cord, and afflicted me; and they have thrown off restraint before me.
en_uk_webbe~JOB~C30~12~On my right hand rise the rabble. They thrust aside my feet, They cast up against me their ways of destruction.
en_uk_webbe~JOB~C30~13~They mar my path. They promote my destruction without anyone’s help.
en_uk_webbe~JOB~C30~14~As through a wide breach they come. They roll themselves in amid the ruin.
en_uk_webbe~JOB~C30~15~Terrors have turned on me. They chase my honour as the wind. My welfare has passed away as a cloud.
en_uk_webbe~JOB~C30~16~“Now my soul is poured out within me. Days of affliction have taken hold of me.
en_uk_webbe~JOB~C30~17~In the night season my bones are pierced in me, and the pains that gnaw me take no rest.
en_uk_webbe~JOB~C30~18~My garment is disfigured by great force. It binds me about as the collar of my tunic.
en_uk_webbe~JOB~C30~19~He has cast me into the mire. I have become like dust and ashes.
en_uk_webbe~JOB~C30~20~I cry to you, and you do not answer me. I stand up, and you gaze at me.
en_uk_webbe~JOB~C30~21~You have turned to be cruel to me. With the might of your hand you persecute me.
en_uk_webbe~JOB~C30~22~You lift me up to the wind, and drive me with it. You dissolve me in the storm.
en_uk_webbe~JOB~C30~23~For I know that you will bring me to death, To the house appointed for all living.
en_uk_webbe~JOB~C30~24~“However doesn’t one stretch out a hand in his fall? Or in his calamity therefore cry for help?
en_uk_webbe~JOB~C30~25~Didn’t I weep for him who was in trouble? Wasn’t my soul grieved for the needy?
en_uk_webbe~JOB~C30~26~When I looked for good, then evil came. When I waited for light, darkness came.
en_uk_webbe~JOB~C30~27~My heart is troubled, and doesn’t rest. Days of affliction have come on me.
en_uk_webbe~JOB~C30~28~I go mourning without the sun. I stand up in the assembly, and cry for help.
en_uk_webbe~JOB~C30~29~I am a brother to jackals, and a companion to ostriches.
en_uk_webbe~JOB~C30~30~My skin grows black and peels from me. My bones are burnt with heat.
en_uk_webbe~JOB~C30~31~Therefore my harp has turned to mourning, and my pipe into the voice of those who weep.
