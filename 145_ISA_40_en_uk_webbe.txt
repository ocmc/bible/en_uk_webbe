en_uk_webbe~ISA~C40~01~“Comfort, comfort my people,” says your God.
en_uk_webbe~ISA~C40~02~“Speak comfortably to Jerusalem; and call out to her that her warfare is accomplished, that her iniquity is pardoned, that she has received of the LORD’s hand double for all her sins.”
en_uk_webbe~ISA~C40~03~The voice of one who calls out, “Prepare the way of the LORD in the wilderness! Make a level highway in the desert for our God.
en_uk_webbe~ISA~C40~04~Every valley shall be exalted, and every mountain and hill shall be made low. The uneven shall be made level, and the rough places a plain.
en_uk_webbe~ISA~C40~05~The LORD’s glory shall be revealed, and all flesh shall see it together; for the mouth of the LORD has spoken it.”
en_uk_webbe~ISA~C40~06~The voice of one saying, “Cry!” One said, “What shall I cry?” “All flesh is like grass, and all its glory is like the flower of the field.
en_uk_webbe~ISA~C40~07~The grass withers, the flower fades, because the LORD’s breath blows on it. Surely the people are like grass.
en_uk_webbe~ISA~C40~08~The grass withers, the flower fades; but the word of our God stands forever.”
en_uk_webbe~ISA~C40~09~You who tell good news to Zion, go up on a high mountain. You who tell good news to Jerusalem, lift up your voice with strength! Lift it up! Don’t be afraid! Say to the cities of Judah, “Behold, your God!”
en_uk_webbe~ISA~C40~10~Behold, the Lord GOD will come as a mighty one, and his arm will rule for him. Behold, his reward is with him, and his recompense before him.
en_uk_webbe~ISA~C40~11~He will feed his flock like a shepherd. He will gather the lambs in his arm, and carry them in his bosom. He will gently lead those who have their young.
en_uk_webbe~ISA~C40~12~Who has measured the waters in the hollow of his hand, and marked off the sky with his span, and calculated the dust of the earth in a measuring basket, and weighed the mountains in scales, and the hills in a balance?
en_uk_webbe~ISA~C40~13~Who has directed the LORD’s Spirit, or has taught him as his counsellor?
en_uk_webbe~ISA~C40~14~Who did he take counsel with, and who instructed him, and taught him in the path of justice, and taught him knowledge, and showed him the way of understanding?
en_uk_webbe~ISA~C40~15~Behold, the nations are like a drop in a bucket, and are regarded as a speck of dust on a balance. Behold, he lifts up the islands like a very little thing.
en_uk_webbe~ISA~C40~16~Lebanon is not sufficient to burn, nor its animals sufficient for a burnt offering.
en_uk_webbe~ISA~C40~17~All the nations are like nothing before him. They are regarded by him as less than nothing, and vanity.
en_uk_webbe~ISA~C40~18~To whom then will you liken God? Or what likeness will you compare to him?
en_uk_webbe~ISA~C40~19~A workman has cast an image, and the goldsmith overlays it with gold, and casts silver chains for it.
en_uk_webbe~ISA~C40~20~He who is too impoverished for such an offering chooses a tree that will not rot. He seeks a skilful workman to set up a carved image for him that will not be moved.
en_uk_webbe~ISA~C40~21~Haven’t you known? Haven’t you heard? Haven’t you been told from the beginning? Haven’t you understood from the foundations of the earth?
en_uk_webbe~ISA~C40~22~It is he who sits above the circle of the earth, and its inhabitants are like grasshoppers; who stretches out the heavens like a curtain, and spreads them out like a tent to dwell in,
en_uk_webbe~ISA~C40~23~who brings princes to nothing, who makes the judges of the earth meaningless.
en_uk_webbe~ISA~C40~24~They are planted scarcely. They are sown scarcely. Their stock has scarcely taken root in the ground. He merely blows on them, and they wither, and the whirlwind takes them away as stubble.
en_uk_webbe~ISA~C40~25~“To whom then will you liken me? Who is my equal?” says the Holy One.
en_uk_webbe~ISA~C40~26~Lift up your eyes on high, and see who has created these, who brings out their army by number. He calls them all by name. by the greatness of his might, and because he is strong in power, not one is lacking.
en_uk_webbe~ISA~C40~27~Why do you say, Jacob, and speak, Israel, “My way is hidden from the LORD, and the justice due me is disregarded by my God?”
en_uk_webbe~ISA~C40~28~Haven’t you known? Haven’t you heard? The everlasting God, the LORD, the Creator of the ends of the earth, doesn’t faint. He isn’t weary. His understanding is unsearchable.
en_uk_webbe~ISA~C40~29~He gives power to the weak. He increases the strength of him who has no might.
en_uk_webbe~ISA~C40~30~Even the youths faint and get weary, and the young men utterly fall;
en_uk_webbe~ISA~C40~31~but those who wait for the LORD will renew their strength. They will mount up with wings like eagles. They will run, and not be weary. They will walk, and not faint.
