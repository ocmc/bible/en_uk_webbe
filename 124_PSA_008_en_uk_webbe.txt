en_uk_webbe~PSA~C008~001~(C008:000) For the Chief Musician; on an instrument of Gath.
en_uk_webbe~PSA~C008~002~(C008:001) LORD, our Lord, how majestic is your name in all the earth! You have set your glory above the heavens!
en_uk_webbe~PSA~C008~003~(C008:002) From the lips of babes and infants you have established strength, because of your adversaries, that you might silence the enemy and the avenger.
en_uk_webbe~PSA~C008~004~(C008:003) When I consider your heavens, the work of your fingers, the moon and the stars, which you have ordained;
en_uk_webbe~PSA~C008~005~(C008:004) what is man, that you think of him? What is the son of man, that you care for him?
en_uk_webbe~PSA~C008~006~(C008:005) For you have made him a little lower than the angels, and crowned him with glory and honour.
en_uk_webbe~PSA~C008~007~(C008:006) You make him ruler over the works of your hands. You have put all things under his feet:
en_uk_webbe~PSA~C008~008~(C008:007) All sheep and cattle, yes, and the animals of the field,
en_uk_webbe~PSA~C008~009~(C008:008) the birds of the sky, the fish of the sea, and whatever passes through the paths of the seas.
en_uk_webbe~PSA~C008~010~(C008:009) LORD, our Lord, how majestic is your name in all the earth!
