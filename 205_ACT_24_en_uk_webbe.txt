en_uk_webbe~ACT~C24~01~After five days, the high priest, Ananias, came down with certain elders and an orator, one Tertullus. They informed the governor against Paul.
en_uk_webbe~ACT~C24~02~When he was called, Tertullus began to accuse him, saying, “Seeing that by you we enjoy much peace, and that prosperity is coming to this nation by your foresight,
en_uk_webbe~ACT~C24~03~we accept it in all ways and in all places, most excellent Felix, with all thankfulness.
en_uk_webbe~ACT~C24~04~But that I don’t delay you, I entreat you to bear with us and hear a few words.
en_uk_webbe~ACT~C24~05~For we have found this man to be a plague, an instigator of insurrections amongst all the Jews throughout the world, and a ringleader of the sect of the Nazarenes.
en_uk_webbe~ACT~C24~06~He even tried to profane the temple, and we arrested him.
en_uk_webbe~ACT~C24~07~(*) But the commanding officer, Lysias, came by and with great violence took him out of our hands.
en_uk_webbe~ACT~C24~08~By examining him yourself you may ascertain all these things of which we accuse him.”
en_uk_webbe~ACT~C24~09~The Jews also joined in the attack, affirming that these things were so.
en_uk_webbe~ACT~C24~10~When the governor had beckoned to him to speak, Paul answered, “Because I know that you have been a judge of this nation for many years, I cheerfully make my defence,
en_uk_webbe~ACT~C24~11~seeing that you can verify that it is not more than twelve days since I went up to worship at Jerusalem.
en_uk_webbe~ACT~C24~12~In the temple they didn’t find me disputing with anyone or stirring up a crowd, either in the synagogues, or in the city.
en_uk_webbe~ACT~C24~13~Nor can they prove to you the things of which they now accuse me.
en_uk_webbe~ACT~C24~14~But this I confess to you, that after the Way, which they call a sect, so I serve the God of our fathers, believing all things which are according to the law, and which are written in the prophets;
en_uk_webbe~ACT~C24~15~having hope towards God, which these also themselves look for, that there will be a resurrection of the dead, both of the just and unjust.
en_uk_webbe~ACT~C24~16~In this I also practise always having a conscience void of offence towards God and men.
en_uk_webbe~ACT~C24~17~Now after some years, I came to bring gifts for the needy to my nation, and offerings;
en_uk_webbe~ACT~C24~18~amid which certain Jews from Asia found me purified in the temple, not with a mob, nor with turmoil.
en_uk_webbe~ACT~C24~19~They ought to have been here before you, and to make accusation, if they had anything against me.
en_uk_webbe~ACT~C24~20~Or else let these men themselves say what injustice they found in me when I stood before the council,
en_uk_webbe~ACT~C24~21~unless it is for this one thing that I cried standing amongst them, ‘Concerning the resurrection of the dead I am being judged before you today!’”
en_uk_webbe~ACT~C24~22~But Felix, having more exact knowledge concerning the Way, deferred them, saying, “When Lysias, the commanding officer, comes down, I will decide your case.”
en_uk_webbe~ACT~C24~23~He ordered the centurion that Paul should be kept in custody, and should have some privileges, and not to forbid any of his friends to serve him or to visit him.
en_uk_webbe~ACT~C24~24~But after some days, Felix came with Drusilla, his wife, who was a Jewess, and sent for Paul, and heard him concerning the faith in Christ Jesus.
en_uk_webbe~ACT~C24~25~As he reasoned about righteousness, self-control, and the judgement to come, Felix was terrified, and answered, “Go your way for this time, and when it is convenient for me, I will summon you.”
en_uk_webbe~ACT~C24~26~Meanwhile, he also hoped that money would be given to him by Paul, that he might release him. Therefore also he sent for him more often and talked with him.
en_uk_webbe~ACT~C24~27~But when two years were fulfilled, Felix was succeeded by Porcius Festus, and desiring to gain favour with the Jews, Felix left Paul in bonds.
