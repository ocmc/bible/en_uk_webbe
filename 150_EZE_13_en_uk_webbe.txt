en_uk_webbe~EZE~C13~01~The LORD’s word came to me, saying,
en_uk_webbe~EZE~C13~02~“Son of man, prophesy against the prophets of Israel who prophesy, and say to those who prophesy out of their own heart, ‘Hear the LORD’s word:
en_uk_webbe~EZE~C13~03~The Lord GOD says, “Woe to the foolish prophets, who follow their own spirit, and have seen nothing!
en_uk_webbe~EZE~C13~04~Israel, your prophets have been like foxes in the waste places.
en_uk_webbe~EZE~C13~05~You have not gone up into the gaps or built up the wall for the house of Israel, to stand in the battle in the LORD’s day.
en_uk_webbe~EZE~C13~06~They have seen falsehood and lying divination, who say, ‘The LORD says;’ but the LORD has not sent them. They have made men to hope that the word would be confirmed.
en_uk_webbe~EZE~C13~07~Haven’t you seen a false vision, and haven’t you spoken a lying divination, in that you say, ‘The LORD says;’ but I have not spoken?”
en_uk_webbe~EZE~C13~08~“‘Therefore the Lord GOD says: “Because you have spoken falsehood and seen lies, therefore, behold, I am against you,” says the Lord GOD.
en_uk_webbe~EZE~C13~09~“My hand will be against the prophets who see false visions and who utter lying divinations. They will not be in the council of my people, neither will they be written in the writing of the house of Israel, neither will they enter into the land of Israel. Then you will know that I am the Lord GOD.”
en_uk_webbe~EZE~C13~10~“‘Because, even because they have seduced my people, saying, “Peace;” and there is no peace. When one builds up a wall, behold, they plaster it with whitewash.
en_uk_webbe~EZE~C13~11~Tell those who plaster it with whitewash that it will fall. There will be an overflowing shower; and you, great hailstones, will fall. A stormy wind will tear it.
en_uk_webbe~EZE~C13~12~Behold, when the wall has fallen, won’t it be said to you, “Where is the plaster with which you have plastered it?”
en_uk_webbe~EZE~C13~13~“‘Therefore the Lord GOD says: “I will even tear it with a stormy wind in my wrath. There will be an overflowing shower in my anger, and great hailstones in wrath to consume it.
en_uk_webbe~EZE~C13~14~So I will break down the wall that you have plastered with whitewash, and bring it down to the ground, so that its foundation will be uncovered. It will fall, and you will be consumed in the middle of it. Then you will know that I am the LORD.
en_uk_webbe~EZE~C13~15~Thus I will accomplish my wrath on the wall, and on those who have plastered it with whitewash. I will tell you, ‘The wall is no more, neither those who plastered it;
en_uk_webbe~EZE~C13~16~to wit, the prophets of Israel who prophesy concerning Jerusalem, and who see visions of peace for her, and there is no peace,’” says the Lord GOD.’”
en_uk_webbe~EZE~C13~17~You, son of man, set your face against the daughters of your people, who prophesy out of their own heart; and prophesy against them,
en_uk_webbe~EZE~C13~18~and say, “The Lord GOD says: ‘Woe to the women who sew pillows on all elbows, and make kerchiefs for the head of persons of every stature to hunt souls! Will you hunt the souls of my people, and save souls alive for yourselves?
en_uk_webbe~EZE~C13~19~You have profaned me amongst my people for handfuls of barley and for pieces of bread, to kill the souls who should not die, and to save the souls alive who should not live, by your lying to my people who listen to lies.’
en_uk_webbe~EZE~C13~20~“Therefore the Lord GOD says: ‘Behold, I am against your pillows, with which you hunt the souls to make them fly, and I will tear them from your arms. I will let the souls go, even the souls whom you hunt to make them fly.
en_uk_webbe~EZE~C13~21~I will also tear your kerchiefs, and deliver my people out of your hand, and they will be no more in your hand to be hunted. Then you will know that I am the LORD.
en_uk_webbe~EZE~C13~22~Because with lies you have grieved the heart of the righteous, whom I have not made sad; and strengthened the hands of the wicked, that he should not return from his wicked way, and be saved alive.
en_uk_webbe~EZE~C13~23~Therefore you shall no more see false visions, nor practise divination. I will deliver my people out of your hand. Then you will know that I am the LORD.’”
