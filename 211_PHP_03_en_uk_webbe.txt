en_uk_webbe~PHP~C03~01~Finally, my brothers, rejoice in the Lord! To write the same things to you, to me indeed is not tiresome, but for you it is safe.
en_uk_webbe~PHP~C03~02~Beware of the dogs; beware of the evil workers; beware of the false circumcision.
en_uk_webbe~PHP~C03~03~For we are the circumcision, who worship God in the Spirit, and rejoice in Christ Jesus, and have no confidence in the flesh;
en_uk_webbe~PHP~C03~04~though I myself might have confidence even in the flesh. If any other man thinks that he has confidence in the flesh, I yet more:
en_uk_webbe~PHP~C03~05~circumcised the eighth day, of the stock of Israel, of the tribe of Benjamin, a Hebrew of Hebrews; concerning the law, a Pharisee;
en_uk_webbe~PHP~C03~06~concerning zeal, persecuting the assembly; concerning the righteousness which is in the law, found blameless.
en_uk_webbe~PHP~C03~07~However, I consider those things that were gain to me as a loss for Christ.
en_uk_webbe~PHP~C03~08~Yes most certainly, and I count all things to be a loss for the excellency of the knowledge of Christ Jesus, my Lord, for whom I suffered the loss of all things, and count them nothing but refuse, that I may gain Christ
en_uk_webbe~PHP~C03~09~and be found in him, not having a righteousness of my own, that which is of the law, but that which is through faith in Christ, the righteousness which is from God by faith,
en_uk_webbe~PHP~C03~10~that I may know him, and the power of his resurrection, and the fellowship of his sufferings, becoming conformed to his death,
en_uk_webbe~PHP~C03~11~if by any means I may attain to the resurrection from the dead.
en_uk_webbe~PHP~C03~12~Not that I have already obtained, or am already made perfect; but I press on, that I may take hold of that for which also I was taken hold of by Christ Jesus.
en_uk_webbe~PHP~C03~13~Brothers, I don’t regard myself as yet having taken hold, but one thing I do: forgetting the things which are behind, and stretching forward to the things which are before,
en_uk_webbe~PHP~C03~14~I press on towards the goal for the prize of the high calling of God in Christ Jesus.
en_uk_webbe~PHP~C03~15~Let us therefore, as many as are perfect, think this way. If in anything you think otherwise, God will also reveal that to you.
en_uk_webbe~PHP~C03~16~Nevertheless, to the extent that we have already attained, let’s walk by the same rule. Let’s be of the same mind.
en_uk_webbe~PHP~C03~17~Brothers, be imitators together of me, and note those who walk this way, even as you have us for an example.
en_uk_webbe~PHP~C03~18~For many walk, of whom I told you often, and now tell you even weeping, as the enemies of the cross of Christ,
en_uk_webbe~PHP~C03~19~whose end is destruction, whose god is the belly, and whose glory is in their shame, who think about earthly things.
en_uk_webbe~PHP~C03~20~For our citizenship is in heaven, from where we also wait for a Saviour, the Lord Jesus Christ,
en_uk_webbe~PHP~C03~21~who will change the body of our humiliation to be conformed to the body of his glory, according to the working by which he is able even to subject all things to himself.
