en_uk_webbe~LEV~C07~01~“‘This is the law of the trespass offering: It is most holy.
en_uk_webbe~LEV~C07~02~In the place where they kill the burnt offering, he shall kill the trespass offering; and its blood he shall sprinkle around on the altar.
en_uk_webbe~LEV~C07~03~He shall offer all of its fat: the fat tail, and the fat that covers the innards,
en_uk_webbe~LEV~C07~04~and he shall take away the two kidneys, and the fat that is on them, which is by the loins, and the cover on the liver, with the kidneys;
en_uk_webbe~LEV~C07~05~and the priest shall burn them on the altar for an offering made by fire to the LORD: it is a trespass offering.
en_uk_webbe~LEV~C07~06~Every male amongst the priests may eat of it. It shall be eaten in a holy place. It is most holy.
en_uk_webbe~LEV~C07~07~“‘As is the sin offering, so is the trespass offering; there is one law for them. The priest who makes atonement with them shall have it.
en_uk_webbe~LEV~C07~08~The priest who offers any man’s burnt offering shall have for himself the skin of the burnt offering which he has offered.
en_uk_webbe~LEV~C07~09~Every meal offering that is baked in the oven, and all that is prepared in the pan and on the griddle, shall be the priest’s who offers it.
en_uk_webbe~LEV~C07~10~Every meal offering, mixed with oil or dry, belongs to all the sons of Aaron, one as well as another.
en_uk_webbe~LEV~C07~11~“‘This is the law of the sacrifice of peace offerings, which one shall offer to the LORD:
en_uk_webbe~LEV~C07~12~If he offers it for a thanksgiving, then he shall offer with the sacrifice of thanksgiving unleavened cakes mixed with oil, and unleavened wafers anointed with oil, and cakes mixed with oil.
en_uk_webbe~LEV~C07~13~He shall offer his offering with the sacrifice of his peace offerings for thanksgiving with cakes of leavened bread.
en_uk_webbe~LEV~C07~14~Of it he shall offer one out of each offering for a heave offering to the LORD. It shall be the priest’s who sprinkles the blood of the peace offerings.
en_uk_webbe~LEV~C07~15~The flesh of the sacrifice of his peace offerings for thanksgiving shall be eaten on the day of his offering. He shall not leave any of it until the morning.
en_uk_webbe~LEV~C07~16~“‘But if the sacrifice of his offering is a vow, or a free will offering, it shall be eaten on the day that he offers his sacrifice. On the next day what remains of it shall be eaten,
en_uk_webbe~LEV~C07~17~but what remains of the meat of the sacrifice on the third day shall be burnt with fire.
en_uk_webbe~LEV~C07~18~If any of the meat of the sacrifice of his peace offerings is eaten on the third day, it will not be accepted, and it shall not be credited to him who offers it. It will be an abomination, and the soul who eats any of it will bear his iniquity.
en_uk_webbe~LEV~C07~19~“‘The meat that touches any unclean thing shall not be eaten. It shall be burnt with fire. As for the meat, everyone who is clean may eat it;
en_uk_webbe~LEV~C07~20~but the soul who eats of the meat of the sacrifice of peace offerings that belongs to the LORD, having his uncleanness on him, that soul shall be cut off from his people.
en_uk_webbe~LEV~C07~21~When anyone touches any unclean thing, the uncleanness of man, or an unclean animal, or any unclean abomination, and eats some of the meat of the sacrifice of peace offerings which belong to the LORD, that soul shall be cut off from his people.’”
en_uk_webbe~LEV~C07~22~The LORD spoke to Moses, saying,
en_uk_webbe~LEV~C07~23~“Speak to the children of Israel, saying, ‘You shall eat no fat, of bull, or sheep, or goat.
en_uk_webbe~LEV~C07~24~The fat of that which dies of itself, and the fat of that which is torn of animals, may be used for any other service, but you shall in no way eat of it.
en_uk_webbe~LEV~C07~25~For whoever eats the fat of the animal which men offer as an offering made by fire to the LORD, even the soul who eats it shall be cut off from his people.
en_uk_webbe~LEV~C07~26~You shall not eat any blood, whether it is of bird or of animal, in any of your dwellings.
en_uk_webbe~LEV~C07~27~Whoever it is who eats any blood, that soul shall be cut off from his people.’”
en_uk_webbe~LEV~C07~28~The LORD spoke to Moses, saying,
en_uk_webbe~LEV~C07~29~“Speak to the children of Israel, saying, ‘He who offers the sacrifice of his peace offerings to the LORD shall bring his offering to the LORD out of the sacrifice of his peace offerings.
en_uk_webbe~LEV~C07~30~With his own hands he shall bring the offerings of the LORD made by fire. He shall bring the fat with the breast, that the breast may be waved for a wave offering before the LORD.
en_uk_webbe~LEV~C07~31~The priest shall burn the fat on the altar, but the breast shall be Aaron’s and his sons’.
en_uk_webbe~LEV~C07~32~The right thigh you shall give to the priest for a heave offering out of the sacrifices of your peace offerings.
en_uk_webbe~LEV~C07~33~He amongst the sons of Aaron who offers the blood of the peace offerings, and the fat, shall have the right thigh for a portion.
en_uk_webbe~LEV~C07~34~For the waved breast and the heaved thigh I have taken from the children of Israel out of the sacrifices of their peace offerings, and have given them to Aaron the priest and to his sons as their portion forever from the children of Israel.’”
en_uk_webbe~LEV~C07~35~This is the consecrated portion of Aaron, and the consecrated portion of his sons, out of the offerings of the LORD made by fire, in the day when he presented them to minister to the LORD in the priest’s office;
en_uk_webbe~LEV~C07~36~which the LORD commanded to be given them of the children of Israel, in the day that he anointed them. It is their portion forever throughout their generations.
en_uk_webbe~LEV~C07~37~This is the law of the burnt offering, the meal offering, the sin offering, the trespass offering, the consecration, and the sacrifice of peace offerings
en_uk_webbe~LEV~C07~38~which the LORD commanded Moses in Mount Sinai in the day that he commanded the children of Israel to offer their offerings to the LORD, in the wilderness of Sinai.
