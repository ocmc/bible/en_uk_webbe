en_uk_webbe~SIR~C03~01~Hear me, your father, O my children, and do what you hear, that you all may be saved.
en_uk_webbe~SIR~C03~02~For the Lord has given the father glory concerning the children, and has confirmed the judgement of the mother concerning the sons.
en_uk_webbe~SIR~C03~03~He who honours his father will make atonement for sins.
en_uk_webbe~SIR~C03~04~He that gives glory to his mother is as one who lays up treasure.
en_uk_webbe~SIR~C03~05~Whoever honours his father will have joy in his own children. He will be heard in the day of his prayer.
en_uk_webbe~SIR~C03~06~He who gives glory to his father will have length of days. He who listens to the Lord will bring rest to his mother,
en_uk_webbe~SIR~C03~07~and will serve under his parents, as to masters.
en_uk_webbe~SIR~C03~08~Honour your father in deed and word, that a blessing may come upon you from him.
en_uk_webbe~SIR~C03~09~For the blessing of the father establishes the houses of children, but the curse of the mother roots out the foundations.
en_uk_webbe~SIR~C03~10~Don’t glorify yourself in the dishonour of your father, for your father’s dishonour is no glory to you.
en_uk_webbe~SIR~C03~11~For the glory of a man is from the honour of his father, and a mother in dishonour is a reproach to her children.
en_uk_webbe~SIR~C03~12~My son, help your father in his old age, and don’t grieve him as long as he lives.
en_uk_webbe~SIR~C03~13~If he fails in understanding, have patience with him. Don’t dishonour him in your full strength.
en_uk_webbe~SIR~C03~14~For the relieving of your father will not be forgotten. Instead of sins it will be added to build you up.
en_uk_webbe~SIR~C03~15~In the day of your affliction it will remember you, as fair weather upon ice, ao will your sins also melt away.
en_uk_webbe~SIR~C03~16~He who forsakes his father is as a blasphemer. He who provokes his mother is cursed by the Lord.
en_uk_webbe~SIR~C03~17~My son, go on with your business in humility; so will you be loved by an acceptable man.
en_uk_webbe~SIR~C03~18~The greater you are, humble yourself the more, and you will find favour before the Lord.
en_uk_webbe~SIR~C03~19~
en_uk_webbe~SIR~C03~20~For the power of the Lord is great, and he is glorified by those who are lowly.
en_uk_webbe~SIR~C03~21~Don’t seek things that are too hard for you, and don’t search out things that are above your strength.
en_uk_webbe~SIR~C03~22~Think about the things that have been commanded you, for you have no need of the things that are secret.
en_uk_webbe~SIR~C03~23~Don’t be overly busy in your superfluous works, for more things are showed to you than men can understand.
en_uk_webbe~SIR~C03~24~For the conceit of many has led them astray. Evil opinion has caused their judgement to slip.
en_uk_webbe~SIR~C03~25~There is no light without eyes. There is no wisdom without knowledge.
en_uk_webbe~SIR~C03~26~A stubborn heart will do badly at the end. He who loves danger will perish in it.
en_uk_webbe~SIR~C03~27~A stubborn heart will be burdened with troubles. The sinner will heap sin upon sins.
en_uk_webbe~SIR~C03~28~The calamity of the proud is no healing, for a weed of wickedness has taken root in him.
en_uk_webbe~SIR~C03~29~The heart of the prudent will understand a parable. A wise man desires the ear of a listener.
en_uk_webbe~SIR~C03~30~Water will quench a flaming fire; almsgiving will make atonement for sins.
en_uk_webbe~SIR~C03~31~He repays good turns is mindful of that which comes afterward. In the time of his falling he will find a support.
