en_uk_webbe~JOH~C05~01~After these things, there was a feast of the Jews, and Jesus went up to Jerusalem.
en_uk_webbe~JOH~C05~02~Now in Jerusalem by the sheep gate, there is a pool, which is called in Hebrew, “Bethesda”, having five porches.
en_uk_webbe~JOH~C05~03~In these lay a great multitude of those who were sick, blind, lame, or paralysed, waiting for the moving of the water;
en_uk_webbe~JOH~C05~04~for an angel went down at certain times into the pool and stirred up the water. Whoever stepped in first after the stirring of the water was healed of whatever disease he had.
en_uk_webbe~JOH~C05~05~A certain man was there who had been sick for thirty-eight years.
en_uk_webbe~JOH~C05~06~When Jesus saw him lying there, and knew that he had been sick for a long time, he asked him, “Do you want to be made well?”
en_uk_webbe~JOH~C05~07~The sick man answered him, “Sir, I have no one to put me into the pool when the water is stirred up, but while I’m coming, another steps down before me.”
en_uk_webbe~JOH~C05~08~Jesus said to him, “Arise, take up your mat, and walk.”
en_uk_webbe~JOH~C05~09~Immediately, the man was made well, and took up his mat and walked. Now it was the Sabbath on that day.
en_uk_webbe~JOH~C05~10~So the Jews said to him who was cured, “It is the Sabbath. It is not lawful for you to carry the mat.”
en_uk_webbe~JOH~C05~11~He answered them, “He who made me well said to me, ‘Take up your mat and walk.’”
en_uk_webbe~JOH~C05~12~Then they asked him, “Who is the man who said to you, ‘Take up your mat and walk’?”
en_uk_webbe~JOH~C05~13~But he who was healed didn’t know who it was, for Jesus had withdrawn, a crowd being in the place.
en_uk_webbe~JOH~C05~14~Afterward Jesus found him in the temple, and said to him, “Behold, you are made well. Sin no more, so that nothing worse happens to you.”
en_uk_webbe~JOH~C05~15~The man went away, and told the Jews that it was Jesus who had made him well.
en_uk_webbe~JOH~C05~16~For this cause the Jews persecuted Jesus, and sought to kill him, because he did these things on the Sabbath.
en_uk_webbe~JOH~C05~17~But Jesus answered them, “My Father is still working, so I am working, too.”
en_uk_webbe~JOH~C05~18~For this cause therefore the Jews sought all the more to kill him, because he not only broke the Sabbath, but also called God his own Father, making himself equal with God.
en_uk_webbe~JOH~C05~19~Jesus therefore answered them, “Most certainly, I tell you, the Son can do nothing of himself, but what he sees the Father doing. For whatever things he does, these the Son also does likewise.
en_uk_webbe~JOH~C05~20~For the Father has affection for the Son, and shows him all things that he himself does. He will show him greater works than these, that you may marvel.
en_uk_webbe~JOH~C05~21~For as the Father raises the dead and gives them life, even so the Son also gives life to whom he desires.
en_uk_webbe~JOH~C05~22~For the Father judges no one, but he has given all judgement to the Son,
en_uk_webbe~JOH~C05~23~that all may honour the Son, even as they honour the Father. He who doesn’t honour the Son doesn’t honour the Father who sent him.
en_uk_webbe~JOH~C05~24~“Most certainly I tell you, he who hears my word and believes him who sent me has eternal life, and doesn’t come into judgement, but has passed out of death into life.
en_uk_webbe~JOH~C05~25~Most certainly I tell you, the hour comes, and now is, when the dead will hear the Son of God’s voice; and those who hear will live.
en_uk_webbe~JOH~C05~26~For as the Father has life in himself, even so he gave to the Son also to have life in himself.
en_uk_webbe~JOH~C05~27~He also gave him authority to execute judgement, because he is a son of man.
en_uk_webbe~JOH~C05~28~Don’t marvel at this, for the hour comes in which all who are in the tombs will hear his voice,
en_uk_webbe~JOH~C05~29~and will come out; those who have done good, to the resurrection of life; and those who have done evil, to the resurrection of judgement.
en_uk_webbe~JOH~C05~30~I can of myself do nothing. As I hear, I judge, and my judgement is righteous; because I don’t seek my own will, but the will of my Father who sent me.
en_uk_webbe~JOH~C05~31~“If I testify about myself, my witness is not valid.
en_uk_webbe~JOH~C05~32~It is another who testifies about me. I know that the testimony which he testifies about me is true.
en_uk_webbe~JOH~C05~33~You have sent to John, and he has testified to the truth.
en_uk_webbe~JOH~C05~34~But the testimony which I receive is not from man. However, I say these things that you may be saved.
en_uk_webbe~JOH~C05~35~He was the burning and shining lamp, and you were willing to rejoice for a while in his light.
en_uk_webbe~JOH~C05~36~But the testimony which I have is greater than that of John, for the works which the Father gave me to accomplish, the very works that I do, testify about me, that the Father has sent me.
en_uk_webbe~JOH~C05~37~The Father himself, who sent me, has testified about me. You have neither heard his voice at any time, nor seen his form.
en_uk_webbe~JOH~C05~38~You don’t have his word living in you, because you don’t believe him whom he sent.
en_uk_webbe~JOH~C05~39~“You search the Scriptures, because you think that in them you have eternal life; and these are they which testify about me.
en_uk_webbe~JOH~C05~40~Yet you will not come to me, that you may have life.
en_uk_webbe~JOH~C05~41~I don’t receive glory from men.
en_uk_webbe~JOH~C05~42~But I know you, that you don’t have God’s love in yourselves.
en_uk_webbe~JOH~C05~43~I have come in my Father’s name, and you don’t receive me. If another comes in his own name, you will receive him.
en_uk_webbe~JOH~C05~44~How can you believe, who receive glory from one another, and you don’t seek the glory that comes from the only God?
en_uk_webbe~JOH~C05~45~“Don’t think that I will accuse you to the Father. There is one who accuses you, even Moses, on whom you have set your hope.
en_uk_webbe~JOH~C05~46~For if you believed Moses, you would believe me; for he wrote about me.
en_uk_webbe~JOH~C05~47~But if you don’t believe his writings, how will you believe my words?”
