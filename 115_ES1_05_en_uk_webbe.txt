en_uk_webbe~ES1~C05~01~After this were the chiefs of fathers’ houses chosen to go up according to their tribes, with their wives and sons and daughters, with their menservants and maidservants, and their cattle.
en_uk_webbe~ES1~C05~02~And Darius sent with them a thousand horsemen, till they had brought them back to Jerusalem safely, and with musical instruments, tabrets and flutes.
en_uk_webbe~ES1~C05~03~And all their kindred played, and he made them go up together with them.
en_uk_webbe~ES1~C05~04~And these are the names of the men which went up, according to their families amongst their tribes, after their several divisions.
en_uk_webbe~ES1~C05~05~The priests, the sons of Phinees, the sons of Aaron: Jesus the son of Josedek, the son of Saraias, and Joakim the son of Zorobabel, the son of Salathiel, of the house of David, of the lineage of Phares, of the tribe of Judah;
en_uk_webbe~ES1~C05~06~who spoke wise sentences before Darius the king of Persia in the second year of his reign, in the month Nisan, which is the first month.
en_uk_webbe~ES1~C05~07~And these are they of Jewry that came up from the captivity, where they lived as strangers, whom Nabuchodonosor the king of Babylon had carried away to Babylon.
en_uk_webbe~ES1~C05~08~And they returned to Jerusalem, and to the other parts of Jewry, every man to his own city, who came with Zorobabel, with Jesus, Nehemias, [and] Zaraias, Resaias, Eneneus, Mardocheus, Beelsarus, Aspharsus, Reelias, Roimus, [and] Baana, their leaders.
en_uk_webbe~ES1~C05~09~The number of them of the nation, and their leaders: the sons of Phoros, two thousand a hundred and seventy and two: the sons of Saphat, four hundred and seventy and two:
en_uk_webbe~ES1~C05~10~the sons of Ares, seven hundred and fifty and six:
en_uk_webbe~ES1~C05~11~the sons of Phaath Moab, of the sons of Jesus and Joab, two thousand and eight hundred and twelve:
en_uk_webbe~ES1~C05~12~the sons of Elam, a thousand and two hundred and fifty and four: the sons of Zathui, nine hundred and forty and five: the sons of Chorbe, seven hundred and five: the sons of Bani, six hundred and forty and eight:
en_uk_webbe~ES1~C05~13~the sons of Bebai, six hundred and twenty and three: the sons of Astad, a thousand and three hundred and twenty and two:
en_uk_webbe~ES1~C05~14~the sons of Adonikam, six hundred and sixty and seven: the sons of Bagoi, two thousand and sixty and six: the sons of Adinu, four hundred and fifty and four:
en_uk_webbe~ES1~C05~15~the sons of Ater, of Ezekias, ninety and two: the sons of Kilan and Azetas, three score and seven: the sons of Azaru, four hundred and thirty and two:
en_uk_webbe~ES1~C05~16~the sons of Annis, a hundred and one: the sons of Arom: the sons of Bassai, three hundred and twenty and three: the sons of Arsiphurith, a hundred and twelve:
en_uk_webbe~ES1~C05~17~the sons of Baiterus, three thousand and five: the sons of Bethlomon, a hundred and twenty and three:
en_uk_webbe~ES1~C05~18~they of Netophas, fifty and five: they of Anathoth, a hundred and fifty and eight: they of Bethasmoth, forty and two:
en_uk_webbe~ES1~C05~19~they of or Kariathiarius, twenty and five: they of Caphira and Beroth, seven hundred and forty and three:
en_uk_webbe~ES1~C05~20~the Chadiasai and Ammidioi, four hundred and twenty and two: they of Kirama and Gabbe, six hundred and twenty and one:
en_uk_webbe~ES1~C05~21~they of Macalon, a hundred and twenty and two: they of Betolion, fifty and two: the sons of Niphis, a hundred and fifty and six:
en_uk_webbe~ES1~C05~22~the sons of Calamolalus and Onus, seven hundred and twenty and five: the sons of Jerechu, three hundred and forty and five:
en_uk_webbe~ES1~C05~23~the sons of Sanaas, three thousand and three hundred and thirty.
en_uk_webbe~ES1~C05~24~The priests: the sons of Jeddu, the son of Jesus, amongst the sons of Sanasib, nine hundred and seventy and two: the sons of Emmeruth, a thousand and fifty and two:
en_uk_webbe~ES1~C05~25~the sons of Phassurus, a thousand and two hundred and forty and seven: the sons of Charme, a thousand and seventeen.
en_uk_webbe~ES1~C05~26~The Levites: the sons of Jesus, and Kadmiel, and Bannas, and Sudias, seventy and four.
en_uk_webbe~ES1~C05~27~The holy singers: the sons of Asaph, a hundred twenty and eight.
en_uk_webbe~ES1~C05~28~The gatekeepers: the sons of Salum, the sons of Atar, the sons of Tolman, the sons of Dacubi, the sons of Ateta, the sons of Sabi, in all a hundred and thirty and nine.
en_uk_webbe~ES1~C05~29~The temple servants: the sons of Esau, the sons of Asipha, the sons of Tabaoth, the sons of Keras, the sons of Sua, the sons of Phaleas, the sons of Labana, the sons of Aggaba.
en_uk_webbe~ES1~C05~30~the sons of Acud, the sons of Uta, the sons of Ketab, the sons of Accaba, the sons of Subai, the sons of Anan, the sons of Cathua, the sons of Geddur,
en_uk_webbe~ES1~C05~31~the sons of Jairus, the sons of Daisan, the sons of Noeba, the sons of Chaseba, the sons of Gazera, the sons of Ozias, the sons of Phinoe, the sons of Asara, the sons of Basthai, the sons of Asana, the sons of Maani, the sons of Naphisi, the sons of Acub, the sons of Achipha, the sons of Asur, the sons of Pharakim, the sons of Basaloth,
en_uk_webbe~ES1~C05~32~the sons of Meedda, the sons of Cutha, the sons of Charea, the sons of Barchus, the sons of Serar, the sons of Thomei, the sons of Nasi, the sons of Atipha.
en_uk_webbe~ES1~C05~33~The sons of the servants of Solomon: the sons of Assaphioth, the sons of Pharida, the sons of Jeeli, the sons of Lozon, the sons of Isdael, the sons of Saphuthi,
en_uk_webbe~ES1~C05~34~the sons of Agia, the sons of Phacareth, the sons of Sabie, the sons of Sarothie, the sons of Masias, the sons of Gas, the sons of Addus, the sons of Subas, the sons of Apherra, the sons of Barodis, the sons of Saphat, the sons of Allon.
en_uk_webbe~ES1~C05~35~All the temple-servants, and the sons of the servants of Solomon, were three hundred and seventy and two.
en_uk_webbe~ES1~C05~36~These came up from Thermeleth, and Thelersas, Charaathalan leading them, and Allar;
en_uk_webbe~ES1~C05~37~and they could not show their families, nor their stock, how they were of Israel: the sons of Dalan the son of Ban, the sons of Nekodan, six hundred and fifty and two.
en_uk_webbe~ES1~C05~38~And of the priests, those who usurped the office of the priesthood and were not found: the sons of Obdia, the sons of Akkos, the sons of Jaddus, who married Augia one of the daughters of Zorzelleus, and was called after his name.
en_uk_webbe~ES1~C05~39~And when the description of the kindred of these men was sought in the register, and was not found, they were removed from executing the office of the priesthood:
en_uk_webbe~ES1~C05~40~for to them said Nehemias and Attharias, that they should not be partakers of the holy things, till there arose up a high priest wearing Urim and Thummim.
en_uk_webbe~ES1~C05~41~So all they of Israel, from twelve years old [and upward,] beside menservants and women servants, were [in number] forty and two thousand and three hundred and sixty.
en_uk_webbe~ES1~C05~42~Their menservants and handmaids were seven thousand and three hundred and thirty and seven: the minstrels and singers, two hundred and forty and five:
en_uk_webbe~ES1~C05~43~four hundred and thirty and five camels, seven thousand and thirty and six horses, two hundred and forty and five mules, five thousand and five hundred and twenty and five beasts of burden.
en_uk_webbe~ES1~C05~44~And certain of the chief men of their families, when they came to the temple of God that is in Jerusalem, vowed to set up the house again in its own place according to their ability,
en_uk_webbe~ES1~C05~45~and to give into the holy treasury of the works a thousand pounds of gold, five thousand of silver, and a hundred priestly vestments.
en_uk_webbe~ES1~C05~46~And the priests and the Levites and those who were of the people lived in Jerusalem and the country; the holy singers also and the gatekeepers and all Israel in their villages.
en_uk_webbe~ES1~C05~47~But when the seventh month was at hand, and when the children of Israel were every man in his own place, they came all together with one consent into the broad place before the first porch which is towards the east.
en_uk_webbe~ES1~C05~48~Then stood up Jesus the son of Josedek, and his kindred the priests, and Zorobabel the son of Salathiel, and his kindred, and made ready the altar of the God of Israel,
en_uk_webbe~ES1~C05~49~to offer burnt sacrifices upon it, according as it is expressly commanded in the book of Moses the man of God.
en_uk_webbe~ES1~C05~50~And certain were gathered to them out of the other nations of the land, and they erected the altar upon its own place, because all the nations of the land were at enmity with them, and oppressed them; and they offered sacrifices according to the time, and burnt offerings to the Lord both morning and evening.
en_uk_webbe~ES1~C05~51~Also they held the feast of tabernacles, as it is commanded in the law, and [offered] sacrifices daily, as was meet:
en_uk_webbe~ES1~C05~52~and after that, the continual oblations, and the sacrifices of the Sabbaths, and of the new moons, and of all the consecrated feasts.
en_uk_webbe~ES1~C05~53~And all those who had made any vow to God began to offer sacrifices to God from the new moon of the seventh month, although the temple of God was not yet built.
en_uk_webbe~ES1~C05~54~And they gave money to the masons and carpenters; and meat and drink,
en_uk_webbe~ES1~C05~55~and cars to them of Sidon and Tyre, that they should bring cedar trees from Libanus, [and] convey them in floats to the haven of Joppa, according to the commandment which was written for them by Cyrus king of the Persians.
en_uk_webbe~ES1~C05~56~And in the second year after his coming to the temple of God at Jerusalem, in the second month, began Zorobabel the son of Salathiel, and Jesus the son of Josedek, and their kindred, and the Levitical priests, and all those who were come to Jerusalem out of the captivity:
en_uk_webbe~ES1~C05~57~and they laid the foundation of the temple of God on the new moon of the second month, in the second year after they were come to Jewry and Jerusalem.
en_uk_webbe~ES1~C05~58~And they appointed the Levites from twenty years old over the works of the Lord. Then stood up Jesus, and his sons and kindred, and Kadmiel his brother, and the sons of Jesus, Emadabun, and the sons of Joda the son of Iliadun, and their sons and kindred, all the Levites, with one accord started the business, labouring to advance the works in the house of God. So the builders built the temple of the Lord.
en_uk_webbe~ES1~C05~59~And the priests stood arrayed in their vestments with musical instruments and trumpets, and the Levites the sons of Asaph with their cymbals,
en_uk_webbe~ES1~C05~60~singing songs of thanksgiving, and praising the Lord, after the order of David king of Israel.
en_uk_webbe~ES1~C05~61~And they sang aloud, praising the Lord in songs of thanksgiving, because his goodness and his glory are forever in all Israel.
en_uk_webbe~ES1~C05~62~And all the people sounded trumpets, and shouted with a loud voice, singing songs of thanksgiving to the Lord for the rearing up of the house of the Lord.
en_uk_webbe~ES1~C05~63~Also of the Levitical priests, and of the heads of their families, the ancients who had seen the former house came to the building of this with lamentation and great weeping.
en_uk_webbe~ES1~C05~64~But many with trumpets and joy [shouted] with loud voice,
en_uk_webbe~ES1~C05~65~insomuch that the people heard not the trumpets for the weeping of the people: for the multitude sounded marvellously, so that it was heard afar off.
en_uk_webbe~ES1~C05~66~Wherefore when the enemies of the tribe of Judah and Benjamin heard it, they came to know what that noise of trumpets should mean.
en_uk_webbe~ES1~C05~67~And they perceived that those who were of the captivity did build the temple to the Lord, the God of Israel.
en_uk_webbe~ES1~C05~68~So they went to zorobabel and Jesus, and to the chief men of the families, and said to them, we will build together with you.
en_uk_webbe~ES1~C05~69~For we likewise, as you°, do obey your Lord, and do sacrifice to him from the days of Asbasareth the king of the Assyrians, who brought us here.
en_uk_webbe~ES1~C05~70~Then Zorobabel and Jesus and the chief men of the families of Israel said to them, It is not for you to build the house to the Lord our God.
en_uk_webbe~ES1~C05~71~We ourselves alone will build to the Lord of Israel, according as Cyrus the king of the Persians has commanded us.
en_uk_webbe~ES1~C05~72~But the heathen of the land lying heavy upon the inhabitants of Judea, and holding them strait, hindered their building;
en_uk_webbe~ES1~C05~73~and by their secret plots, and popular persuasions and commotions, they hindered the finishing of the building all the time that King Cyrus lived: so they were hindered from building for the space of two years, until the reign of Darius.
