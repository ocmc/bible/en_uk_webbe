en_uk_webbe~MAR~C15~01~Immediately in the morning the chief priests, with the elders and scribes, and the whole council, held a consultation, bound Jesus, carried him away, and delivered him up to Pilate.
en_uk_webbe~MAR~C15~02~Pilate asked him, “Are you the King of the Jews?” He answered, “So you say.”
en_uk_webbe~MAR~C15~03~The chief priests accused him of many things.
en_uk_webbe~MAR~C15~04~Pilate again asked him, “Have you no answer? See how many things they testify against you!”
en_uk_webbe~MAR~C15~05~But Jesus made no further answer, so that Pilate marvelled.
en_uk_webbe~MAR~C15~06~Now at the feast he used to release to them one prisoner, whom they asked of him.
en_uk_webbe~MAR~C15~07~There was one called Barabbas, bound with his fellow insurgents, men who in the insurrection had committed murder.
en_uk_webbe~MAR~C15~08~The multitude, crying aloud, began to ask him to do as he always did for them.
en_uk_webbe~MAR~C15~09~Pilate answered them, saying, “Do you want me to release to you the King of the Jews?”
en_uk_webbe~MAR~C15~10~For he perceived that for envy the chief priests had delivered him up.
en_uk_webbe~MAR~C15~11~But the chief priests stirred up the multitude, that he should release Barabbas to them instead.
en_uk_webbe~MAR~C15~12~Pilate again asked them, “What then should I do to him whom you call the King of the Jews?”
en_uk_webbe~MAR~C15~13~They cried out again, “Crucify him!”
en_uk_webbe~MAR~C15~14~Pilate said to them, “Why, what evil has he done?” But they cried out exceedingly, “Crucify him!”
en_uk_webbe~MAR~C15~15~Pilate, wishing to please the multitude, released Barabbas to them, and handed over Jesus, when he had flogged him, to be crucified.
en_uk_webbe~MAR~C15~16~The soldiers led him away within the court, which is the Praetorium; and they called together the whole cohort.
en_uk_webbe~MAR~C15~17~They clothed him with purple, and weaving a crown of thorns, they put it on him.
en_uk_webbe~MAR~C15~18~They began to salute him, “Hail, King of the Jews!”
en_uk_webbe~MAR~C15~19~They struck his head with a reed, and spat on him, and bowing their knees, did homage to him.
en_uk_webbe~MAR~C15~20~When they had mocked him, they took the purple off him, and put his own garments on him. They led him out to crucify him.
en_uk_webbe~MAR~C15~21~They compelled one passing by, coming from the country, Simon of Cyrene, the father of Alexander and Rufus, to go with them, that he might bear his cross.
en_uk_webbe~MAR~C15~22~They brought him to the place called Golgotha, which is, being interpreted, “The place of a skull.”
en_uk_webbe~MAR~C15~23~They offered him wine mixed with myrrh to drink, but he didn’t take it.
en_uk_webbe~MAR~C15~24~Crucifying him, they parted his garments amongst them, casting lots on them, what each should take.
en_uk_webbe~MAR~C15~25~It was the third hour, and they crucified him.
en_uk_webbe~MAR~C15~26~The superscription of his accusation was written over him, “THE KING OF THE JEWS.”
en_uk_webbe~MAR~C15~27~With him they crucified two robbers; one on his right hand, and one on his left.
en_uk_webbe~MAR~C15~28~The Scripture was fulfilled, which says, “He was counted with transgressors.”
en_uk_webbe~MAR~C15~29~Those who passed by blasphemed him, wagging their heads, and saying, “Ha! You who destroy the temple, and build it in three days,
en_uk_webbe~MAR~C15~30~save yourself, and come down from the cross!”
en_uk_webbe~MAR~C15~31~Likewise, also the chief priests mocking amongst themselves with the scribes said, “He saved others. He can’t save himself.
en_uk_webbe~MAR~C15~32~Let the Christ, the King of Israel, now come down from the cross, that we may see and believe him.” Those who were crucified with him also insulted him.
en_uk_webbe~MAR~C15~33~When the sixth hour had come, there was darkness over the whole land until the ninth hour.
en_uk_webbe~MAR~C15~34~At the ninth hour Jesus cried with a loud voice, saying, “Eloi, Eloi, lama sabachthani?” which is, being interpreted, “My God, my God, why have you forsaken me?”
en_uk_webbe~MAR~C15~35~Some of those who stood by, when they heard it, said, “Behold, he is calling Elijah.”
en_uk_webbe~MAR~C15~36~One ran, and filling a sponge full of vinegar, put it on a reed, and gave it to him to drink, saying, “Let him be. Let’s see whether Elijah comes to take him down.”
en_uk_webbe~MAR~C15~37~Jesus cried out with a loud voice, and gave up the spirit.
en_uk_webbe~MAR~C15~38~The veil of the temple was torn in two from the top to the bottom.
en_uk_webbe~MAR~C15~39~When the centurion, who stood by opposite him, saw that he cried out like this and breathed his last, he said, “Truly this man was the Son of God!”
en_uk_webbe~MAR~C15~40~There were also women watching from afar, amongst whom were both Mary Magdalene, and Mary the mother of James the less and of Joses, and Salome;
en_uk_webbe~MAR~C15~41~who, when he was in Galilee, followed him and served him; and many other women who came up with him to Jerusalem.
en_uk_webbe~MAR~C15~42~When evening had now come, because it was the Preparation Day, that is, the day before the Sabbath,
en_uk_webbe~MAR~C15~43~Joseph of Arimathaea, a prominent council member who also himself was looking for God’s Kingdom, came. He boldly went in to Pilate, and asked for Jesus’ body.
en_uk_webbe~MAR~C15~44~Pilate marvelled if he were already dead; and summoning the centurion, he asked him whether he had been dead long.
en_uk_webbe~MAR~C15~45~When he found out from the centurion, he granted the body to Joseph.
en_uk_webbe~MAR~C15~46~He bought a linen cloth, and taking him down, wound him in the linen cloth, and laid him in a tomb which had been cut out of a rock. He rolled a stone against the door of the tomb.
en_uk_webbe~MAR~C15~47~Mary Magdalene and Mary, the mother of Joses, saw where he was laid.
