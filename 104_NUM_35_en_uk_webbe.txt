en_uk_webbe~NUM~C35~01~The LORD spoke to Moses in the plains of Moab by the Jordan at Jericho, saying,
en_uk_webbe~NUM~C35~02~“Command the children of Israel to give to the Levites cities to dwell in out of their inheritance. You shall give pasture lands for the cities around them to the Levites.
en_uk_webbe~NUM~C35~03~They shall have the cities to dwell in. Their pasture lands shall be for their livestock, and for their possessions, and for all their animals.
en_uk_webbe~NUM~C35~04~“The pasture lands of the cities, which you shall give to the Levites, shall be from the wall of the city and outward one thousand cubits around it.
en_uk_webbe~NUM~C35~05~You shall measure outside of the city for the east side two thousand cubits, and for the south side two thousand cubits, and for the west side two thousand cubits, and for the north side two thousand cubits, the city being in the middle. This shall be the pasture lands of their cities.
en_uk_webbe~NUM~C35~06~“The cities which you shall give to the Levites, they shall be the six cities of refuge, which you shall give for the man slayer to flee to. Besides them you shall give forty-two cities.
en_uk_webbe~NUM~C35~07~All the cities which you shall give to the Levites shall be forty-eight cities together with their pasture lands.
en_uk_webbe~NUM~C35~08~Concerning the cities which you shall give of the possession of the children of Israel, from the many you shall take many, and from the few you shall take few. Everyone according to his inheritance which he inherits shall give some of his cities to the Levites.”
en_uk_webbe~NUM~C35~09~The LORD spoke to Moses, saying,
en_uk_webbe~NUM~C35~10~“Speak to the children of Israel, and tell them, ‘When you pass over the Jordan into the land of Canaan,
en_uk_webbe~NUM~C35~11~then you shall appoint for yourselves cities to be cities of refuge for you, that the man slayer who kills any person unwittingly may flee there.
en_uk_webbe~NUM~C35~12~The cities shall be for your refuge from the avenger, that the man slayer not die until he stands before the congregation for judgement.
en_uk_webbe~NUM~C35~13~The cities which you shall give shall be for you six cities of refuge.
en_uk_webbe~NUM~C35~14~You shall give three cities beyond the Jordan, and you shall give three cities in the land of Canaan. They shall be cities of refuge.
en_uk_webbe~NUM~C35~15~For the children of Israel, and for the stranger and for the foreigner living amongst them, shall these six cities be for refuge, that everyone who kills any person unwittingly may flee there.
en_uk_webbe~NUM~C35~16~“‘But if he struck him with an instrument of iron, so that he died, he is a murderer. The murderer shall surely be put to death.
en_uk_webbe~NUM~C35~17~If he struck him with a stone in the hand, by which a man may die, and he died, he is a murderer. The murderer shall surely be put to death.
en_uk_webbe~NUM~C35~18~Or if he struck him with a weapon of wood in the hand, by which a man may die, and he died, he is a murderer. The murderer shall surely be put to death.
en_uk_webbe~NUM~C35~19~The avenger of blood shall himself put the murderer to death. When he meets him, he shall put him to death.
en_uk_webbe~NUM~C35~20~If he shoved him out of hatred, or hurled something at him while lying in wait, so that he died,
en_uk_webbe~NUM~C35~21~or in hostility struck him with his hand, so that he died, he who struck him shall surely be put to death. He is a murderer. The avenger of blood shall put the murderer to death when he meets him.
en_uk_webbe~NUM~C35~22~“‘But if he shoved him suddenly without hostility, or hurled on him anything without lying in wait,
en_uk_webbe~NUM~C35~23~or with any stone, by which a man may die, not seeing him, and cast it on him so that he died, and he was not his enemy and not seeking his harm,
en_uk_webbe~NUM~C35~24~then the congregation shall judge between the striker and the avenger of blood according to these ordinances.
en_uk_webbe~NUM~C35~25~The congregation shall deliver the man slayer out of the hand of the avenger of blood, and the congregation shall restore him to his city of refuge, where he had fled. He shall dwell therein until the death of the high priest, who was anointed with the holy oil.
en_uk_webbe~NUM~C35~26~“‘But if the man slayer shall at any time go beyond the border of his city of refuge where he flees,
en_uk_webbe~NUM~C35~27~and the avenger of blood finds him outside of the border of his city of refuge, and the avenger of blood kills the man slayer, he shall not be guilty of blood,
en_uk_webbe~NUM~C35~28~because he should have remained in his city of refuge until the death of the high priest. But after the death of the high priest, the man slayer shall return into the land of his possession.
en_uk_webbe~NUM~C35~29~“‘These things shall be for a statute and ordinance to you throughout your generations in all your dwellings.
en_uk_webbe~NUM~C35~30~“‘Whoever kills any person, the murderer shall be slain based on the testimony of witnesses; but one witness shall not testify alone against any person so that he dies.
en_uk_webbe~NUM~C35~31~“‘Moreover you shall take no ransom for the life of a murderer who is guilty of death. He shall surely be put to death.
en_uk_webbe~NUM~C35~32~“‘You shall take no ransom for him who has fled to his city of refuge, that he may come again to dwell in the land before the death of the priest.
en_uk_webbe~NUM~C35~33~“‘So you shall not pollute the land where you live; for blood pollutes the land. No atonement can be made for the land, for the blood that is shed in it, but by the blood of him who shed it.
en_uk_webbe~NUM~C35~34~You shall not defile the land which you inhabit, where I dwell; for I, the LORD, dwell amongst the children of Israel.’”
