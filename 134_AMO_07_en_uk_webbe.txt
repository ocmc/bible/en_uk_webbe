en_uk_webbe~AMO~C07~01~Thus the Lord GOD showed me: and behold, he formed locusts in the beginning of the shooting up of the latter growth; and behold, it was the latter growth after the king’s harvest.
en_uk_webbe~AMO~C07~02~When they finished eating the grass of the land, then I said, “Lord GOD, forgive, I beg you! How could Jacob stand? For he is small.”
en_uk_webbe~AMO~C07~03~The LORD relented concerning this. “It shall not be,” says the LORD.
en_uk_webbe~AMO~C07~04~Thus the Lord GOD showed me and behold, the Lord GOD called for judgement by fire; and it dried up the great deep, and would have devoured the land.
en_uk_webbe~AMO~C07~05~Then I said, “Lord GOD, stop, I beg you! How could Jacob stand? For he is small.”
en_uk_webbe~AMO~C07~06~The LORD relented concerning this. “This also shall not be,” says the Lord GOD.
en_uk_webbe~AMO~C07~07~Thus he showed me and behold, the Lord stood beside a wall made by a plumb line, with a plumb line in his hand.
en_uk_webbe~AMO~C07~08~The LORD said to me, “Amos, what do you see?” I said, “A plumb line.” Then the Lord said, “Behold, I will set a plumb line in the middle of my people Israel. I will not again pass by them any more.
en_uk_webbe~AMO~C07~09~The high places of Isaac will be desolate, the sanctuaries of Israel will be laid waste; and I will rise against the house of Jeroboam with the sword.”
en_uk_webbe~AMO~C07~10~Then Amaziah the priest of Bethel sent to Jeroboam king of Israel, saying, “Amos has conspired against you in the middle of the house of Israel. The land is not able to bear all his words.
en_uk_webbe~AMO~C07~11~For Amos says, ‘Jeroboam will die by the sword, and Israel shall surely be led away captive out of his land.’”
en_uk_webbe~AMO~C07~12~Amaziah also said to Amos, “You seer, go, flee away into the land of Judah, and there eat bread, and prophesy there:
en_uk_webbe~AMO~C07~13~but don’t prophesy again any more at Bethel; for it is the king’s sanctuary, and it is a royal house!”
en_uk_webbe~AMO~C07~14~Then Amos answered Amaziah, “I was no prophet, neither was I a prophet’s son; but I was a herdsman, and a farmer of sycamore figs;
en_uk_webbe~AMO~C07~15~and the LORD took me from following the flock, and the LORD said to me, ‘Go, prophesy to my people Israel.’
en_uk_webbe~AMO~C07~16~Now therefore listen to the LORD’s word: ‘You say, Don’t prophesy against Israel, and don’t preach against the house of Isaac.’
en_uk_webbe~AMO~C07~17~Therefore the LORD says: ‘Your wife shall be a prostitute in the city, and your sons and your daughters shall fall by the sword, and your land shall be divided by line; and you yourself shall die in a land that is unclean, and Israel shall surely be led away captive out of his land.’”
