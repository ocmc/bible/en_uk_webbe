en_uk_webbe~MAT~C14~01~At that time, Herod the tetrarch heard the report concerning Jesus,
en_uk_webbe~MAT~C14~02~and said to his servants, “This is John the Baptiser. He is risen from the dead. That is why these powers work in him.”
en_uk_webbe~MAT~C14~03~For Herod had arrested John, and bound him, and put him in prison for the sake of Herodias, his brother Philip’s wife.
en_uk_webbe~MAT~C14~04~For John said to him, “It is not lawful for you to have her.”
en_uk_webbe~MAT~C14~05~When he would have put him to death, he feared the multitude, because they counted him as a prophet.
en_uk_webbe~MAT~C14~06~But when Herod’s birthday came, the daughter of Herodias danced amongst them and pleased Herod.
en_uk_webbe~MAT~C14~07~Whereupon he promised with an oath to give her whatever she should ask.
en_uk_webbe~MAT~C14~08~She, being prompted by her mother, said, “Give me here on a platter the head of John the Baptiser.”
en_uk_webbe~MAT~C14~09~The king was grieved, but for the sake of his oaths, and of those who sat at the table with him, he commanded it to be given,
en_uk_webbe~MAT~C14~10~and he sent and beheaded John in the prison.
en_uk_webbe~MAT~C14~11~His head was brought on a platter, and given to the young lady; and she brought it to her mother.
en_uk_webbe~MAT~C14~12~His disciples came, and took the body, and buried it. Then they went and told Jesus.
en_uk_webbe~MAT~C14~13~Now when Jesus heard this, he withdrew from there in a boat, to a deserted place apart. When the multitudes heard it, they followed him on foot from the cities.
en_uk_webbe~MAT~C14~14~Jesus went out, and he saw a great multitude. He had compassion on them, and healed their sick.
en_uk_webbe~MAT~C14~15~When evening had come, his disciples came to him, saying, “This place is deserted, and the hour is already late. Send the multitudes away, that they may go into the villages, and buy themselves food.”
en_uk_webbe~MAT~C14~16~But Jesus said to them, “They don’t need to go away. You give them something to eat.”
en_uk_webbe~MAT~C14~17~They told him, “We only have here five loaves and two fish.”
en_uk_webbe~MAT~C14~18~He said, “Bring them here to me.”
en_uk_webbe~MAT~C14~19~He commanded the multitudes to sit down on the grass; and he took the five loaves and the two fish, and looking up to heaven, he blessed, broke and gave the loaves to the disciples, and the disciples gave to the multitudes.
en_uk_webbe~MAT~C14~20~They all ate, and were filled. They took up twelve baskets full of that which remained left over from the broken pieces.
en_uk_webbe~MAT~C14~21~Those who ate were about five thousand men, in addition to women and children.
en_uk_webbe~MAT~C14~22~Immediately Jesus made the disciples get into the boat, and to go ahead of him to the other side, while he sent the multitudes away.
en_uk_webbe~MAT~C14~23~After he had sent the multitudes away, he went up into the mountain by himself to pray. When evening had come, he was there alone.
en_uk_webbe~MAT~C14~24~But the boat was now in the middle of the sea, distressed by the waves, for the wind was contrary.
en_uk_webbe~MAT~C14~25~In the fourth watch of the night, Jesus came to them, walking on the sea.
en_uk_webbe~MAT~C14~26~When the disciples saw him walking on the sea, they were troubled, saying, “It’s a ghost!” and they cried out for fear.
en_uk_webbe~MAT~C14~27~But immediately Jesus spoke to them, saying, “Cheer up! It is I! Don’t be afraid.”
en_uk_webbe~MAT~C14~28~Peter answered him and said, “Lord, if it is you, command me to come to you on the waters.”
en_uk_webbe~MAT~C14~29~He said, “Come!” Peter stepped down from the boat, and walked on the waters to come to Jesus.
en_uk_webbe~MAT~C14~30~But when he saw that the wind was strong, he was afraid, and beginning to sink, he cried out, saying, “Lord, save me!”
en_uk_webbe~MAT~C14~31~Immediately Jesus stretched out his hand, took hold of him, and said to him, “You of little faith, why did you doubt?”
en_uk_webbe~MAT~C14~32~When they got up into the boat, the wind ceased.
en_uk_webbe~MAT~C14~33~Those who were in the boat came and worshipped him, saying, “You are truly the Son of God!”
en_uk_webbe~MAT~C14~34~When they had crossed over, they came to the land of Gennesaret.
en_uk_webbe~MAT~C14~35~When the people of that place recognised him, they sent into all that surrounding region, and brought to him all who were sick;
en_uk_webbe~MAT~C14~36~and they begged him that they might just touch the fringe of his garment. As many as touched it were made whole.
