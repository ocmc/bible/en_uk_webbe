en_uk_webbe~JOB~C20~01~Then Zophar the Naamathite answered,
en_uk_webbe~JOB~C20~02~“Therefore my thoughts answer me, even by reason of my haste that is in me.
en_uk_webbe~JOB~C20~03~I have heard the reproof which puts me to shame. The spirit of my understanding answers me.
en_uk_webbe~JOB~C20~04~Don’t you know this from old time, since man was placed on earth,
en_uk_webbe~JOB~C20~05~that the triumphing of the wicked is short, the joy of the godless but for a moment?
en_uk_webbe~JOB~C20~06~Though his height mount up to the heavens, and his head reach to the clouds,
en_uk_webbe~JOB~C20~07~yet he will perish forever like his own dung. Those who have seen him will say, ‘Where is he?’
en_uk_webbe~JOB~C20~08~He will fly away as a dream, and will not be found. Yes, he will be chased away like a vision of the night.
en_uk_webbe~JOB~C20~09~The eye which saw him will see him no more, neither will his place see him any more.
en_uk_webbe~JOB~C20~10~His children will seek the favour of the poor. His hands will give back his wealth.
en_uk_webbe~JOB~C20~11~His bones are full of his youth, but youth will lie down with him in the dust.
en_uk_webbe~JOB~C20~12~“Though wickedness is sweet in his mouth, though he hide it under his tongue,
en_uk_webbe~JOB~C20~13~though he spare it, and will not let it go, but keep it still within his mouth;
en_uk_webbe~JOB~C20~14~yet his food in his bowels is turned. It is cobra venom within him.
en_uk_webbe~JOB~C20~15~He has swallowed down riches, and he will vomit them up again. God will cast them out of his belly.
en_uk_webbe~JOB~C20~16~He will suck cobra venom. The viper’s tongue will kill him.
en_uk_webbe~JOB~C20~17~He will not look at the rivers, the flowing streams of honey and butter.
en_uk_webbe~JOB~C20~18~He will restore that for which he laboured, and will not swallow it down. He will not rejoice according to the substance that he has gotten.
en_uk_webbe~JOB~C20~19~For he has oppressed and forsaken the poor. He has violently taken away a house, and he will not build it up.
en_uk_webbe~JOB~C20~20~“Because he knew no quietness within him, he will not save anything of that in which he delights.
en_uk_webbe~JOB~C20~21~There was nothing left that he didn’t devour, therefore his prosperity will not endure.
en_uk_webbe~JOB~C20~22~In the fullness of his sufficiency, distress will overtake him. The hand of everyone who is in misery will come on him.
en_uk_webbe~JOB~C20~23~When he is about to fill his belly, God will cast the fierceness of his wrath on him. It will rain on him while he is eating.
en_uk_webbe~JOB~C20~24~He will flee from the iron weapon. The bronze arrow will strike him through.
en_uk_webbe~JOB~C20~25~He draws it out, and it comes out of his body. Yes, the glittering point comes out of his liver. Terrors are on him.
en_uk_webbe~JOB~C20~26~All darkness is laid up for his treasures. An unfanned fire will devour him. It will consume that which is left in his tent.
en_uk_webbe~JOB~C20~27~The heavens will reveal his iniquity. The earth will rise up against him.
en_uk_webbe~JOB~C20~28~The increase of his house will depart. They will rush away in the day of his wrath.
en_uk_webbe~JOB~C20~29~This is the portion of a wicked man from God, the heritage appointed to him by God.”
