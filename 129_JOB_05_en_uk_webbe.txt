en_uk_webbe~JOB~C05~01~“Call now; is there any who will answer you? To which of the holy ones will you turn?
en_uk_webbe~JOB~C05~02~For resentment kills the foolish man, and jealousy kills the simple.
en_uk_webbe~JOB~C05~03~I have seen the foolish taking root, but suddenly I cursed his habitation.
en_uk_webbe~JOB~C05~04~His children are far from safety. They are crushed in the gate. Neither is there any to deliver them,
en_uk_webbe~JOB~C05~05~whose harvest the hungry eats up, and take it even out of the thorns. The snare gapes for their substance.
en_uk_webbe~JOB~C05~06~For affliction doesn’t come out of the dust, neither does trouble spring out of the ground;
en_uk_webbe~JOB~C05~07~but man is born to trouble, as the sparks fly upward.
en_uk_webbe~JOB~C05~08~“But as for me, I would seek God. I would commit my cause to God,
en_uk_webbe~JOB~C05~09~who does great things that can’t be fathomed, marvellous things without number;
en_uk_webbe~JOB~C05~10~who gives rain on the earth, and sends waters on the fields;
en_uk_webbe~JOB~C05~11~so that he sets up on high those who are low, those who mourn are exalted to safety.
en_uk_webbe~JOB~C05~12~He frustrates the plans of the crafty, So that their hands can’t perform their enterprise.
en_uk_webbe~JOB~C05~13~He takes the wise in their own craftiness; the counsel of the cunning is carried headlong.
en_uk_webbe~JOB~C05~14~They meet with darkness in the day time, and grope at noonday as in the night.
en_uk_webbe~JOB~C05~15~But he saves from the sword of their mouth, even the needy from the hand of the mighty.
en_uk_webbe~JOB~C05~16~So the poor has hope, and injustice shuts her mouth.
en_uk_webbe~JOB~C05~17~“Behold, happy is the man whom God corrects. Therefore do not despise the chastening of the Almighty.
en_uk_webbe~JOB~C05~18~For he wounds and binds up. He injures and his hands make whole.
en_uk_webbe~JOB~C05~19~He will deliver you in six troubles; yes, in seven no evil will touch you.
en_uk_webbe~JOB~C05~20~In famine he will redeem you from death; in war, from the power of the sword.
en_uk_webbe~JOB~C05~21~You will be hidden from the scourge of the tongue, neither will you be afraid of destruction when it comes.
en_uk_webbe~JOB~C05~22~You will laugh at destruction and famine, neither will you be afraid of the animals of the earth.
en_uk_webbe~JOB~C05~23~For you will be allied with the stones of the field. The animals of the field will be at peace with you.
en_uk_webbe~JOB~C05~24~You will know that your tent is in peace. You will visit your fold, and will miss nothing.
en_uk_webbe~JOB~C05~25~You will know also that your offspring will be great, Your offspring as the grass of the earth.
en_uk_webbe~JOB~C05~26~You will come to your grave in a full age, like a shock of grain comes in its season.
en_uk_webbe~JOB~C05~27~Look at this. We have searched it. It is so. Hear it, and know it for your good.”
