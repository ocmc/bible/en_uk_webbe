en_uk_webbe~SIR~C04~01~My son, don’t deprive the poor of his living. Don’t make the needy eyes wait long.
en_uk_webbe~SIR~C04~02~Don’t make a hungry soul sorrowful, or provoke a man in his distress.
en_uk_webbe~SIR~C04~03~Don’t add more trouble to a heart that is provoked. Don’t put off giving to him who is in need.
en_uk_webbe~SIR~C04~04~Don’t reject a suppliant in his affliction. Don’t turn your face away from a poor man.
en_uk_webbe~SIR~C04~05~Don’t turn your eye away from one who asks. Give no occasion to a man to curse you.
en_uk_webbe~SIR~C04~06~For if he curses you in the bitterness of his soul, he who made him will hear his supplication.
en_uk_webbe~SIR~C04~07~Endear yourself to the assembly. Bow your head to a great man.
en_uk_webbe~SIR~C04~08~Incline your ear to a poor man. Answer him with peaceful words in humility.
en_uk_webbe~SIR~C04~09~Deliver him who is wronged from the hand of him that wrongs him; Don’t be faint-hearted in giving judgement.
en_uk_webbe~SIR~C04~10~Be as a father to the fatherless, and like a husband to their mother. So you will be as a son of the Most High, and he will love you more than your mother does.
en_uk_webbe~SIR~C04~11~Wisdom exalts her sons, and takes hold of those who seek her.
en_uk_webbe~SIR~C04~12~He who loves her loves life. Those who seek to her early will be filled with gladness.
en_uk_webbe~SIR~C04~13~He who holds her fast will inherit glory. Where he enters, the Lord will bless.
en_uk_webbe~SIR~C04~14~Those who serve her minister to the Holy One. The Lord loves those who love her.
en_uk_webbe~SIR~C04~15~He who gives ear to her will judge the nations. He who heeds her will dwell securely.
en_uk_webbe~SIR~C04~16~If he trusts her, he will inherit her, and his generations will possess her.
en_uk_webbe~SIR~C04~17~For at the first she will walk with him in crooked ways, and will bring fear and dread upon him, and torment him with her discipline, until she may trust his soul, and try him by her judgements.
en_uk_webbe~SIR~C04~18~Then she will return him again to the straight way, and will gladden him, and reveal to him her secrets.
en_uk_webbe~SIR~C04~19~If he goes astray, she will forsake him, and hand him over to his fall.
en_uk_webbe~SIR~C04~20~Observe the opportunity, and beware of evil. Don’t be ashamed of your soul.
en_uk_webbe~SIR~C04~21~For there is a shame that brings sin, and there is a shame that is glory and grace.
en_uk_webbe~SIR~C04~22~Don’t show partiality against your soul. Don’t revere any man to your falling.
en_uk_webbe~SIR~C04~23~Don’t refrain from speaking when it is for safety. Don’t hide your wisdom for the sake of seeming fair.
en_uk_webbe~SIR~C04~24~For wisdom will be known by speech, and instruction by the word of the tongue.
en_uk_webbe~SIR~C04~25~Don’t speak against the truth and be shamed for your ignorance.
en_uk_webbe~SIR~C04~26~Don’t be ashamed to confess your sins. Don’t fight the river’s current.
en_uk_webbe~SIR~C04~27~Don’t lay yourself down for a fool to tread upon. Don’t be partial to one that is mighty.
en_uk_webbe~SIR~C04~28~Strive for the truth to death, and the Lord God will fight for you.
en_uk_webbe~SIR~C04~29~Don’t be rough hasty with your tongue, or slack and negligent in your deeds.
en_uk_webbe~SIR~C04~30~Don’t be like a lion in your house, or suspicious of your servants.
en_uk_webbe~SIR~C04~31~Don’t let your hand be stretched out to receive, and closed when you should repay.
