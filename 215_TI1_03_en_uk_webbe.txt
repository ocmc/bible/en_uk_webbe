en_uk_webbe~TI1~C03~01~This is a faithful saying: someone who seeks to be an overseer desires a good work.
en_uk_webbe~TI1~C03~02~The overseer therefore must be without reproach, the husband of one wife, temperate, sensible, modest, hospitable, good at teaching;
en_uk_webbe~TI1~C03~03~not a drinker, not violent, not greedy for money, but gentle, not quarrelsome, not covetous;
en_uk_webbe~TI1~C03~04~one who rules his own house well, having children in subjection with all reverence;
en_uk_webbe~TI1~C03~05~(but how could someone who doesn’t know how to rule one’s own house take care of God’s assembly?)
en_uk_webbe~TI1~C03~06~not a new convert, lest being puffed up he fall into the same condemnation as the devil.
en_uk_webbe~TI1~C03~07~Moreover he must have good testimony from those who are outside, to avoid falling into reproach and the snare of the devil.
en_uk_webbe~TI1~C03~08~Servants, in the same way, must be reverent, not double-tongued, not addicted to much wine, not greedy for money,
en_uk_webbe~TI1~C03~09~holding the mystery of the faith in a pure conscience.
en_uk_webbe~TI1~C03~10~Let them also first be tested; then let them serve if they are blameless.
en_uk_webbe~TI1~C03~11~Their wives in the same way must be reverent, not slanderers, temperate, and faithful in all things.
en_uk_webbe~TI1~C03~12~Let servants be husbands of one wife, ruling their children and their own houses well.
en_uk_webbe~TI1~C03~13~For those who have served well gain for themselves a good standing, and great boldness in the faith which is in Christ Jesus.
en_uk_webbe~TI1~C03~14~These things I write to you, hoping to come to you shortly;
en_uk_webbe~TI1~C03~15~but if I wait long, that you may know how men ought to behave themselves in God’s house, which is the assembly of the living God, the pillar and ground of the truth.
en_uk_webbe~TI1~C03~16~Without controversy, the mystery of godliness is great: God was revealed in the flesh, justified in the spirit, seen by angels, preached amongst the nations, believed on in the world, and received up in glory.
