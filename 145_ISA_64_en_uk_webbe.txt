en_uk_webbe~ISA~C64~01~Oh that you would tear the heavens, that you would come down, that the mountains might quake at your presence.
en_uk_webbe~ISA~C64~02~As when fire kindles the brushwood, and the fire causes the water to boil; Make your name known to your adversaries, that the nations may tremble at your presence!
en_uk_webbe~ISA~C64~03~When you did awesome things which we didn’t look for, you came down, and the mountains quaked at your presence.
en_uk_webbe~ISA~C64~04~For from of old men have not heard, nor perceived by the ear, nor has the eye seen a God besides you, who works for him who waits for him.
en_uk_webbe~ISA~C64~05~You meet him who rejoices and does righteousness, those who remember you in your ways. Behold, you were angry, and we sinned. We have been in sin for a long time. Shall we be saved?
en_uk_webbe~ISA~C64~06~For we have all become like one who is unclean, and all our righteousness is like a polluted garment. We all fade like a leaf; and our iniquities, like the wind, take us away.
en_uk_webbe~ISA~C64~07~There is no one who calls on your name, who stirs himself up to take hold of you; for you have hidden your face from us, and have consumed us by means of our iniquities.
en_uk_webbe~ISA~C64~08~But now, LORD, you are our Father. We are the clay and you our potter. We all are the work of your hand.
en_uk_webbe~ISA~C64~09~Don’t be furious, LORD. Don’t remember iniquity forever. Look and see, we beg you, we are all your people.
en_uk_webbe~ISA~C64~10~Your holy cities have become a wilderness. Zion has become a wilderness, Jerusalem a desolation.
en_uk_webbe~ISA~C64~11~Our holy and our beautiful house where our fathers praised you is burnt with fire. All our pleasant places are laid waste.
en_uk_webbe~ISA~C64~12~Will you hold yourself back for these things, LORD? Will you keep silent and punish us very severely?
