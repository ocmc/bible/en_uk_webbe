en_uk_webbe~ACT~C02~01~Now when the day of Pentecost had come, they were all with one accord in one place.
en_uk_webbe~ACT~C02~02~Suddenly there came from the sky a sound like the rushing of a mighty wind, and it filled all the house where they were sitting.
en_uk_webbe~ACT~C02~03~Tongues like fire appeared and were distributed to them, and one sat on each of them.
en_uk_webbe~ACT~C02~04~They were all filled with the Holy Spirit, and began to speak with other languages, as the Spirit gave them the ability to speak.
en_uk_webbe~ACT~C02~05~Now there were dwelling in Jerusalem Jews, devout men, from every nation under the sky.
en_uk_webbe~ACT~C02~06~When this sound was heard, the multitude came together and were bewildered, because everyone heard them speaking in his own language.
en_uk_webbe~ACT~C02~07~They were all amazed and marvelled, saying to one another, “Behold, aren’t all these who speak Galileans?
en_uk_webbe~ACT~C02~08~How do we hear, everyone in our own native language?
en_uk_webbe~ACT~C02~09~Parthians, Medes, Elamites, and people from Mesopotamia, Judea, Cappadocia, Pontus, Asia,
en_uk_webbe~ACT~C02~10~Phrygia, Pamphylia, Egypt, the parts of Libya around Cyrene, visitors from Rome, both Jews and proselytes,
en_uk_webbe~ACT~C02~11~Cretans and Arabians: we hear them speaking in our languages the mighty works of God!”
en_uk_webbe~ACT~C02~12~They were all amazed, and were perplexed, saying to one another, “What does this mean?”
en_uk_webbe~ACT~C02~13~Others, mocking, said, “They are filled with new wine.”
en_uk_webbe~ACT~C02~14~But Peter, standing up with the eleven, lifted up his voice, and spoke out to them, “You men of Judea, and all you who dwell at Jerusalem, let this be known to you, and listen to my words.
en_uk_webbe~ACT~C02~15~For these aren’t drunken, as you suppose, seeing it is only the third hour of the day.
en_uk_webbe~ACT~C02~16~But this is what has been spoken through the prophet Joel:
en_uk_webbe~ACT~C02~17~‘It will be in the last days, says God, that I will pour out my Spirit on all flesh. Your sons and your daughters will prophesy. Your young men will see visions. Your old men will dream dreams.
en_uk_webbe~ACT~C02~18~Yes, and on my servants and on my handmaidens in those days, I will pour out my Spirit, and they will prophesy.
en_uk_webbe~ACT~C02~19~I will show wonders in the sky above, and signs on the earth beneath: blood, and fire, and billows of smoke.
en_uk_webbe~ACT~C02~20~The sun will be turned into darkness, and the moon into blood, before the great and glorious day of the Lord comes.
en_uk_webbe~ACT~C02~21~It will be that whoever will call on the name of the Lord will be saved.’
en_uk_webbe~ACT~C02~22~“Men of Israel, hear these words! Jesus of Nazareth, a man approved by God to you by mighty works and wonders and signs which God did by him amongst you, even as you yourselves know,
en_uk_webbe~ACT~C02~23~him, being delivered up by the determined counsel and foreknowledge of God, you have taken by the hand of lawless men, crucified and killed;
en_uk_webbe~ACT~C02~24~whom God raised up, having freed him from the agony of death, because it was not possible that he should be held by it.
en_uk_webbe~ACT~C02~25~For David says concerning him, ‘I saw the Lord always before my face, for he is on my right hand, that I should not be moved.
en_uk_webbe~ACT~C02~26~Therefore my heart was glad, and my tongue rejoiced. Moreover my flesh also will dwell in hope;
en_uk_webbe~ACT~C02~27~because you will not leave my soul in Hades, neither will you allow your Holy One to see decay.
en_uk_webbe~ACT~C02~28~You made known to me the ways of life. You will make me full of gladness with your presence.’
en_uk_webbe~ACT~C02~29~“Brothers, I may tell you freely of the patriarch David, that he both died and was buried, and his tomb is with us to this day.
en_uk_webbe~ACT~C02~30~Therefore, being a prophet, and knowing that God had sworn with an oath to him that of the fruit of his body, according to the flesh, he would raise up the Christ to sit on his throne,
en_uk_webbe~ACT~C02~31~he foreseeing this spoke about the resurrection of the Christ, that his soul wasn’t left in Hades, and his flesh didn’t see decay.
en_uk_webbe~ACT~C02~32~This Jesus God raised up, to which we all are witnesses.
en_uk_webbe~ACT~C02~33~Being therefore exalted by the right hand of God, and having received from the Father the promise of the Holy Spirit, he has poured out this, which you now see and hear.
en_uk_webbe~ACT~C02~34~For David didn’t ascend into the heavens, but he says himself, ‘The Lord said to my Lord, “Sit by my right hand
en_uk_webbe~ACT~C02~35~until I make your enemies a footstool for your feet.”’
en_uk_webbe~ACT~C02~36~“Let all the house of Israel therefore know certainly that God has made him both Lord and Christ, this Jesus whom you crucified.”
en_uk_webbe~ACT~C02~37~Now when they heard this, they were cut to the heart, and said to Peter and the rest of the apostles, “Brothers, what shall we do?”
en_uk_webbe~ACT~C02~38~Peter said to them, “Repent, and be baptised, every one of you, in the name of Jesus Christ for the forgiveness of sins, and you will receive the gift of the Holy Spirit.
en_uk_webbe~ACT~C02~39~For the promise is to you, and to your children, and to all who are far off, even as many as the Lord our God will call to himself.”
en_uk_webbe~ACT~C02~40~With many other words he testified, and exhorted them, saying, “Save yourselves from this crooked generation!”
en_uk_webbe~ACT~C02~41~Then those who gladly received his word were baptised. There were added that day about three thousand souls.
en_uk_webbe~ACT~C02~42~They continued steadfastly in the apostles’ teaching and fellowship, in the breaking of bread, and prayer.
en_uk_webbe~ACT~C02~43~Fear came on every soul, and many wonders and signs were done through the apostles.
en_uk_webbe~ACT~C02~44~All who believed were together, and had all things in common.
en_uk_webbe~ACT~C02~45~They sold their possessions and goods, and distributed them to all, according as anyone had need.
en_uk_webbe~ACT~C02~46~Day by day, continuing steadfastly with one accord in the temple, and breaking bread at home, they took their food with gladness and singleness of heart,
en_uk_webbe~ACT~C02~47~praising God, and having favour with all the people. The Lord added to the assembly day by day those who were being saved.
