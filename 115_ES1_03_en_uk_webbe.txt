en_uk_webbe~ES1~C03~01~Now king Darius made a great feast to all his subjects, and to all that were born in his house, and to all the princes of Media and of Persia,
en_uk_webbe~ES1~C03~02~and to all the local governors and captains and governors that were under him, from India to Ethiopia, in the hundred and twenty and seven provinces.
en_uk_webbe~ES1~C03~03~And when they had eaten and drunken, and being satisfied were gone home, then Darius the king went into his bedchamber, and slept, and awaked out of his sleep.
en_uk_webbe~ES1~C03~04~Then the three young men of the body-guard, that kept the king’s person, spoke one to another:
en_uk_webbe~ES1~C03~05~Let every one of us say one thing which shall be strongest: and he whose sentence shall seem wiser than the others, to him shall Darius the king give great gifts, and great honours in token of victory:
en_uk_webbe~ES1~C03~06~as, to be clothed in purple, to drink in gold, and to sleep upon gold, and a chariot with bridles of gold, and a turban of fine linen, and a chain about his neck:
en_uk_webbe~ES1~C03~07~and he shall sit next to Darius because of his wisdom, and shall be called Darius his cousin.
en_uk_webbe~ES1~C03~08~And then they wrote every one his sentence, and set to their seals, and laid [the writing] under king Darius his pillow,
en_uk_webbe~ES1~C03~09~and said, When the king is risen, some shall give him the writing; and of whose side the king and the three princes of Persia shall judge that his sentence is the wisest, to him shall the victory be given, as it is written.
en_uk_webbe~ES1~C03~10~The first wrote, Wine is the strongest.
en_uk_webbe~ES1~C03~11~The second wrote, The king is strongest.
en_uk_webbe~ES1~C03~12~The third wrote, Women are strongest: but above all things Truth bears away the victory.
en_uk_webbe~ES1~C03~13~Now when the king was risen up, they took the writing, and gave it to him, and so he read it:
en_uk_webbe~ES1~C03~14~and sending forth he called all the princes of Persia and of Media, and the local governors, and the captains, and the governors, and the chief officers;
en_uk_webbe~ES1~C03~15~and sat him down in the royal seat of judgement; and the writing was read before them.
en_uk_webbe~ES1~C03~16~And he said, Call the young men, and they shall explain their own sentences. So they were called, and came in.
en_uk_webbe~ES1~C03~17~And they said to them, Declare to us your mind concerning the things you° have written. Then began the first, who had spoken of the strength of wine,
en_uk_webbe~ES1~C03~18~and said thus, O sirs, how exceedingly strong is wine! it causes all men to err that drink it:
en_uk_webbe~ES1~C03~19~it makes the mind of the king and of the fatherless child to be all one; of the bondman and of the freeman, of the poor man and of the rich:
en_uk_webbe~ES1~C03~20~it turns also every thought into cheer and mirth, so that a man remembers neither sorrow nor debt:
en_uk_webbe~ES1~C03~21~and it makes every heart rich, so that a man remembers neither king nor local governor; and it makes to speak all things by talents:
en_uk_webbe~ES1~C03~22~and when they are in their cups, they forget their love both to friends and kindred, and a little after draw their swords:
en_uk_webbe~ES1~C03~23~but when they awake from their wine, they remember not what they have done.
en_uk_webbe~ES1~C03~24~O sirs, is not wine the strongest, seeing that it enforces to do thus? And when he had so spoken, he held his peace.
